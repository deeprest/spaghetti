#!/bin/bash

# git-lfs
function linuxInstallGLFS
{
  pushd tool/linux
  # https://git-lfs.github.com/
  tar -xf git-lfs-linux-amd64-2.2.1.tar.gz
  source git-lfs-2.2.1/install.sh
  rm -r git-lfs-2.2.1
  popd
  git lfs install
}


# INSTALL NODEJS
# curl https://nodejs.org/dist/v4.3.0/node-v4.3.0-linux-x64.tar.xz > node.tar.gz
# OUTNAME=$(tar -tf node.tar.xz | head -n 1)
# echo $OUTNAME
# tar -xf node.tar.xz
# mv $OUTNAME node
# if (( $(uname -a | grep -c Debian)  == 1 )); then
#   #Debian specifix node install (node 4.x)
#   curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -
#   sudo apt-get install -y nodejs
# fi
#npm fancy install
#curl -L https://npmjs.org/install.sh | sh
#npm --version

# Debian: Run the following to install the source for nodejs
#curl -sL https://deb.nodesource.com/setup | sudo -E bash -
# Then, run this to get the latest nodejs package
# nodejs v0.10.46 (was the latest in the debian repo at this time)
# sudo apt-get install clang nodejs npm
#sudo apt-get install clang libgl1-mesa-dev libsdl2-dev

### setup home dir npm library
# echo "prefix=${HOME}/.local/npm-packages" > ~/.npmrc
###  add this to .bashrc (or whatever startup script for your user)
## NPM packages in homedir
#NPM_PACKAGES="$HOME/.local/npm-packages"
## Tell our environment about user-installed node tools
#PATH="$PATH:$NPM_PACKAGES/bin"
## Unset manpath so we can inherit from /etc/manpath via the `manpath` command
#unset MANPATH  # delete if you already modified MANPATH elsewhere in your configuration
#MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
## Tell Node about these packages
#NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"


# Sauce build tool: nodejs, npm, llvm
function linuxInstallSauce
{
  #sudo apt-get install clang nodejs npm

  echo "Installing Sauce..."
  SAUCE=tool/sauce
  pushd $SAUCE
    npm install .
    ./coffee.sh
  popd

  echo "Building external dependencies..."
  pushd tool
    ./get-libs.sh
  popd
}

linuxInstallSauce


function fixNPM
{
  # Install npm packages globally without sudo on macOS and Linux
  # found at https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md
  #1. Create a directory for globally packages
  mkdir "${HOME}/.npm-packages"
  #2. Indicate to npm where to store globally installed packages. In your ~/.npmrc file add:
  prefix=${HOME}/.npm-packages
  #3. Ensure npm will find installed binaries and man pages. Add the following to your .bashrc/.zshrc:
  # NPM packages in homedir
  NPM_PACKAGES="$HOME/.local/npm-packages"
  # Tell our environment about user-installed node tools
  PATH="$NPM_PACKAGES/bin:$PATH"
  # Unset manpath so we can inherit from /etc/manpath via the `manpath` command
  unset MANPATH  # delete if you already modified MANPATH elsewhere in your configuration
  MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
  # Tell Node about these packages
  NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
}
#fixNPM
