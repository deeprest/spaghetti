# show which minimum version of MacOS the executable was built for.

otool -l /path/to/bin | grep -E -A4 '(LC_VERSION_MIN_MACOSX|LC_BUILD_VERSION)' | grep -B1 sdk
