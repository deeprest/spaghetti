#!/usr/bin/env bash

# dynamic lib install names must be set to be found in executable directory at runtime


# DIR=../../external/darwin/lib
#
# chmod +rw $DIR/libSDL2-2.0.0.dylib
# install_name_tool -id "@executable_path/libSDL2-2.0.0.dylib" $DIR/libSDL2-2.0.0.dylib
# otool -L $DIR/libSDL2-2.0.0.dylib | head -n 2
#
# chmod +rw $DIR/libSDL2_image-2.0.0.dylib
# install_name_tool -id "@executable_path/libSDL2_image-2.0.0.dylib" $DIR/libSDL2_image-2.0.0.dylib
# otool -L $DIR/libSDL2_image-2.0.0.dylib | head -n 2


pushd ../../dev-build/darwin
  install_name_tool -change "/usr/local/opt/sdl2/lib/libSDL2-2.0.0.dylib" "@executable_path/libSDL2-2.0.0.dylib" demo
  install_name_tool -change "/usr/local/opt/sdl2_image/lib/libSDL2_image-2.0.0.dylib" "@executable_path/libSDL2_image-2.0.0.dylib" demo
popd
