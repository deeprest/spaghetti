#!/bin/bash

rootDir=$(pwd)/..
assetDir=$rootDir/asset
buildDir=$rootDir/build

assets=$(echo "$(find $assetDir -type f)" | grep -E "\.png|\.ogg|\.json|\.as")
echo "$assets" #| grep -E ".png|.as")

for i in $assets; do
  echo "$i"
  cp -u "$i" $buildDir
done
