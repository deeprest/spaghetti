#!/bin/bash

# GIT-LFS
function installGLFS
{
  brew install git-lfs
  # pushd tool/darwin
  #   tar -xf git-lfs-darwin-amd64-2.2.1.tar.gz
  #   source git-lfs-2.2.1/install.sh
  # popd
  git lfs install
}
installGLFS

# Sauce build tool: nodejs, npm, llvm
function installSauce
{
  brew install node@6 npm
  # curl https://nodejs.org/dist/v4.3.0/node-v4.3.0.pkg > node.pkg
  # sudo installer -pkg node.pkg -target /
  SAUCE=tool/sauce
  ln -s $(brew --prefix llvm)/lib/libclang.dylib $SAUCE/libclang.dylib
  pushd $SAUCE
    npm install .
    ./coffee.sh
  popd
  pushd tool
    ./get-libs.sh
  popd
}
installSauce

# function fixNPM
# {
#   # Install npm packages globally without sudo on macOS and Linux
#   # found at https://github.com/sindresorhus/guides/blob/master/npm-global-without-sudo.md
#   #1. Create a directory for globally packages
#   mkdir "${HOME}/.npm-packages"
#   #2. Indicate to npm where to store globally installed packages. In your ~/.npmrc file add:
#   prefix=${HOME}/.npm-packages
#   #3. Ensure npm will find installed binaries and man pages. Add the following to your .bashrc/.zshrc:
#   # NPM packages in homedir
#   NPM_PACKAGES="$HOME/.local/npm-packages"
#   # Tell our environment about user-installed node tools
#   PATH="$NPM_PACKAGES/bin:$PATH"
#   # Unset manpath so we can inherit from /etc/manpath via the `manpath` command
#   unset MANPATH  # delete if you already modified MANPATH elsewhere in your configuration
#   MANPATH="$NPM_PACKAGES/share/man:$(manpath)"
#   # Tell Node about these packages
#   NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
# }
#fixNPM
