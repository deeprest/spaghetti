#!/bin/bash
# linux darwin mingw
target=unknown
if [[ "$OSTYPE" == "linux-gnu" ]]; then
  target=linux
elif [[ "$OSTYPE" == "darwin"* ]]; then
  target=darwin
elif [[ "$OSTYPE" == "msys" ]]; then
  target=mingw
fi

rootDir=$(pwd) #$(dirname $(pwd) )
includeDir=$rootDir/external/$target/include
libraryDir=$rootDir/external/$target/lib
sourceDir=$rootDir/code
executableDir=$rootDir/TEST/$target

executableName=purity
includeDirectories=(-I/usr/local/include -I/usr/include -I"$sourceDir" -I"$includeDir" -I"$sourceDir/angelscript" )
libraries=(-Wl,-L/usr/local/lib,-L/usr/lib,-L$libraryDir -lSDL2 -lSDL2_image -langelscript )
compilerDefines=( -DGL_GLEXT_PROTOTYPES )
sourceFiles=$(find $sourceDir -type f | grep -E ".*\.cpp$" | grep -Ev "/\.\w" )

clangArgs=( -std=c++11 )

if [ "$target"=="darwin" ]; then
  #-D_THREAD_SAFE )
  includeDirectories=( -I"$sourceDir" -I"$includeDir" -I"$sourceDir/angelscript" -I/usr/local/include -I/usr/include )
  libraries+=( -lm -liconv -Wl,-framework,OpenGL -Wl,-framework,CoreAudio -Wl,-framework,AudioToolbox -Wl,-framework,ForceFeedback -lobjc -Wl,-framework,CoreVideo -Wl,-framework,Cocoa -Wl,-framework,Carbon -Wl,-framework,IOKit )
fi

if [ "$target" == "mingw" ]; then
  includeDirectories+=(-I/usr/include/SDL2 -Dmain=SDL_main -I/g/mingw/msys/1.0/mingw/lib/gcc/mingw32/4.8.1/include/c++ -I/g/mingw/msys/1.0/mingw/lib/gcc/mingw32/4.8.1/include/c++/mingw32)
  libraries+=(-L$libraryDir/mingw,-LG:\mingw\msys\1.0\lib,-lSDL2,-langelscript,-lmingw32,-lSDL2main)
  echo ${includeDirectories[*]}
  echo ${libraries[*]}
fi

mkdir -p $executableDir
# clean
rm -f $executableDir/$executableName
#echo $executableDir/$executableName ${clangArgs[*]} ${sourceFiles[*]} ${libraries[*]} ${includeDirectories[*]} ${compilerDefines[*]}
echo Building...
clang++ -g -o $executableDir/$executableName ${clangArgs[*]} ${sourceFiles[*]} ${libraries[*]} ${includeDirectories[*]} ${compilerDefines[*]}
