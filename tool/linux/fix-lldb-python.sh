
PYTHON=2.7
LLVM=3.8
LLVM_PATCH=1
pushd /usr/lib/llvm-$LLVM/lib/python$PYTHON/site-packages/lldb
  sudo rm _lldb.so libLLVM-$LLVM.0.so.1 libLLVM-$LLVM.so.1
  sudo ln -s ../../../liblldb.so.1 _lldb.so
  sudo ln -s ../../../libLLVM-$LLVM.$LLVM_PATCH.so.1 libLLVM-$LLVM.0.so.1
  sudo ln -s ../../../libLLVM-$LLVM.$LLVM_PATCH.so.1 libLLVM-$LLVM.so.1
  # this was necessary for the Atom.io Nuclide debugger to find module _lldb
  sudo ln -s /usr/lib/llvm-$LLVM/lib/liblldb.so /usr/lib/python$PYTHON/dist-packages/_lldb.so
popd
# export PYTHONPATH='/usr/lib/llvm-$LLVM/lib/python$PYTHON/site-packages'
