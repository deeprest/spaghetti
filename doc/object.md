
# Objects
I refer to GameObjects and Components collectively as 'objects', but the only difference is GameObjects contain Components and not the other way around.

## Component
Components are the only 'real' objects.
* Transform
* Mesh (bound to transform, associated with a meshview)
* Meshview
  * material
* Logic
* RigidBody (bound to a transform)
  * Shapes
  * Joints

## GameObject
A GameObject is a collection of Components and their references.


# Object File Layout
An object file is a collection of GameObjects, their Components and external references.


## External Section
This is a registry of external IDs and *partially defined* external objects. External objects are not completely defined here, only enough to resolve the external references to something meaningful. The goal is to preserve the relationship of objects defined internally (in this file) with those defined elsewhere; and only define what is required by internal objects.

This allows objects to be defined in a modular way and still allow for references to mostly-undefined objects through a type of 'interface'. In theory, the internal objects could be associated with *any* external object, provided they meet the defined criteria.

If the external section seems to contain an excessive amount of data, it may be an indication the internal and external objects are overly-coupled and would be more appropriately defined together.

## Object Definitions

+ Two sections: one for GameObject/Component definitions, and one for references to externally-defined objects.
+ Key is the component type
+ For each type, there is an *object* or an *array* of objects
+ Each object has an ID, which is simply a way to reference that
+ IDs are either *internal*, unless defined in the **external section**
+ If any component/object needs to reference external objects, those objects/components are defined in the **external section**
+ The external section minimally defines w


    external:[
      {
        id: 0x0B00
        transform:
          id: 0x7F01
        body:
          id: 0xBB33
      },
      ...
    ]
    object:[
      {
        id: 0x03
        transform:
          id: 0x0301
          parent: 0x7F01
          matrix: ...
        rigidbody:
          id: 0xBB01
          transform: 0x0301
          type:2
          shape:[
            {
              friction: 0.5
              type: 2
              vert:[0.0,0.1,...]
            },
            ...
          ]
          joint:[
            {
              type: 1
              bodyA: 0xBB01
              bodyB: 0xBB33
            },
            ...
          ]
      }
    ]
