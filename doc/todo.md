# To Do
+ camera component. add props to object file
+ object file format; global object ids; object map
+ materials. serialize material values along with shader source
+ sauce: incremental builds
+ physics components: body(one per transform), fixture, shape, joint
+ debug draw joints
+ audio source component
+ collision reaction -> play sound
+ generic pool
+ meshes should not be components, but should be stored in a pool
- destroy non-shared meshes when meshview is destroyed
- mesh reinitialization. delete GL buffers, etc
- audio clip stored in pool
- textures, pool

## Thoughts
* Tightly-coupled. Do not create abstractions around libraries when possible; use their types.
* A GameObject is a container for components. It's purpose is to hold references to components, for other components on the object.
* All components are kept in contiguous memory by type. Access the correct vector by map lookup to a vector pointer (cast to template type).
* Resources: use directory tree for development, but access resources from archives using physfs in packaged builds.
* Scene
  * preallocate all types
  * deserialize basic types (data, ID refs)
  * construct complex types from basic types (ref lookups)
  * register objects


## Wishlist
- input bindings
* Deployment: package a build with mojosetup
* Handle System instead of smart pointers.
* Data Baking
  - prereq: handles
  - memory alignment
  - deserialize from json, export baked objects and pointer table
  - load baked object, pointer fixup table
  - commandline util: just ifdef in main and build a utility from NodeJS API(nw.js?) that sauce.
* Angelscript
  * coroutines
  * one concurrency thread
  * hot-reloading
  * interpreter
* Lua integration
* Verse2 integration
- Sauce: CBOR serialization
- script coroutines: check 'contextmgr' in addons!!  http://www.angelcode.com/angelscript/sdk/docs/manual/doc_adv_coroutine.html
- script hot-loading: http://www.angelcode.com/angelscript/sdk/docs/manual/doc_adv_dynamic_build.html#doc_adv_dynamic_build_ondemand
