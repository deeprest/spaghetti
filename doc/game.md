

# Big Idea
You spawn creatures and lead them through the circle of life, from birth to reproduction and eventually death. Upon death you may continue to play as one of your offspring, so the journey does not necessarily end. The implicit goal is survival but the experience is designed around discovery and wonder.


## Events
- birth / introduction (learn basics to continue- like first level of Mario)
- interact/play with siblings. echo, follow, fight over control of object, play 'tag'
- follow mother/protector and learn what to eat. you are an apprentice
- strike out on your own, possibly with your 'pack'. you are a an adulthood
- find sources of food. you are a provider
- discover wonders. you are an explorer
- establish a home. you are a homemaker
- attract a mate, impress them with your home and 'mating display' (decorate and dance)
- romance explosion. you are a lover
- watch your hatchlings grow. you are a protector


## Actions
- swim. boost
- speak. call. sing (like Journey)
- bite to attack
- carry (with mouth)
- eat various types of meats and non-meats
- bug out when the jaws of death show up
- cavort with various creatures

# Design (and anti-goals)
* (SEE antigoals doc)
* have no menu: 'menu selections' are made through game mechanics
  + start in 'aquarium/zoo/home' which is a collection of all the player's creatures
* no projectiles

## Creatures

1. shape/form
2. movement
3. aggression
4. what they eat
5. other behavior
6. reproduction method

### fish (triggerfish)
[Celestial Eye](https://en.wikipedia.org/wiki/Celestial_Eye)
1. short chain with fins in adulthood
2. quick and agile
3. defensive
4. eat plants
5. social, [shoaling](https://en.wikipedia.org/wiki/Shoaling_and_schooling) fish
6. romance explosion. sticky eggs

### eel
1. chain links.
2. slithers
3. opportunist
4. vampiric
5. screamers. 'those are screaming eels, your highness!'
6. romance explosion. sticky eggs


### urchin
1. various shapes. sometimes a ball with spikes that can damage or poison(temporary paralysis of affected area)
2. goes with the flow
3. chill
4. absorb nutrients from surroundings
5. spikes can be broken off, and the meaty center provides a certain kind of nutrition.
6. asexual

### 'sheemp'

### piranas
swarm

### jaws
dangerous. mighty jaws. play sound of war drum when detected.
### angler
has attractive lure
### blowfish
creature that can temporarily increase size. maybe can push with current.
### whale
can swallow you whole if you are small
### octopus
dangerous. camouflaged.

# things
* rocks
* water current forces, ambient flow
* debris/smell particles
* plants to eat
* chunky treats to eat
* nutrition represented with color


# random
* sockwah velvet: aquatic sock-monkey creatures in a craftsy dry sea
* vicious fishes
