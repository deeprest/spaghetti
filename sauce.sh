#!/bin/bash

SAUCE_DIR=../sauce
PROJECT_CONFIG=$(pwd)/build.cson

pushd $SAUCE_DIR
  npm run build # coffee
  ./sauce --config=$PROJECT_CONFIG $@
popd
