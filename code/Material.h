#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <tuple>
#include <array>

#include "SDL2/SDL_log.h"
#include "GL.h"
#include "json.hpp"


namespace Spaghetti
{
  using json = nlohmann::json;
  using string = std::string;

  struct Material
  {
    static Material* GetCurrent();
    static Material* GetMaterial( const string& materialName );

    bool Initialize( const json& obj );
    bool Validate();
    void Activate();
    void Destroy();

    inline GLuint GetProgramID() const { return programID; }

    string name;
    std::unordered_map<string,bool> propertyBool;

    std::unordered_map<string,GLint> attribute3f;
    std::unordered_map<string,GLint> attribute2f;
    std::unordered_map<string,std::tuple<GLint,float>> uniform1f;
    std::unordered_map<string,std::tuple<GLint,std::array<float,4>>> uniform4fv;

  private:
    static std::unordered_map<string, Material*> allMaterials;

    GLuint programID = 0;

    struct Shader
    {
      void Source( const string& s );
      void Destroy();
      void LogStatus();

      string key;  // used to identify the shader
      GLuint type; // GL_VERTEX_SHADER, GL_FRAGMENT_SHADER, etc
      GLuint ID;
      string source;
    };

    std::vector<Shader> attachedShaders;

    void Attach( Shader shader );
    void Detach( Shader shader );
  };

  // void to_json( json& j, const Material& obj)
  // {}
  // void from_json(const json& j, Material& obj)
  // {}

  // struct Texture
  // {
  //   string name;
  //   unsigned int gl_id;
  //   unsigned char *imageData;
  //   int w, h;
  //   int channels;
  // };

  //void LoadTexture( const string& sourceImage );


}
