#ifdef SCRIPTING_ENABLED
#include "Binding.h"
#include "Core.h"
#include <cassert>

#include "angelscript/scriptarray.h"
#include "angelscript/scriptbuilder.h"
#include "angelscript/scriptdictionary.h"
#include "angelscript/scriptmath.h"
#include "angelscript/scriptstdstring.h"


namespace Spaghetti
{
  using namespace std;
  using namespace Debug;

  // Implement a simple message callback function
  void MessageCallback( const asSMessageInfo* msg, void* param )
  {
    const char* type = "ERR ";
    if( msg->type == asMSGTYPE_WARNING )
      type = "WARN";
    else
    if( msg->type == asMSGTYPE_INFORMATION )
      type = "INFO";

    SDL_Log( "%s (%d, %d) : %s : %s\n", msg->section, msg->row, msg->col, type, msg->message );
  }

  void Core::InitializeScript()
  {
    // Create the script engine
    engine = asCreateScriptEngine( ANGELSCRIPT_VERSION );
    int r = 0;
    // Set the message callback to receive information on errors in human readable form.
    r = engine->SetMessageCallback( asFUNCTION( MessageCallback ), 0, asCALL_CDECL );
    assert( r >= 0 );

    // Register Add-ons
    RegisterScriptMath( engine );
    RegisterScriptArray( engine, true );
    RegisterStdString( engine );
    RegisterScriptDictionary( engine );

    SDL::Initialize( window, engine );
    RegisterAllBindings( engine );

    contextManager = new CContextMgr();
    contextManager->RegisterCoRoutineSupport( engine );

    ScriptBuild( "script/main.as" );
    // ExecuteContext();

    // Find the function that is to be called.
    mod = engine->GetModule( "MyModule" );
    func = mod->GetFunctionByDecl( "void main()" );
    if( func == 0 )
    {
      // The function couldn't be found. Instruct the script write to include the expected function in the script.
      SDL_Log( "The script must have the function 'void main()'. Please add it and try again.\n" );
      return;
    }
    ctx = contextManager->AddContext( engine, func );

    asIScriptFunction* coroutineFunc = mod->GetFunctionByDecl( "void coroutineTest()" );
    if( func == 0 )
    {
      // The function couldn't be found. Instruct the script write to include the expected function in the script.
      SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "The script must have the function 'void main()'. Please add it and try again.\n" );
      return;
    }
    else
    {
      contextManager->AddContextForCoRoutine( ctx, coroutineFunc );
    }
  }

  void Core::ScriptBuild( const char* scriptName )
  {
    // The CScriptBuilder helper is an add-on that loads the file, performs a pre-processing pass if necessary, and then tells
    // the engine to build a script module.
    CScriptBuilder builder;
    int r = builder.StartNewModule( engine, "MyModule" );
    if( r < 0 )
    {
      // If the code fails here it is usually because there is no more memory to allocate the module
      SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Unrecoverable error while starting a new module.\n" );
      return;
    }
    r = builder.AddSectionFromFile( scriptName );
    if( r < 0 )
    {
      // The builder wasn't able to load the file. Maybe the file has been removed, or the wrong name was given, or some
      // preprocessing commands are incorrectly written.
      SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Please correct the errors in the script and try again.\n" );
      return;
    }
    r = builder.BuildModule();
    if( r < 0 )
    {
      // An error occurred. Instruct the script writer to fix the compilation errors that were listed in the output stream.
      SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Please correct the errors in the script and try again.\n" );
      return;
    }
  }

  void Core::ExecuteContext()
  {

    // Find the function that is to be called.
    mod = engine->GetModule( "MyModule" );
    func = mod->GetFunctionByDecl( "void main()" );
    if( func == 0 )
    {
      // The function couldn't be found. Instruct the script write to include the expected function in the script.
      SDL_Log( "The script must have the function 'void main()'. Please add it and try again.\n" );
      return;
    }

    //ctx = contextManager->AddContext( engine, func );

    // Create our context, prepare it, and then execute
    ctx = engine->CreateContext();
    ctx->Prepare( func );
    int r = ctx->Execute();
    if( r != asEXECUTION_FINISHED )
    {
      // The execution didn't complete as expected. Determine what happened.
      if( r == asEXECUTION_EXCEPTION )
      {
        // An exception occurred, let the script writer know what happened so it can be corrected.
        SDL_Log( "An exception '%s' occurred. Please correct the code and try again.\n", ctx->GetExceptionString() );
      }
    }
  }

  // void Core::ReloadScripts()
  // {
  //   ScriptBuild( "script/main.as" );
  //   ExecuteContext();
  // }
}

#endif
