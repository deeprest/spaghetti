#include "Resource.h"

#include <fstream>
#include <sstream>
#include <string>

#include "SDL2/SDL.h"
#include "SDL2/SDL_image.h"
#include "physfs.h"

#include "Common.h"


namespace Spaghetti
{

  using namespace std;
  using json = nlohmann::json;

  string Resource::basepath;
  string Resource::persistentPath;

  void Resource::Initialize()
  {
    char* base = SDL_GetBasePath();
    basepath = base;
    SDL_free( base );
    SDL_Log( "SDL basepath: %s", basepath.c_str() );

    char* pref = SDL_GetPrefPath( "deeprest", "spaghetti" );
    persistentPath = pref;
    SDL_Log( "SDL persistentPath: %s", persistentPath.c_str() );
    SDL_free( pref );

    if( PHYSFS_init( basepath.c_str() ) > 0 )
    {
      string basedir( PHYSFS_getBaseDir() );
      SDL_Log( "PhysFS basedir: %s", basedir.c_str() );
      ;
      //string prefdir( PHYSFS_getPrefDir("deeprest", "spaghetti") );
      //SDL_Log( "PhysFS prefdir: %s", prefdir.c_str() );
      PHYSFS_Version compiled;
      PHYSFS_VERSION( &compiled );
      SDL_Log( "PhysFS (compiled) version %d.%d.%d ...\n", compiled.major, compiled.minor, compiled.patch );
      PHYSFS_Version linked;
      PHYSFS_getLinkedVersion( &linked );
      SDL_Log( "PhysFS (linked) version %d.%d.%d.\n", linked.major, linked.minor, linked.patch );
      for( const PHYSFS_ArchiveInfo** i = PHYSFS_supportedArchiveTypes(); *i != NULL; i++ )
      {
        SDL_Log( "Supported archive: [%s], which is [%s]", ( *i )->extension, ( *i )->description );
      }
      if( !PHYSFS_mount( basedir.c_str(), nullptr, 1 ) )
        SDL_LogError( LOG_CATEGORY_ASSET, "PhysFS mount basedir: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );

      if( !PHYSFS_mount( persistentPath.c_str(), nullptr, 1 ) )
        SDL_LogError( LOG_CATEGORY_ASSET, "PhysFS mount persistent path: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );

      if( !PHYSFS_mount( ( basedir + "arc.zip" ).c_str(), nullptr, 1 ) )
        SDL_LogError( LOG_CATEGORY_ASSET, "PhysFS mount zip: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );

      // {
      //   char** listBegin = PHYSFS_enumerateFiles( "" );
      //   for( char** file = listBegin; *file != NULL; file++ )
      //   {
      //     SDL_Log( "[%s]", *file );
      //   }
      //   PHYSFS_freeList( listBegin );
      // }

      SDL_Log( "Setting write directory: %s", persistentPath.c_str() );
      PHYSFS_setWriteDir( persistentPath.c_str() );
    }
    else
    {
      SDL_LogError( LOG_CATEGORY_ASSET, "PhysFS %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
    }
  }

  void Resource::ReadBuffer( const string& filename, std::vector<uint8_t>& buffer )
  {
    json jsonObject = nullptr;
    if( PHYSFS_exists( filename.c_str() ) )
    {
      SDL_LogVerbose( LOG_CATEGORY_ASSET, "Reading %s", filename.c_str() );
      PHYSFS_File* handle = PHYSFS_openRead( filename.c_str() );
      if( handle == NULL )
      {
        SDL_Log( "ReadBuffer error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
        //throw std::invalid_argument("handle not found: " + std::string(filename));
        return;
      }
      PHYSFS_uint32 len = PHYSFS_fileLength( handle );
      buffer.resize( len );
      PHYSFS_sint64 countRead = PHYSFS_readBytes( handle, &buffer[0], len );
      if( countRead == -1 || countRead < len )
        SDL_Log( "ReadBuffer error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
      // SDL_Log("BYTES READ %d %d", len, countRead );
    }
    else
    {
      SDL_LogError( LOG_CATEGORY_ASSET, "Asset %s does not exist", filename.c_str() );
    }
  }

  json Resource::ReadJSON( const string& filename )
  {
    std::vector<uint8_t> buffer;
    ReadBuffer( filename, buffer );
    return json::parse( buffer );
  }

  void Resource::WriteJSON( const string& filename, const json& obj )
  {
    /* if( ofstream outstream{( filename ).c_str(), ios::out } )
        outstream << obj; */

    SDL_LogVerbose( LOG_CATEGORY_ASSET, "Writing %s", filename.c_str() );
    PHYSFS_File* file = PHYSFS_openWrite( filename.c_str() );
    if( file == NULL )
    {
      SDL_Log( "WriteJSON error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
      return;
    }
    std::string str = obj.dump();
    void* data = (void*)str.data();
    PHYSFS_sint64 result = PHYSFS_writeBytes( file, data, str.size() );
    if( result == -1 )
    {
      SDL_Log( "WriteJSON error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
      return;
    }
  }

  string Resource::ReadString( const string& filename )
  {
    string str;
    if( ifstream is{( basepath + filename ).c_str(), ios::binary | ios::ate} )
    {
      auto size = is.tellg();
      str = string( size, '\0' );
      is.seekg( 0 );
      is.read( &str[0], size );
      // is.close();
    }
    return str;
  }

  void Resource::WriteCBOR( const string& filename, const json& obj )
  {
    /*std::vector<std::uint8_t> v_cbor = json::to_cbor( obj );
    if( ofstream outstream{( filename ).c_str(), ios::binary} )
    {
      outstream.write( reinterpret_cast<char*>( v_cbor.data() ), v_cbor.size() );
    }*/

    std::vector<std::uint8_t> v_cbor = json::to_cbor( obj );
    SDL_LogVerbose( LOG_CATEGORY_ASSET, "Writing CBOR %s", filename.c_str() );
    PHYSFS_File* file = PHYSFS_openWrite( filename.c_str() );
    if( file == NULL )
    {
      SDL_Log( "WriteCBOR error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
      return;
    }
    PHYSFS_sint64 result = PHYSFS_writeBytes( file, v_cbor.data(), v_cbor.size() );
    if( result == -1 )
    {
      SDL_Log( "WriteCBOR error: %s", PHYSFS_getErrorByCode( PHYSFS_getLastErrorCode() ) );
      return;
    }
  }

  json Resource::ReadCBOR( const std::string& filename )
  {
    std::vector<uint8_t> buffer;
    ReadBuffer( filename, buffer );
    return json::from_cbor( buffer );
  }

  void Resource::WritePNG( SDL_Surface* surface, const std::string& filename )
  {
    string path = persistentPath + filename;
    if( IMG_SavePNG( surface, path.c_str() ) != 0 )
      SDL_LogError( LOG_CATEGORY_ASSET, "Failed to save image (%s)", path.c_str() );
  }
}
