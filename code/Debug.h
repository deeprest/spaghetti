#pragma once

#include <string>
#include "Box2D/Common/b2Draw.h"
#include "Scene.h"

namespace Spaghetti
{
  namespace Debug
  {
    void Initialize();
    void CheckMath();
    int CheckGLErrors( const char* file, int line );
  }

  class DebugDraw : public b2Draw
  {
    GameObject* go;

  public:
    void Initialize( Scene* scene );
    void PreDraw();
    void PostDraw();

  	/// Draw a closed polygon provided in CCW order.
  	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;
  	/// Draw a solid closed polygon provided in CCW order.
  	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;
  	void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) override;
  	void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) override;
  	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) override;
  	void DrawTransform(const b2Transform& xf) override;
  };

}
