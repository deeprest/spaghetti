#include "Scene.h"

#include "glm/detail/func_geometric.hpp"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "json.hpp"
#include <random>
#include <sstream>

#include "Component.h"
#include "ComponentHeaders.h"
#include "Debug.h"
#include "Material.h"
#include "Mathematics.h"
#include "MemoryPool.h"
#include "Resource.h"
#include "Serialize.h"
#include "Timer.h"

namespace Spaghetti
{
  using namespace std;
  using namespace glm;
  using json = nlohmann::json;
  // move to util.h, random.h, helpers.h etc
  std::random_device randomdevice;
  std::default_random_engine randomengine( randomdevice() );
  // std::mt19937 gen( rd() );

  DebugDraw* debugDraw;
  /*

    void ContactListener::BeginContact( b2Contact* contact )
    {
      b2Fixture* fixA = contact->GetFixtureA();
      b2Body* bodyA = fixA->GetBody();
      Rigidbody* rbA = static_cast<Rigidbody*>( bodyA->GetUserData() );

      b2Fixture* fixB = contact->GetFixtureB();
      b2Body* bodyB = fixB->GetBody();
      Rigidbody* rbB = static_cast<Rigidbody*>( bodyB->GetUserData() );

      if( rbA->contactCallback != nullptr )
        rbA->contactCallback( rbA, rbB, contact );
      if( rbB->contactCallback != nullptr )
        rbB->contactCallback( rbB, rbA, contact );
    }

    void DestructionListener::SayGoodbye( b2Joint* joint )
    {
      // Joint* jnt = static_cast<Joint*>(joint->GetUserData());
      // Component::Destroy( jnt );
    }

    void DestructionListener::SayGoodbye( b2Fixture* fixture ) {}
  */


  std::unordered_map<ID_t, Rigidbody*> KillThese;


  void CreateHead( json& blueprint, int& ID, vec2 offset, float radius, int& rigidbodyParentID )
  {
    json transform = R"({"localMatrix":[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]})"_json;
    transform["localMatrix"][12] = offset.x;
    transform["localMatrix"][13] = offset.y;
    transform["id"] = ID++;
    blueprint["transform"].push_back( transform );

    // mesh disc
    json disc = R"({"type":"disc"})"_json;
    disc["radius"] = radius;

    // meshview disc
    json meshview = json::object();
    meshview["material"] = "simple";
    meshview["transform"]["id"] = transform["id"];
    meshview["mesh"] = disc;
    blueprint["meshview"].push_back( meshview );

    json rigidbody = R"({"type":"dynamic","shape":[{"type":"circle"}]})"_json;
    rigidbody["shape"][0]["radius"] = radius;
    rigidbody["id"] = ID++;
    rigidbody["transform"]["id"] = transform["id"];
    blueprint["rigidbody"].push_back( rigidbody );

    rigidbodyParentID = rigidbody["id"];
  }

  void CreateLink( json& blueprint, int& ID, vec2 offset, vec2 direction, float radius1, float radius2, int& rigidbodyParentID )
  {
    json transform = R"({"localMatrix":[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]})"_json;
    transform["localMatrix"][12] = offset.x;
    transform["localMatrix"][13] = offset.y;
    transform["id"] = ID++;
    blueprint["transform"].push_back( transform );

    // mesh disc
    json disc = R"({"type":"disc"})"_json;
    disc["radius"] = radius1;

    // meshview disc
    json meshview = json::object();
    meshview["material"] = "blue";
    meshview["transform"]["id"] = transform["id"];
    meshview["mesh"] = disc;
    blueprint["meshview"].push_back( meshview );

    json rigidbody = R"({"type":"dynamic","shape":[{"type":"circle"}]})"_json;
    rigidbody["shape"][0]["radius"] = radius1;
    // json box = R"({"type":"polygon"})"_json;
    // rigidbody["shape"].push_back( box );
    rigidbody["id"] = ID++;
    rigidbody["transform"]["id"] = transform["id"];
    blueprint["rigidbody"].push_back( rigidbody );


    // mesh polygon
    json segment = R"({"type":"segment"})"_json;
    segment["length"] = length( direction );
    segment["cap1"] = radius2;
    segment["cap2"] = radius1;
    segment["dir"] = json::array();
    segment["dir"][0] = -direction.x;
    segment["dir"][1] = -direction.y;
    // meshview segment
    meshview["mesh"] = segment;
    blueprint["meshview"].push_back( meshview );

    // the segment's "parent" joint
    json joint = R"({"type":"revolute"})"_json;
    joint["rigidbodyA"]["id"] = rigidbodyParentID;
    joint["rigidbodyB"]["id"] = rigidbody["id"];
    joint["anchor"]["x"] = 0;
    joint["anchor"]["y"] = 0;
    float angleLimit = std::uniform_real_distribution<float>( 0, 1 )( randomengine );
    joint["angleMin"] = -angleLimit;
    joint["angleMax"] = angleLimit;
    blueprint["joint"].push_back( joint );

    rigidbodyParentID = rigidbody["id"];
  }

  void CreateAppendage( json& blueprint, int& ID, vec2 offset, vec2 direction, float radiusMax, float radiusMin, int rigidbodyParentID )
  {
    const int links = std::uniform_int_distribution<int>( 2, 10 )( randomengine );
    vec2 increment;
    float radiusDec = ( radiusMax - radiusMin ) / (float)links;
    float radius = radiusMax;
    vec2 previousOffset;
    float lengthFactor = length( direction );
    for( size_t i = 0; i < links; i++ )
    {
      direction = normalize( direction ) * length( offset - previousOffset );
      CreateLink( blueprint, ID, offset, direction, radius, radius - radiusDec, rigidbodyParentID );
      increment = normalize( direction ) * radius * 1.8f * lengthFactor;
      radius -= radiusDec;
      previousOffset = offset;
      offset += increment;
    }
  }

  GameObject* Scene::GenerateCreature( const json& param )
  {
    json blueprint;
    if( blueprint.count( "transform" ) == 0 )
      blueprint["transform"] = json::array();
    if( blueprint.count( "meshview" ) == 0 )
      blueprint["meshview"] = json::array();
    if( blueprint.count( "logic" ) == 0 )
      blueprint["logic"] = json::array();
    if( blueprint.count( "rigidbody" ) == 0 )
      blueprint["rigidbody"] = json::array();
    if( blueprint.count( "joint" ) == 0 )
      blueprint["joint"] = json::array();
    json audioSource = R"({})"_json;
    blueprint["audiosource"].push_back( audioSource );

    json logic = R"({"function":"creature"})"_json;
    blueprint["logic"].push_back( logic );

    int appendageMin = Read<int>( param["appendage"], "min", 2 );
    int appendageMax = Read<int>( param["appendage"], "max", 4 );
    // ids
    int ID = 1;
    int parentRigidbody = -1;

    // params
    int appendages = std::uniform_int_distribution<int>( appendageMin, appendageMax )( randomengine );
    float radiusMax = std::uniform_real_distribution<float>( 0.5f, 1.5f )( randomengine );  // Read<float>( param, "radiusMax", 0.5f );
    float radiusMin = Read<float>( param, "radiusMin", 0.1f );
    float lengthFactor = std::uniform_real_distribution<float>( 0.5f, 2 )( randomengine );  // Read<float>( param, "lengthFactor", 1.0f );

    float headRadius = std::uniform_real_distribution<float>( radiusMax, radiusMax + 1 )( randomengine );
    CreateHead( blueprint, ID, vec2( 0, 0 ), headRadius, parentRigidbody );
    int headRigidbody = parentRigidbody;

    vec2 directions[appendages];
    float alpha = std::uniform_real_distribution<float>( 0, 2 * glm::pi<float>() )( randomengine );
    float inc = 2 * glm::pi<float>() / (float)appendages;
    for( size_t i = 0; i < appendages; i++ )
    {
      directions[i] = vec2( sinf( alpha ), cosf( alpha ) );
      alpha += inc;
      CreateAppendage( blueprint, ID, directions[i] * ( headRadius + radiusMax ) * lengthFactor, directions[i] * radiusMax * lengthFactor, radiusMax, radiusMin, headRigidbody );
    }

    // Resource::WriteJSON( "creature-"+to_string(ID)+".json", blueprint );
    GameObject* go = CreateGameObject( blueprint );
    if( go )
    {
      // destroy bodies when they touch
      std::vector<Rigidbody*>& bodies = go->GetComponents<Rigidbody>();
      for( auto& rb : bodies )
      {

        rb->contactCallback = [&]( Rigidbody* rbA, Rigidbody* rbB, b2Contact* contact )
        {
          // NOTE Do not destroy a b2Body within this callback
          if( rbA->gameObject == rbB->gameObject )
            return;

          b2Vec2 velDiff = rbA->body->GetLinearVelocity() - rbB->body->GetLinearVelocity();
          b2Vec2 veldiffnormal = velDiff;
          veldiffnormal.Normalize();
          b2WorldManifold worldManifold;
          contact->GetWorldManifold( &worldManifold );
          float tangentHit = b2Abs( b2Dot( worldManifold.normal, veldiffnormal ) );
          const float maxVel = 10;
          float vdiff = Clamp<float>( velDiff.Length(), 0, maxVel ) / maxVel;
          const float minMass = 0.1f;
          if( rbA->body->GetMass() > minMass && rbB->body->GetMass() > minMass )
            rbA->gameObject->GetComponent<AudioSource>()->Play( vdiff * tangentHit, false );
          // SDL_Log( "hit (veldiff tangent massdiff massA massB): %f %f %f %f", velDiff.Length(), tangentHit, rbA->body->GetMass(), rbB->body->GetMass() );

          // HACK use material to determine collision reaction
          std::string materialNameA;
          std::vector<MeshView*>& mvsA = rbA->gameObject->GetComponents<MeshView>();
          for( auto& mv : mvsA )
            if( mv->renderTransform == rbA->boundTransform )
              materialNameA = mv->material->name;

          // HACK use material to determine reaction
          std::string materialNameB;
          std::vector<MeshView*>& mvsB = rbB->gameObject->GetComponents<MeshView>();
          for( auto& mv : mvsB )
            if( mv->renderTransform == rbB->boundTransform )
              materialNameB = mv->material->name;

          if( KillThese.count( rbA->id ) == 0 )
            if( materialNameA.compare( "green" ) == 0 && materialNameB.compare( "simple" ) == 0 )
            {
              // KillThese[rbA->id] = rbA;
              rbA->gameObject->GetComponent<AudioSource>()->Play( 1, false );
            }

          if( KillThese.count( rbB->id ) == 0 )
            if( materialNameB.compare( "green" ) == 0 && materialNameA.compare( "simple" ) == 0 )
            {
              // KillThese[rbB->id] = rbB;
              rbB->gameObject->GetComponent<AudioSource>()->Play( 1, false );
            }
        };
      }
      /////
    }
    return go;
  }


  void Scene::Initialize( const json& scene )
  {
    world = new b2World( b2Vec2( 0, 0 ) );
    contactListener = new b2ContactListener();
    world->SetContactListener( contactListener );
    /*destructionListener = new DestructionListener();
    world->SetDestructionListener( destructionListener );*/
    debugDraw = new DebugDraw();
    debugDraw->Initialize( this );
    debugDraw->SetFlags( b2Draw::e_shapeBit | b2Draw::e_jointBit | b2Draw::e_centerOfMassBit );
    world->SetDebugDraw( debugDraw );

    defaultMaterial = new Material();
    defaultMaterial->Initialize( Resource::ReadJSON( "material/debug.json" ) );
    defaultMaterial->Validate();

    GameObject* cameraObject = CreateGameObject( Resource::ReadJSON( "object/default-camera.json" ) );
    cameraObject->SetPosition( vec3( 0, 0, 50 ) );
    cameraObject->GetComponent<Camera>()->Activate();

    if( scene.count( "object" ) )
    {
      // load objects from init json
      const json& allObject = scene.at( "object" );
      for( auto& obj : allObject )
      {
        json proto;
        GameObject* go = nullptr;
        if( obj.count( "object" ) > 0 )
        {
          string filename = obj.at( "object" );
          proto = Resource::ReadJSON( filename );
          if( proto.empty() )
            continue;
          go = CreateGameObject( proto );
          if( go == nullptr )
            continue;
          go->filename = filename;
        }
        else
        {
          go = CreateGameObject( obj );
          if( go == nullptr )
            continue;
        }
        if( obj.count( "state" ) )
          go->SetState( obj["state"] );
      }
    }

    /*
    // generate a bunch of creatures
    // std::uniform_real_distribution<float> distPosition( 0, 2.0f*glm::pi<float>() );
    const int creatureCount = 0;
    const float creatureRadius = 50;
    float inc = 2.0f*glm::pi<float>() / (float)creatureCount;
    float phi = 0;
    for( size_t i = 0; i < creatureCount; i++ )
    {
      GameObject* go = GenerateCreature( Resource::ReadJSON( "creature-params.json" ) );
      if( go )
      {
        //go->SetPosition( glm::vec3( 0, distPosition( randomengine ), distPosition( randomengine ) ) );
        go->SetPosition( glm::vec3( 0, sinf( phi )* creatureRadius, cosf( phi ) * creatureRadius) );
        phi += inc;
      }
    }
    */


    UpdateTransforms();
  }

  void Scene::AddTransform( int depth, ID_t id, Transform* tf )
  {
    transformLayer[depth][id] = tf;
  }
  void Scene::RemoveTransform( Transform* tf )
  {
    transformLayer[tf->depth].erase( tf->id );
  }

  void Scene::UpdateTransforms()
  {
    std::vector<Slot<Rigidbody>> allRigidbodyBuckets = Memory::GetPool<Rigidbody>().slot;
    std::vector<Rigidbody, Allocator<Rigidbody>>& allRigidbody = Memory::GetPool<Rigidbody>().pool;
    for( auto& bucket : allRigidbodyBuckets )
    {
      if( bucket.active )
      {
        const Rigidbody& rb = allRigidbody[bucket.index];
        const b2Transform& xf = rb.body->GetTransform();
        b2Vec2 pos = xf.p;
        b2Rot rot = xf.q;
        b2Vec2 rx = rot.GetXAxis();
        b2Vec2 ry = rot.GetYAxis();
        Transform& transform = *rb.boundTransform;
        transform.WorldMatrix = mat4(
            rx.x, rx.y, 0, 0,
            ry.x, ry.y, 0, 0,
            1, 0, 0, 0,
            pos.x, pos.y, 0, 1 );
        // transform.UpdateLocalMatrix();
        transform.updatedThisFrame = true;
      }
    }

    // Update matrix transforms by depth level. Parent-to-child update.
    for( auto lpair = transformLayer.begin(); lpair != transformLayer.end(); ++lpair )
    {
      for( auto idxf = lpair->second.begin(); idxf != lpair->second.end(); ++idxf )
      {
        Transform* transform = idxf->second;
        if( transform->GetParent() == nullptr )
        {
          if( !transform->updatedThisFrame )
          {
            // Has no parent. Local transform == world transform.
            transform->WorldMatrix = transform->LocalMatrix;
          }
          // reset
          transform->updatedThisFrame = false;
        }
        else
        {
          transform->WorldMatrix = transform->GetParent()->WorldMatrix * transform->LocalMatrix;
        }
      }
    }
  }


  void Scene::UpdateScene()
  {
    // fixed-step update ( http://gafferongames.com/game-physics/fix-your-timestep/ )
    // updateAccumulator += 3::deltaTick;
    // while( updateAccumulator >= targetUpdateTimestep )
    //{
    // updateAccumulator -= targetUpdateTimestep;
    // float dT = ( (float)targetUpdateTimestep ) / 1000.0f;
    float DeltaTime = Time::deltaTime;

    if( Camera::Current() != nullptr )
    {
      Camera::Current()->Update( DeltaTime );
    }

    std::vector<Slot<Logic>> allLogicSlots = Memory::GetPool<Logic>().slot;
    std::vector<Logic, Allocator<Logic>>& allLogic = Memory::GetPool<Logic>().pool;
    for( auto& bucket : allLogicSlots )
    {
      if( bucket.active )
      {
        Logic& comp = allLogic[bucket.index];
        comp.Update( DeltaTime );
      }
    }

    world->Step( 1.0f / 60.0f, 10, 10 );
    UpdateTransforms();

    // destroy bodies when they touch
    for( auto& pair : KillThese )
      Component::Destroy( pair.second );
    KillThese.clear();
  }

  void Scene::RenderScene( float dT )
  {
    Camera* activeCamera = Camera::Current();
    if( activeCamera )
    {

      // TODO batch meshviews by material
      // defaultMaterial->Activate();
      // activeCamera->PreRender();

      std::vector<Slot<MeshView>> allMeshViewBuckets = Memory::GetPool<MeshView>().slot;
      std::vector<MeshView, Allocator<MeshView>>& allMeshView = Memory::GetPool<MeshView>().pool;
      for( auto& bucket : allMeshViewBuckets )
      {
        if( bucket.active )
        {
          MeshView& comp = allMeshView[bucket.index];
          // TODO batching
          comp.material->Activate();
          activeCamera->PreRender();
          //
          comp.UpdateView();
        }
      }

      if( bRenderDebug )
      {
        debugDraw->PreDraw();
        activeCamera->PreRender();
        world->DrawDebugData();
        debugDraw->PostDraw();
      }
    }
  }

  void Scene::DebugDumpLayers()
  {
    SDL_Log( "DebugDumpLayers" );
    for( auto lpair = transformLayer.begin(); lpair != transformLayer.end(); ++lpair )
    {
      for( auto idxf = lpair->second.begin(); idxf != lpair->second.end(); ++idxf )
      {
        stringstream ss;
        if( idxf->second->GetParent() == nullptr )
          ss << "DEPTH:" << idxf->second->GetDepth() << " ID:" << idxf->first << " PARENT:null";
        else
          ss << "DEPTH:" << idxf->second->GetDepth() << " ID:" << idxf->first << " PARENT:" << idxf->second->GetParent()->id;
        SDL_Log( "%s", ss.str().c_str() );
      }
    }
  }

  void Scene::Save()
  {
    json obs = json::array();
    for( auto& obj : objects )
    {
      json ob = *obj;
      obs.push_back( ob );
    }
    json scene;
    scene["object"] = obs;
    Resource::WriteCBOR( "scenebin", scene );
  }

  void Scene::Load( const std::string& name )
  {
    json test = Resource::ReadCBOR( name );
    SDL_Log( "test: %s", test.dump( 2 ).c_str() );

    GameObject go = test;
    // TODO destroy b2World
    // Initialize( test );
  }
}  // namespace Spaghetti
