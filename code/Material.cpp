#include "Material.h"

#include <algorithm>

#include "SDL2/SDL_log.h"
#include "SDL2/SDL_opengl.h"
#include "SDL2/SDL_opengl_glext.h"
#include "SDL2/SDL_image.h"
#include <cstdint>
#include <fstream>
#include <glm/mat4x4.hpp>  // glm::mat4
#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

#include "Debug.h"
#include "Resource.h"
#include "Serialize.h"

namespace Spaghetti
{
  using namespace std;
  using json = nlohmann::json;
  using string = string;

  unordered_map<string, Material*> Material::allMaterials;
  Material* instance = nullptr;


  void Material::Shader::Source( const string& s )
  {
    ID = glCreateShader( type );
    // SDL_Log( "shader ctor test: %s %d", this->key.c_str(), this->type );
    source = s;
    const GLchar* str[] = { source.c_str() };
    const GLint length = source.length();
    glShaderSource( ID, 1, str, &length );
    glCompileShader( ID );
    // LogStatus();
  }

  void Material::Shader::Destroy()
  {
    glDeleteShader( ID );
  }

  void LogParam( GLuint ID, const char* name, GLenum param )
  {
    GLint out;
    glGetShaderiv( ID, param, &out );
    SDL_LogMessage( SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_INFO, "%s: %d", name, out );
  }

  void Material::Shader::LogStatus()
  {
    SDL_Log( "Shader: %s", key.c_str() );
    LogParam( ID, "GL_SHADER_TYPE", GL_SHADER_TYPE );
    // LogParam( ID, "GL_DELETE_STATUS", GL_DELETE_STATUS );
    LogParam( ID, "GL_COMPILE_STATUS", GL_COMPILE_STATUS );
    // LogParam( ID, "GL_INFO_LOG_LENGTH", GL_INFO_LOG_LENGTH );
    // LogParam( ID, "GL_SHADER_SOURCE_LENGTH", GL_SHADER_SOURCE_LENGTH );
    GLint length;
    glGetShaderiv( ID, GL_INFO_LOG_LENGTH, &length );
    if( length > 0 )
    {
      GLchar infoLog[length];
      glGetShaderInfoLog( ID, length, NULL, infoLog );
      SDL_LogMessage( SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_INFO, "Shader Info Log: %s", infoLog );
    }
  }


  bool Material::Initialize( const nlohmann::json& material )
  {
    programID = glCreateProgram();

    name = Read<string>( material, "name", "simple" );

    string vert;
    if( material.count( "vert" ) )
    {
      vert = material.at( "vert" ).get<std::string>();
    }
    else if( material.count( "file_vert" ) )
    {
      vert = Resource::ReadString( "shader/" + material.at( "file_vert" ).get<string>() );
    }
    if( vert.length() > 0 )
    {
      Shader vertexShader;
      vertexShader.key = name;
      vertexShader.type = GL_VERTEX_SHADER;
      vertexShader.Source( vert.data() );
      Attach( vertexShader );
      vertexShader.LogStatus();
    }
    else
    {
      SDL_LogMessage( SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_ERROR, "Failed to load vertex shader from file: %s", vert.c_str() );
    }

    string frag;
    if( material.count( "frag" ) )
    {
      frag = material.at( "frag" ).get<std::string>();
    }
    else if( material.count( "file_frag" ) )
    {
      frag = Resource::ReadString( "shader/" + material.at( "file_frag" ).get<string>() );
    }
    if( frag.length() > 0 )
    {
      Shader fragmentShader;
      fragmentShader.key = name;
      fragmentShader.type = GL_FRAGMENT_SHADER;
      fragmentShader.Source( frag.data() );
      Attach( fragmentShader );
      fragmentShader.LogStatus();
    }
    else
    {
      SDL_LogMessage( SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_ERROR, "Failed to load fragment shader from file: %s", frag.c_str() );
    }

    glLinkProgram( programID );
    GLint params;
    glGetProgramiv( programID, GL_LINK_STATUS, &params );
    SDL_Log( "Material %s GL_LINK_STATUS: %d", name.c_str(), params );

    if( material.count( "property" ) )
    {
      for( auto it = material["property"].begin(); it != material["property"].end(); ++it )
      {
        string key = it.key();
        const json& value = it.value();

        if( value.is_boolean() )
        {
          propertyBool[key] = value;
        }
        // else
        // if( value.is_string() )
        // {
        //   property[key] = value;
        // }
      }
    }

    if( material.count( "uniform" ) )
    {
      for( auto it = material["uniform"].begin(); it != material["uniform"].end(); ++it )
      {
        string name = it.key();
        // must call glGetUniformLocation after glLinkProgram
        GLint glLocation = glGetUniformLocation( programID, name.c_str() );

        const json& value = it.value();
        if( value.is_number() )
        {
          uniform1f[name] = make_tuple( glLocation, value.get<float>() );
        }
        else if( value.is_array() )
        {
          vector<float> fv = value.get<vector<float>>();
          if( fv.size() == 4 )
          {
            array<float, 4> a4;
            copy_n( fv.begin(), 4, a4.begin() );
            uniform4fv[name] = make_tuple( glLocation, a4 );
            // uniform4fv.emplace(make_pair(name, array<float,4>());
          }
          // if( fv.size() == 16 )
        }
        else if( name == "mainTexture" )
        {
          std::vector<uint8_t> buffer;
          Resource::ReadBuffer( value, buffer );
          // Read the PNG data out of the file buffer.
          SDL_RWops* rwo = SDL_RWFromConstMem( buffer.data(), buffer.size() );
          SDL_Surface* surface = IMG_LoadTyped_RW( rwo, 0, "PNG" );

          GLuint textureID;
          glGenTextures( 1, &textureID );
          glBindTexture( GL_TEXTURE_2D, textureID );
          glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
          glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
          glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
          glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
          GLuint type = GL_RGBA;
          glTexImage2D( GL_TEXTURE_2D, 0, type, surface->w, surface->h, 0, type, GL_UNSIGNED_BYTE, (void*)surface->pixels );
          glBindTexture( GL_TEXTURE_2D, 0 );

          glGenerateMipmap(GL_TEXTURE_2D);
        }
      }
    }
    if( material.count( "attribute" ) )
    {
      for( auto it = material["attribute"].begin(); it != material["attribute"].end(); ++it )
      {
        string name = it.key();
        // must call glGetAttribLocation after glLinkProgram
        GLint glLocation = glGetAttribLocation( programID, name.c_str() );
        // glBindAttribLocation( programID, 0, "position" );
        string type = it.value();
        if( type == "vec3" )
          attribute3f[name] = glLocation;
        if( type == "vec2" )
          attribute2f[name] = glLocation;
      }
    }

    allMaterials.emplace( make_pair( name, this ) );
    return true;
  }

  bool Material::Validate()
  {
    glValidateProgram( programID );
    GLint validated;
    glGetProgramiv( programID, GL_VALIDATE_STATUS, &validated );
    SDL_Log( "GL_VALIDATE_STATUS: %d", validated );
    GLint activeUniforms;
    glGetProgramiv( programID, GL_ACTIVE_UNIFORMS, &activeUniforms );
    SDL_Log( "Shader active uniforms: %d", activeUniforms );
    GLint activeAttributes;
    glGetProgramiv( programID, GL_ACTIVE_ATTRIBUTES, &activeAttributes );
    SDL_Log( "Shader active attributes: %d", activeAttributes );
    string str;
    GLint length = 0;
    glGetProgramiv( programID, GL_INFO_LOG_LENGTH, &length );
    if( length > 0 )
    {
      GLchar infoLog[length];
      glGetProgramInfoLog( programID, length, NULL, infoLog );
      SDL_LogMessage( SDL_LOG_CATEGORY_RENDER, SDL_LOG_PRIORITY_INFO, "Shader Info Log: %s", infoLog );
    }
    return ( validated > 0 );
  }

  void Material::Activate()
  {
    instance = this;
    glUseProgram( programID );

    if( uniform1f.size() > 0 )
    {
      for( const auto& element : uniform1f )
      {
        GLint location = 0;
        float value = 0.0f;
        tie( location, value ) = element.second;
        glUniform1f( location, value );
        if( Debug::CheckGLErrors( __FILE__, __LINE__ ) )
        {
          SDL_Log( "bad uniform1f %s", element.first.c_str() );
        }
      }
    }
    if( uniform4fv.size() > 0 )
    {
      for( const auto& element : uniform4fv )
      {
        GLint location = 0;
        array<float, 4> ray;
        tie( location, ray ) = element.second;
        glUniform4fv( location, 1, ray.data() );
        if( Debug::CheckGLErrors( __FILE__, __LINE__ ) )
        {
          SDL_Log( "bad uniform4f %s", element.first.c_str() );
        }
      }
    }

    if( propertyBool.count( "DepthTest" ) )
    {
      if( propertyBool["DepthTest"] && !glIsEnabled( GL_DEPTH_TEST ) )
        glEnable( GL_DEPTH_TEST );
      else if( !propertyBool["DepthTest"] && glIsEnabled( GL_DEPTH_TEST ) )
        glDisable( GL_DEPTH_TEST );
    }

    if( propertyBool.count( "Blend" ) )
    {
      if( propertyBool["Blend"] && !glIsEnabled( GL_BLEND ) )
      {
        glEnable( GL_BLEND );
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
      }
      else if( !propertyBool["Blend"] && glIsEnabled( GL_BLEND ) )
        glDisable( GL_BLEND );
    }

    // glLineWidth( 2 );

    // glEnable( GL_CULL_FACE );
    // glCullFace( GL_BACK );
    // glFrontFace( GL_CCW );
    // glDepthFunc( GL_LESS );
    // GLuint RenderMode = GL_LINES;
    // glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );  //GL_FILL
    // glEnable( GL_PRIMITIVE_RESTART );
    // glPrimitiveRestartIndex( 3 );
    if( Debug::CheckGLErrors( __FILE__, __LINE__ ) )
    {
      SDL_Log( "Material: %s", name.c_str() );
    }
  }

  void Material::Destroy()
  {
    for( auto& s : attachedShaders )
      s.Destroy();
    glDeleteProgram( programID );
  }

  void Material::Attach( Shader shader )
  {
    attachedShaders.push_back( shader );
    glAttachShader( programID, shader.ID );
    Debug::CheckGLErrors( __FILE__, __LINE__ );
  }

  void Material::Detach( Shader shader )
  {
    // attachedShader.remove( shader );
    glDetachShader( programID, shader.ID );
    Debug::CheckGLErrors( __FILE__, __LINE__ );
  }

  Material* Material::GetCurrent()
  {
    // if( instance == nullptr )
    // {
    //   instance = new Material();
    // }
    return instance;
  }

  Material* Material::GetMaterial( const std::string& materialName )
  {
    if( allMaterials.count( materialName ) )
      return allMaterials[materialName];
    // SDL_Log("Material not found: %s", materialName.c_str() );
    return nullptr;
  }


  /*
    bool Material::GetShader( const string& key, Shader* out )
    {
      SDL_Log( "attachedShaders count: %d", (int)attachedShaders.size() );
      for( auto& s : attachedShaders )
      {
        SDL_Log( "GetShader %s", s->GetKey().c_str() );
        if( s->GetKey() == key )
        {
          *out = *s;
          return true;
        }
      }
      return false;
    }
  */

  /*
    SDL_Surface* Material::surface = nullptr;
    GLuint Material::textureID = 0;
    GLuint Material::samplerID = 0;
    static void Material::LoadTexture( const string& sourceImage )
    {
      SDL_Log( "Loading texture %s", sourceImage.c_str() );
      SDL_ClearError();
      surface = IMG_Load( sourceImage.c_str() );
      if( surface )
      {
        glGenTextures( 1, &textureID );
        glBindTexture( GL_TEXTURE_2D, textureID );
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        GLuint type = ( surface->format->BytesPerPixel == 4 ) ? GL_RGBA : GL_RGB;
        glTexImage2D( GL_TEXTURE_2D, 0, type, surface->w, surface->h, 0, type, GL_UNSIGNED_BYTE, (void*)surface->pixels );
        glBindTexture( GL_TEXTURE_2D, 0 );
      }
      else
      {
        SDL_Log( "%s", IMG_GetError() );
      }
    }
*/


  /*
      static void InitializeSampler()
      {
        glGenSamplers( 1, &samplerID );
        glSamplerParameteri( samplerID, GL_TEXTURE_WRAP_S, GL_REPEAT );
        glSamplerParameteri( samplerID, GL_TEXTURE_WRAP_T, GL_REPEAT );
        glSamplerParameteri( samplerID, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glSamplerParameteri( samplerID, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
        //glSamplerParameterf(samplerID, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);

        BindSampler();
      }

      static void BindSampler()
      {
        //glActiveTexture( GL_TEXTURE0 )
        glBindTexture( GL_TEXTURE_2D, textureID );
        // texture unit 0
        glBindSampler( 0, samplerID );
      }

      static void UnbindSampler()
      {
        glBindSampler( 0, 0 );
      }
  */


  /*
  // TEXTURE
  unsigned int num_gl_id = 0;
  vector<GLuint> all_textures;

  void Uninitialize()
  {
    for( int i = 0; i < all_textures.size(); i++ )
      glDeleteTextures( 1, &all_textures[i] );
  }

  unsigned int LoadTexture( const string& filename )
  {
    SDL_RWops* rwo = SDL_RWFromFile( filename.c_str(), "r" );
    // SDL_RWops* rwo = SDL_RWFromConstMem
    SDL_Surface* surface = IMG_LoadTyped_RW( rwo, 0, "PNG" );
    if( !surface || !surface->pixels )
    {
      SDL_LogWarn( SDL_LOG_CATEGORY_RENDER, "- texture load failed:(%s) (%s)", IMG_GetError(), filename.c_str() );
      SDL_FreeSurface( surface );
      return 0;
    }
    int width = surface->w;
    int height = surface->h;
    int bytesPerPixel = surface->format->BytesPerPixel;
    GLint type = GL_BGR;
    if( bytesPerPixel == 4 )
      type = GL_BGRA;

    // create a temporary buffer to copy our data from the SDL_Surface
    uint8_t* temp = new uint8_t[width * height * bytesPerPixel];
    SDL_SetColorKey( surface, SDL_SRCCOLORKEY, SDL_MapRGBA( surface->format, 255, 255, 255, 0 ) );
    memcpy( temp, surface->pixels, ( width * height * bytesPerPixel ) );
    SDL_FreeSurface( surface );

    if( IMG_GetError() )
      SDL_Log( "LoadTexture():first mark: %s", filename.c_str() );

    FlipVertical( temp, width, height, bytesPerPixel );

    Texture t;
    t.imageData = temp;
    t.w = width;
    t.h = height;
    t.channels = bytesPerPixel;

    glGenTextures( 1, &t.gl_id );
    all_textures.push_back( t.gl_id );

    glBindTexture( GL_TEXTURE_2D, t.gl_id );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR );
    glTexImage2D(GL_TEXTURE_2D, 0, type, t->w, t->h, 0, type, GL_UNSIGNED_BYTE, t->imageData);

    if( Debug::CheckGLErrors( __FILE__, __LINE__ ) )
      log_write( ":error source: texture params." );



    try
    {
      glTexImage2D(GL_TEXTURE_2D, 0, type, t->w, t->h, 0, type, GL_UNSIGNED_BYTE, t->imageData);
      gluBuild2DMipmaps( GL_TEXTURE_2D, type, t.w, t.h, type, GL_UNSIGNED_BYTE, t.imageData );
      if( checkForError() )
      {
        log_write( ":error source: gluBuild2DMaps. type=%d GL_RGBA=%d GL_RGB=%d", type, GL_RGBA, GL_RGB );
      }
    }
    catch( ... )
    {
      log_write( LOG_FILE, "- Error generating gl texture: (%s) w:%d h:%d", filename.c_str(), t.w, t.h );
      return 0;
    }


    Debug::CheckGLErrors( __FILE__, __LINE__ );

    return t.gl_id;
  }
  */


  /*
  void FlipVertical( uint8_t* img, int w, int h, int bytesPerPixel )
  {
    uint8_t* temp = new uint8_t[w * h * bytesPerPixel];
    memset( temp, 0, w * h * bytesPerPixel );
    for( int r = 0; r < h; r++ )  // go row-by-row, copy in inverse order
      memcpy( temp + ( r * w * bytesPerPixel ), img + ( ( h - r - 1 ) * w * bytesPerPixel ), ( w * bytesPerPixel ) );
    memcpy( img, temp, w * h * bytesPerPixel );
    delete[] temp;
  }
    void switch_R_and_B( char* img, int w, int h, int bpp )
    {
      char tempRGB;
      // swap the R and B values to get RGB since the bitmap color format is in BGR
      for( int i = 0; i < ( w * h * bpp ); i += bpp )
      {
        tempRGB = img[i];
        img[i] = img[i + 2];
        img[i + 2] = tempRGB;
      }
    }*/
}  // namespace Spaghetti
