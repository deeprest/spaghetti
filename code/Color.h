#pragma once
#include "Vec4.h"

typedef vec4 color;


namespace Color
{
  // inline color RandColor( bool withAlpha = false ){ return color( random_f(),
  // random_f(), random_f(), (withAlpha)? random_f() : 1.0f ); }
  static color blend( color& c1, color& c2, float alpha )
  {
    return ( c1 * ( 1.0f - alpha ) + ( c2 * alpha ) );
  }

  static color CLEAR( 1, 1, 1, 0 );

  // grayscale
  static color WHITE( 1, 1, 1, 1 );
  static color GRAY_75( .75f, .75f, .75f, 1 );
  static color GRAY( .5f, .5f, .5f, 1 );
  static color GRAY_50( .5f, .5f, .5f, 1 );
  static color GREY( .5f, .5f, .5f, 1 );
  static color GRAY_25( .25f, .25f, .25f, 1 );
  static color GRAY_10( .1f, .1f, .1f, 1 );
  static color BLACK( 0, 0, 0, 1 );

  // ROYGBIV, light-to-dark
  static color RED( 1, 0, 0, 1 );
  static color DARK_RED( 0.5f, 0, 0, 1 );
  static color RED_ORANGE( 1, .3f, .1f, 1 );
  static color ORANGE( 1, .5f, 0, 1 );
  static color LIGHT_ORANGE( 1, .8f, .5f, 1 );
  static color ORANGE_YELLOW( 1, .75f, 0, 1 );
  static color YELLOW_ORANGE( 1, .75f, 0, 1 );
  static color YELLOW( 1, 1, 0, 1 );
  static color YELLOW_GREEN( .7f, 1, .2f, 1 );
  static color GREEN( 0, 1, 0, 1 );
  static color BLUE_GREEN( 0, 1, 1, 1 );
  static color LIGHT_BLUE( .5, .5, 1, 1 );
  static color BLUE( 0, 0, 1, 1 );
  static color PURPLE( 1, 0, 1, 1 );
  static color VIOLET( 1, 0, 1, 1 );
}
