#pragma once
#include "Allocator.h"
#include "Common.h"
#include "SDL2/SDL_log.h"
#include <vector>

namespace Spaghetti
{
  extern ID_t NextGlobalID;

  struct PoolType
  {
    ID_t id = 0;
    size_t index = 0;
  };

  template <typename T>
  struct Slot
  {
    size_t index = 0;
    bool active = false;
    bool isFree = false;
  };

  template <typename T>
  class Pool
  {
  public:
    size_t freecount = 0;
    std::vector<Slot<T>> slot;
    std::vector<T, Spaghetti::Allocator<T>> pool;

    void Initialize( size_t poolsize )
    {
      pool.reserve( poolsize );
      slot.reserve( poolsize );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Mem total: %zu MB", Spaghetti::total / ( 1024 * 1024 ) );
    }

    T* New()
    {
      const char* typnam = typeid( T ).name();
      if( pool.size() + 1 > pool.capacity() )
      {
        // 'freecount' is the count of allocated-but-unused objects.
        if( freecount > 0 )
        {
          // find an unused slot
          for( auto& bu : slot )
          {
            if( bu.isFree )
            {
              bu.isFree = false;
              bu.active = true;
              freecount--;
              SDL_LogVerbose( LOG_CATEGORY_MEMORY, "CREATE (RECYCLE) %s slot:%zu count:%zu capacity:%zu",
                              typnam, bu.index, pool.size(), pool.capacity() );
              return &pool[bu.index];
            }
          }
        }
        // No free buckets found
        // A new capacity would invalidate all existing pointers
        // If reallocating here, then preserve object references with handles (offsets from base) instead of pointers.
        // transform.reserve( transform.capacity() * 2 );
        SDL_LogDebug( LOG_CATEGORY_MEMORY, "*** No free buckets in Pool<%s> ***", typnam );
        return nullptr;
      }
      // There are free slots in the pool
      pool.emplace_back();
      slot.emplace_back();
      size_t index = pool.size() - 1;
      slot[index].index = index;
      slot[index].active = true;
      // SDL_Log( "{{type}} count:%zu capacity:%zu", pool.Pool.size(), pool.Pool.capacity() );
      T& pt = pool.data()[index];
      pt.id = NextGlobalID++;
      pt.index = index;
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> New() count:%zu freecount:%zu capacity:%zu",
                    typnam, pool.size(), freecount, pool.capacity() );
      return &pt;
    }

    void MarkForDestruction( T* cmp )
    {
      slot[cmp->index].active = false;
    }

    void Destroy( T* cmp )
    {
      slot[cmp->index].isFree = true;
      slot[cmp->index].active = false;
      freecount++;

      const char* typnam = typeid( T ).name();
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> Destroy() index:%zu count:%zu freecount:%zu capacity:%zu",
                    typnam, cmp->index, pool.size(), freecount, pool.capacity() );
    }

    void Dump()
    {
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<{{type}}> Dump() count:%zu freecount:%zu capacity:%zu",
                    pool.size(), freecount, pool.capacity() );
    }
  };
}
