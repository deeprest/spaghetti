#pragma once
#include "SDL2/SDL_log.h"
#include "SDL2/SDL_platform.h"
#ifdef __LINUX__
#elif __MACOSX__
#elif __WINDOWS__
// mingw
#else
// #error "Unsupported platform"
#endif

namespace Spaghetti
{
  typedef uint64_t ID_t;

  enum
  {
    LOG_CATEGORY_SCRIPT = SDL_LOG_CATEGORY_CUSTOM,
    LOG_CATEGORY_ASSET,
    LOG_CATEGORY_MEMORY
  };

}  // namespace Spaghetti
