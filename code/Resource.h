#pragma once

#include <string>
#include "json.hpp"
#include "SDL2/SDL_surface.h"

namespace Spaghetti
{
  namespace Resource
  {
    void Initialize();

    void ReadBuffer( const std::string& filename, std::vector<uint8_t>& buffer);
    nlohmann::json ReadJSON( const std::string& filename );
    std::string ReadString( const std::string& filename );
    nlohmann::json ReadCBOR( const std::string& filename );

    void WriteJSON( const std::string& filename, const nlohmann::json& obj );
    void WriteCBOR( const std::string& filename, const nlohmann::json& obj );
    void WritePNG( SDL_Surface* surface, const std::string& filename );

    extern std::string basepath;
    extern std::string persistentPath;

  }
}
