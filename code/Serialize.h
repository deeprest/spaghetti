#pragma once
#include "json.hpp"
#include <string>

#include <iterator>
#include <sstream>
#include <string>
#include <vector>

namespace Spaghetti
{
  using json = nlohmann::json;
  using string = std::string;

  // template <typename T>
  // void split( const string& str, char delimeter, T output )
  // {
  //   stringstream ss( str );
  //   string token;
  //   while( std::getline( ss, token, delimeter ) )
  //   {
  //     *( output++ ) = token;
  //   }
  // }
  //
  // vector<string> split( const string& str, char delimeter )
  // {
  //   std::vector<string> elements;
  //   split( str, delimeter, back_inserter( elements ) );
  //   return elements;
  // }


  // one-line safe read with default value if key does not exist.
  template <typename T>
  inline T Read( const json& obj, const string& key, T defaultValue )
  {
    if( obj.count( key ) )
      return obj[key].get<T>();
    return defaultValue;
  }

}
