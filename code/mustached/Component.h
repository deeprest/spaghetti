#pragma once

#include <functional>
#include <unordered_map>
// #include <typeinfo>
// #include <typeindex>
// #include <cstddef> //for offsetof
#include "Common.h"
#include "MemoryPool.h"
#include "glm/glm.hpp"
#include "json.hpp"
#include <string>

namespace Spaghetti
{
  using json = nlohmann::json;
  using string = std::string;

  struct GameObject;
  struct Scene;

  // SPAGHETTI_TEMPLATE
  struct Transform;
  struct Camera;
  struct MeshView;
  struct Rigidbody;
  struct Joint;
  struct Input;
  struct Logic;
  struct AudioSource;

  struct Component : public PoolType
  {
    GameObject* gameObject = nullptr;
    Scene* scene = nullptr;

    // return value of false prevents the entire gameobject from being created.
    bool Initialize( const nlohmann::json& params ) { return true; }
    // use this to clean up before recycle
    inline void OnDestroy() {}

    template <typename T>
    static T* GetComponentByID( ID_t id );
    template <typename T>
    static void Destroy( T* cmp );
  };

  struct GameObject
  {
  private:
    // SPAGHETTI_TEMPLATE
    std::vector<Transform*> TransformComponents;
    std::vector<Camera*> CameraComponents;
    std::vector<MeshView*> MeshViewComponents;
    std::vector<Rigidbody*> RigidbodyComponents;
    std::vector<Joint*> JointComponents;
    std::vector<Input*> InputComponents;
    std::vector<Logic*> LogicComponents;
    std::vector<AudioSource*> AudioSourceComponents;

  public:
    string filename = "object/default.json";

    template <typename T>
    std::vector<T*>& GetComponents();

    template <typename T>
    T* GetComponent();

    // template<typename T>
    // T* AddComponent();

    // SPAGHETTI_TEMPLATE
    void RemoveComponent( Transform* cmp );
    void RemoveComponent( Camera* cmp );
    void RemoveComponent( MeshView* cmp );
    void RemoveComponent( Rigidbody* cmp );
    void RemoveComponent( Joint* cmp );
    void RemoveComponent( Input* cmp );
    void RemoveComponent( Logic* cmp );
    void RemoveComponent( AudioSource* cmp );

    template <typename T>
    const std::vector<T*>& GetComponentsConst() const;

    template <typename T>
    T* GetComponentConst() const;


    void SetPosition( glm::vec3 pos );
    void SetState( const json& state );

    static void Destroy( GameObject* go );
    static void DestroyImmediate( GameObject* go );
    std::function<void()> OnDestroy;
  };

  void to_json( json& j, const GameObject& obj );
  void from_json( const json& j, GameObject& obj );

}  // namespace Spaghetti
