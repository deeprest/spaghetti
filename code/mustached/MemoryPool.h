#pragma once

#include <cstddef>
#include <vector>
#include <memory>
#include "SDL2/SDL_log.h"
#include "Allocator.h"
#include "Common.h"
#include "Pool.h"

namespace Spaghetti
{
#if 0
  extern ID_t NextGlobalID;

  struct PoolType
  {
    ID_t id = 0;
    size_t index = 0;
  };

  template <typename T>
  struct Slot
  {
    size_t index = 0;
    bool active = false;
    bool isFree = false;
  };

  template <typename T>
  class Pool
  {
  public:
    size_t freecount = 0;
    std::vector<Slot<T>> slot;
    std::vector<T, Spaghetti::Allocator<T>> pool;

    void Initialize( size_t poolsize );
    T* New();
    void Destroy( T* pooltype );
    void Dump();
    void MarkForDestruction( T* pooltype );
  };
#endif

  namespace Memory
  {
    void InitializePool( size_t poolsize );

    template<typename T>
    Pool<T>& GetPool();

    template<typename T>
    T* Create();

    template<typename T>
    void MarkForDestruction( T* comp );

    template<typename T>
    void Destroy( T* comp );

    void Dump();

    void CollectGarbage();
  }


}
