#include "MemoryPool.h"

#include <iostream>
#include <memory>
#include <new>
#include <string>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <vector>

#include "Box2D/Box2D.h"
#include "SDL2/SDL_log.h"
#include "physfs.h"

#include "Allocator.h"
#include "AudioSource.h"
#include "Camera.h"
#include "Component.h"
#include "Input.h"
#include "Joint.h"
#include "Logic.h"
#include "Mesh.h"
#include "MeshView.h"
#include "Rigidbody.h"
#include "Pool.h"
#include "Resource.h"
#include "Scene.h"
#include "Transform.h"

#if false
void* operator new(std::size_t sz)
{
  // SDL_PollEvent() calls SDL_PumpEvents(). On MacOS, this process receives many calls to
  // new() for input events. This originates in macho/kernel, a kevent is sent using XPC
  // and a std::vector of ulongs is allocated with default new() for mouse and key events.
  // SDL_Log("new called, size = %zu",sz);
  return std::malloc(sz);
}
void operator delete(void* ptr) noexcept
{
  // SDL_Log("delete called");
  std::free(ptr);
}
#endif

// Memory allocators. Modify these to use your own allocator.
void* b2Alloc( int32 size )
{
  // SDL_Log("Box2d alloc: %d", size );
  // total += size;
  return malloc( size );
}

void b2Free( void* mem )
{
  // SDL_Log("Box2d free");
  free( mem );
}

// You can modify this to use your logging facility.
void b2Log( const char* string, ... )
{
  va_list args;
  va_start( args, string );
  SDL_Log( string, args );
  // vprintf(string, args);
  va_end( args );
}

namespace Spaghetti
{
  // Allocator.h
  size_t total;

  using namespace std;
  using json = nlohmann::json;
  using std::placeholders::_1;

  // Using a void* because tha map values are vector, and they hold different types.
  // On lookup, use the key-type to cast the value to a vector of the correct type.
  unordered_map<type_index, void*> typemap;
  unordered_map<type_index, string> typeToName;

  // SPAGHETTI_TEMPLATE
  // Components
  Pool<Transform> TransformPool;
  Pool<Camera> CameraPool;
  Pool<MeshView> MeshViewPool;
  Pool<Rigidbody> RigidbodyPool;
  Pool<Joint> JointPool;
  Pool<Input> InputPool;
  Pool<Logic> LogicPool;
  Pool<AudioSource> AudioSourcePool;
  // PoolTypes
  Pool<Mesh> MeshPool;

  std::vector<GameObject*> garbage;
  unordered_map<ID_t, PoolType*> allPoolTypes;
  ID_t NextGlobalID = 1;

#if 0
  template <typename T>
  void Pool<T>::Initialize( size_t poolsize )
  {
    pool.reserve( poolsize );
    slot.reserve( poolsize );
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Mem total: %zu MB", Spaghetti::total / ( 1024 * 1024 ) );
  }

  template <typename T>
  T* Pool<T>::New()
  {
    const char* typnam = typeid( T ).name();
    if( pool.size() + 1 > pool.capacity() )
    {
      // 'freecount' is the count of allocated-but-unused objects.
      if( freecount > 0 )
      {
        // find an unused slot
        for( auto& bu : slot )
        {
          if( bu.isFree )
          {
            bu.isFree = false;
            bu.active = true;
            freecount--;
            SDL_LogVerbose( LOG_CATEGORY_MEMORY, "CREATE (RECYCLE) %s slot:%zu count:%zu capacity:%zu",
                            typnam, bu.index, pool.size(), pool.capacity() );
            return &pool[bu.index];
          }
        }
      }
      // No free buckets found
      // A new capacity would invalidate all existing pointers
      // If reallocating here, then preserve object references with handles (offsets from base) instead of pointers.
      // transform.reserve( transform.capacity() * 2 );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "*** No free buckets in Pool<%s> ***", typnam );
      return nullptr;
    }
    // There are free slots in the pool
    pool.emplace_back();
    slot.emplace_back();
    size_t index = pool.size() - 1;
    slot[index].index = index;
    slot[index].active = true;
    // SDL_Log( " count:%zu capacity:%zu", pool.Pool.size(), pool.Pool.capacity() );
    T& pt = pool.data()[index];
    pt.id = NextGlobalID++;
    pt.index = index;
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> New() count:%zu freecount:%zu capacity:%zu",
                  typnam, pool.size(), freecount, pool.capacity() );
    return &pt;
  }

  template<typename T>
  void Pool<T>::MarkForDestruction( T* cmp )
  {
    slot[cmp->index].active = false;
  }

  template <typename T>
  void Pool<T>::Destroy( T* cmp )
  {
    slot[cmp->index].isFree = true;
    slot[cmp->index].active = false;
    freecount++;

    const char* typnam = typeid( T ).name();
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> Destroy() index:%zu count:%zu freecount:%zu capacity:%zu",
                  typnam, cmp->index, pool.size(), freecount, pool.capacity() );
  }

  template <typename T>
  void Pool<T>::Dump()
  {
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<> Dump() count:%zu freecount:%zu capacity:%zu",
                  pool.size(), freecount, pool.capacity() );
  }

#endif

  namespace Memory
  {
    /*template<typename T>
    Pool<T>& GetPool()
    {
            // On lookup, use the key-type to cast the value to a vector of the correct type.
            return *((Pool<T>*)(typemap[type_index(typeid(T))]));
    }*/

    // SPAGHETTI_TEMPLATE
    template <>
    Pool<Transform>& GetPool() { return TransformPool; }
    template <>
    Pool<Camera>& GetPool() { return CameraPool; }
    template <>
    Pool<MeshView>& GetPool() { return MeshViewPool; }
    template <>
    Pool<Rigidbody>& GetPool() { return RigidbodyPool; }
    template <>
    Pool<Joint>& GetPool() { return JointPool; }
    template <>
    Pool<Input>& GetPool() { return InputPool; }
    template <>
    Pool<Logic>& GetPool() { return LogicPool; }
    template <>
    Pool<AudioSource>& GetPool() { return AudioSourcePool; }

    template <>
    Pool<Mesh>& GetPool() { return MeshPool; }


    void InitializePool( size_t poolsize )
    {
      SDL_LogVerbose( LOG_CATEGORY_MEMORY, "Initialize Memory Pools" );
      // SPAGHETTI_TEMPLATE
      TransformPool.Initialize( poolsize );
      typemap[type_index( typeid( Transform ) )] = &TransformPool;
      typeToName[type_index( typeid( Transform ) )] = "transform";
      CameraPool.Initialize( poolsize );
      typemap[type_index( typeid( Camera ) )] = &CameraPool;
      typeToName[type_index( typeid( Camera ) )] = "camera";
      MeshViewPool.Initialize( poolsize );
      typemap[type_index( typeid( MeshView ) )] = &MeshViewPool;
      typeToName[type_index( typeid( MeshView ) )] = "meshview";
      RigidbodyPool.Initialize( poolsize );
      typemap[type_index( typeid( Rigidbody ) )] = &RigidbodyPool;
      typeToName[type_index( typeid( Rigidbody ) )] = "rigidbody";
      JointPool.Initialize( poolsize );
      typemap[type_index( typeid( Joint ) )] = &JointPool;
      typeToName[type_index( typeid( Joint ) )] = "joint";
      InputPool.Initialize( poolsize );
      typemap[type_index( typeid( Input ) )] = &InputPool;
      typeToName[type_index( typeid( Input ) )] = "input";
      LogicPool.Initialize( poolsize );
      typemap[type_index( typeid( Logic ) )] = &LogicPool;
      typeToName[type_index( typeid( Logic ) )] = "logic";
      AudioSourcePool.Initialize( poolsize );
      typemap[type_index( typeid( AudioSource ) )] = &AudioSourcePool;
      typeToName[type_index( typeid( AudioSource ) )] = "audiosource";

      MeshPool.Initialize( poolsize );
      typemap[type_index( typeid( Mesh ) )] = &MeshPool;
      typeToName[type_index( typeid( Mesh ) )] = "";

      /*			// meshes
                              char** listBegin = PHYSFS_enumerateFiles( "mesh/" );
                              for( char** file = listBegin; *file != NULL; file++ )
                              {
                                      SDL_Log( " MESH [%s]", *file );
                                      json jmesh = Resource::ReadJSON(string( "mesh/" ) + ( *file ) );
                                      Pool<Mesh>& meshes = GetPool<Mesh>();
                                      Mesh& mesh = *meshes.New();
                                      mesh.Initialize( jmesh );
                              }
                              PHYSFS_freeList( listBegin );
      */

      SDL_Log( "Mem total: %zu MB", Spaghetti::total / ( 1024 * 1024 ) );
    }


    template <typename T>
    T* Create()
    {
      Pool<T>& pool = GetPool<T>();
      T* pt = pool.New();
      if( pt )
      {
        allPoolTypes[pt->id] = pt;
        return pt;
      }
      return nullptr;
    }

    template <typename T>
    void MarkForDestruction( T* cmp )
    {
      Pool<T>& pool = GetPool<T>();
      pool.MarkForDestruction( cmp );
    }

    template <typename T>
    void Destroy( T* cmp )
    {
      Pool<T>& pool = GetPool<T>();
      pool.Destroy( cmp );

      cmp->gameObject = nullptr;
    }

    void Dump()
    {
      // SPAGHETTI_TEMPLATE
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Transform> count:%zu freecount:%zu capacity:%zu",
                    TransformPool.pool.size(), TransformPool.freecount, TransformPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Camera> count:%zu freecount:%zu capacity:%zu",
                    CameraPool.pool.size(), CameraPool.freecount, CameraPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<MeshView> count:%zu freecount:%zu capacity:%zu",
                    MeshViewPool.pool.size(), MeshViewPool.freecount, MeshViewPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Rigidbody> count:%zu freecount:%zu capacity:%zu",
                    RigidbodyPool.pool.size(), RigidbodyPool.freecount, RigidbodyPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Joint> count:%zu freecount:%zu capacity:%zu",
                    JointPool.pool.size(), JointPool.freecount, JointPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Input> count:%zu freecount:%zu capacity:%zu",
                    InputPool.pool.size(), InputPool.freecount, InputPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<Logic> count:%zu freecount:%zu capacity:%zu",
                    LogicPool.pool.size(), LogicPool.freecount, LogicPool.pool.capacity() );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<AudioSource> count:%zu freecount:%zu capacity:%zu",
                    AudioSourcePool.pool.size(), AudioSourcePool.freecount, AudioSourcePool.pool.capacity() );
    }

    void CollectGarbage()
    {
      for( auto& go : garbage )
      {
        GameObject::DestroyImmediate( go );
      }
      garbage.clear();
    }
  }  // namespace Memory

  // SPAGHETTI_TEMPLATE
  template <>
  Transform* Component::GetComponentByID( ID_t id )
  {
    return (Transform*)allPoolTypes[id];
  }
  template <>
  Camera* Component::GetComponentByID( ID_t id )
  {
    return (Camera*)allPoolTypes[id];
  }
  template <>
  MeshView* Component::GetComponentByID( ID_t id )
  {
    return (MeshView*)allPoolTypes[id];
  }
  template <>
  Rigidbody* Component::GetComponentByID( ID_t id )
  {
    return (Rigidbody*)allPoolTypes[id];
  }
  template <>
  Joint* Component::GetComponentByID( ID_t id )
  {
    return (Joint*)allPoolTypes[id];
  }
  template <>
  Input* Component::GetComponentByID( ID_t id )
  {
    return (Input*)allPoolTypes[id];
  }
  template <>
  Logic* Component::GetComponentByID( ID_t id )
  {
    return (Logic*)allPoolTypes[id];
  }
  template <>
  AudioSource* Component::GetComponentByID( ID_t id )
  {
    return (AudioSource*)allPoolTypes[id];
  }

  void GameObject::DestroyImmediate( GameObject* go )
  {
    // SPAGHETTI_TEMPLATE
    std::vector<Transform*>& cmpsTransform = go->GetComponents<Transform>();
    for( auto& cmp : cmpsTransform )
      cmp->OnDestroy();
    std::vector<Camera*>& cmpsCamera = go->GetComponents<Camera>();
    for( auto& cmp : cmpsCamera )
      cmp->OnDestroy();
    std::vector<MeshView*>& cmpsMeshView = go->GetComponents<MeshView>();
    for( auto& cmp : cmpsMeshView )
      cmp->OnDestroy();
    std::vector<Rigidbody*>& cmpsRigidbody = go->GetComponents<Rigidbody>();
    for( auto& cmp : cmpsRigidbody )
      cmp->OnDestroy();
    std::vector<Joint*>& cmpsJoint = go->GetComponents<Joint>();
    for( auto& cmp : cmpsJoint )
      cmp->OnDestroy();
    std::vector<Input*>& cmpsInput = go->GetComponents<Input>();
    for( auto& cmp : cmpsInput )
      cmp->OnDestroy();
    std::vector<Logic*>& cmpsLogic = go->GetComponents<Logic>();
    for( auto& cmp : cmpsLogic )
      cmp->OnDestroy();
    std::vector<AudioSource*>& cmpsAudioSource = go->GetComponents<AudioSource>();
    for( auto& cmp : cmpsAudioSource )
      cmp->OnDestroy();
    for( auto& cmp : cmpsTransform )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsCamera )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsMeshView )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsRigidbody )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsJoint )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsInput )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsLogic )
      Memory::Destroy( cmp );
    for( auto& cmp : cmpsAudioSource )
      Memory::Destroy( cmp );
  }

  void GameObject::Destroy( GameObject* go )
  {
    if( go->OnDestroy )
    {
      go->OnDestroy();
    }
    garbage.push_back( go );

    // SPAGHETTI_TEMPLATE
    std::vector<Transform*>& cmpsTransform = go->GetComponents<Transform>();
    for( auto& cmp : cmpsTransform )
      Memory::MarkForDestruction( cmp );
    std::vector<Camera*>& cmpsCamera = go->GetComponents<Camera>();
    for( auto& cmp : cmpsCamera )
      Memory::MarkForDestruction( cmp );
    std::vector<MeshView*>& cmpsMeshView = go->GetComponents<MeshView>();
    for( auto& cmp : cmpsMeshView )
      Memory::MarkForDestruction( cmp );
    std::vector<Rigidbody*>& cmpsRigidbody = go->GetComponents<Rigidbody>();
    for( auto& cmp : cmpsRigidbody )
      Memory::MarkForDestruction( cmp );
    std::vector<Joint*>& cmpsJoint = go->GetComponents<Joint>();
    for( auto& cmp : cmpsJoint )
      Memory::MarkForDestruction( cmp );
    std::vector<Input*>& cmpsInput = go->GetComponents<Input>();
    for( auto& cmp : cmpsInput )
      Memory::MarkForDestruction( cmp );
    std::vector<Logic*>& cmpsLogic = go->GetComponents<Logic>();
    for( auto& cmp : cmpsLogic )
      Memory::MarkForDestruction( cmp );
    std::vector<AudioSource*>& cmpsAudioSource = go->GetComponents<AudioSource>();
    for( auto& cmp : cmpsAudioSource )
      Memory::MarkForDestruction( cmp );
  }

  typedef std::function<void( nlohmann::json& )> JsonFilterFunction;

  // run a filter function on every object
  void FilterJson( json& obj, JsonFilterFunction jff )
  {
    if( obj.is_array() )
    {
      for( auto it = obj.begin(); it != obj.end(); ++it )
      {
        FilterJson( *it, jff );
      }
    }
    else if( obj.is_object() )
    {
      jff( obj );
      for( auto it = obj.begin(); it != obj.end(); ++it )
      {
        // cout << it.key() << " : " << it.value() << "\n";
        if( ( *it ).is_object() || ( *it ).is_array() )
        {
          FilterJson( *it, jff );
        }
      }
    }
  }

  // SPAGHETTI_TEMPLATE
  std::vector<ComponentInitBinding> initsTransform;
  std::vector<ComponentInitBinding> initsCamera;
  std::vector<ComponentInitBinding> initsMeshView;
  std::vector<ComponentInitBinding> initsRigidbody;
  std::vector<ComponentInitBinding> initsJoint;
  std::vector<ComponentInitBinding> initsInput;
  std::vector<ComponentInitBinding> initsLogic;
  std::vector<ComponentInitBinding> initsAudioSource;
  // bool CreateComponent(Scene* scene, const json& jsonComponent, const std::string& lowercase, GameObject* go, unordered_map<ID_t,ID_t>& oldToNewID, std::vector<ComponentInitBinding>& inits )
  bool CreateComponent( Scene* scene, const json& jsonComponent, const std::string& lowercase, GameObject* go, unordered_map<ID_t, ID_t>& oldToNewID )
  {

    // SPAGHETTI_TEMPLATE
    if( lowercase == "transform" )
    {
      // Must create all components to get the real ID
      Transform* cmp = Memory::Create<Transform>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Transform>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsTransform.push_back( { std::bind( &Transform::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Transform::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "camera" )
    {
      // Must create all components to get the real ID
      Camera* cmp = Memory::Create<Camera>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Camera>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsCamera.push_back( { std::bind( &Camera::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Camera::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "meshview" )
    {
      // Must create all components to get the real ID
      MeshView* cmp = Memory::Create<MeshView>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<MeshView>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsMeshView.push_back( { std::bind( &MeshView::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &MeshView::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "rigidbody" )
    {
      // Must create all components to get the real ID
      Rigidbody* cmp = Memory::Create<Rigidbody>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Rigidbody>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsRigidbody.push_back( { std::bind( &Rigidbody::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Rigidbody::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "joint" )
    {
      // Must create all components to get the real ID
      Joint* cmp = Memory::Create<Joint>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Joint>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsJoint.push_back( { std::bind( &Joint::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Joint::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "input" )
    {
      // Must create all components to get the real ID
      Input* cmp = Memory::Create<Input>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Input>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsInput.push_back( { std::bind( &Input::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Input::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "logic" )
    {
      // Must create all components to get the real ID
      Logic* cmp = Memory::Create<Logic>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<Logic>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsLogic.push_back( { std::bind( &Logic::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &Logic::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    if( lowercase == "audiosource" )
    {
      // Must create all components to get the real ID
      AudioSource* cmp = Memory::Create<AudioSource>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<AudioSource>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[oldID] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      initsAudioSource.push_back( { std::bind( &AudioSource::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &AudioSource::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    return true;
  }


  GameObject* Scene::CreateGameObject( const nlohmann::json& obj )
  {
    bool valid = true;
    GameObject* go = new GameObject();
    unordered_map<ID_t, ID_t> oldToNewID;
    // std::vector<ComponentInitBinding> inits;

    // SPAGHETTI_TEMPLATE
    initsTransform.clear();
    initsCamera.clear();
    initsMeshView.clear();
    initsRigidbody.clear();
    initsJoint.clear();
    initsInput.clear();
    initsLogic.clear();
    initsAudioSource.clear();
    string type;
    string lowercase;
    for( auto it = obj.begin(); valid && it != obj.end(); ++it )
    {
      type = it.key();
      lowercase = type;
      std::transform( lowercase.begin(), lowercase.end(), lowercase.begin(), ::tolower );
      if( ( *it ).is_object() )
      {
        // if( !CreateComponent( this, *it, lowercase, go, oldToNewID, inits ) )
        if( !CreateComponent( this, *it, lowercase, go, oldToNewID ) )
        {
          valid = false;
          break;
        }
      }
      else if( ( *it ).is_array() )
      {
        for( auto element = ( *it ).begin(); valid && element != ( *it ).end(); ++element )
        {
          // if( !CreateComponent( this, *element, lowercase, go, oldToNewID, inits ) )
          if( !CreateComponent( this, *element, lowercase, go, oldToNewID ) )  //, inits ) )
          {
            valid = false;
            break;
          }
        }
      }
    }
    if( valid )
    {
      // SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID (old to new):");
      /*for(auto o2n : oldToNewID )
      {
        SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "%llu:%llu", o2n.first, o2n.second );
      }*/
      // Initialize components in order defined in build config.
      // Components can rely on this order when initializing.
      // Correct the IDs for each component before initializing,
      // so that components can call Component::GetComponentByID()

      // SPAGHETTI_TEMPLATE

      for( auto cib : initsTransform )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Transform)" );
          break;
        }
      }
      for( auto cib : initsCamera )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Camera)" );
          break;
        }
      }
      for( auto cib : initsMeshView )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (MeshView)" );
          break;
        }
      }
      for( auto cib : initsRigidbody )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Rigidbody)" );
          break;
        }
      }
      for( auto cib : initsJoint )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Joint)" );
          break;
        }
      }
      for( auto cib : initsInput )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Input)" );
          break;
        }
      }
      for( auto cib : initsLogic )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (Logic)" );
          break;
        }
      }
      for( auto cib : initsAudioSource )
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&]( json& j )
                    {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          } } );

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed (AudioSource)" );
          break;
        }
      }
    }
    if( valid )
    {
      objects.push_back( go );
      return go;
    }
    GameObject::DestroyImmediate( go );
    delete go;
    SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Game Object did not initialize!" );
    return nullptr;
  }


}  // namespace Spaghetti
