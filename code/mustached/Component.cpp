#include "Component.h"

#include <vector>
#include <typeinfo>
#include <typeindex>
#include <cstddef> //for offsetof

#include "json.hpp"
#include "MemoryPool.h"
#include "ComponentHeaders.h"

namespace Spaghetti
{
  using namespace std;
  using namespace glm;
  using json = nlohmann::json;

  // template<typename T>
  // void Component::Destroy( T* cmp )
  // {
  //   cmp->gameObject->RemoveComponent( cmp );
  //   cmp->OnDestroy();
  //   Memory::Destroy( cmp );
  // }

  // template<typename T>
  // void GameObject::RemoveComponent( T* cmp )
  // {
  //   std::vector<T*>& cmps = GetComponents<T>();
  //   for (auto it = cmps.begin(); it != cmps.end(); )
  //   {
  //     if( *it == cmp )
  //     {
  //       cmps.erase( it );
  //       break;
  //     }
  //   }
  // }

  // SPAGHETTI_TEMPLATE

  template<>
  void Component::Destroy( Transform* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( Camera* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( MeshView* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( Rigidbody* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( Joint* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( Input* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( Logic* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  template<>
  void Component::Destroy( AudioSource* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }

  // SPAGHETTI_TEMPLATE

  template<>
  std::vector<Transform*>& GameObject::GetComponents()
  {
    return TransformComponents;
  }
  template<>
  std::vector<Camera*>& GameObject::GetComponents()
  {
    return CameraComponents;
  }
  template<>
  std::vector<MeshView*>& GameObject::GetComponents()
  {
    return MeshViewComponents;
  }
  template<>
  std::vector<Rigidbody*>& GameObject::GetComponents()
  {
    return RigidbodyComponents;
  }
  template<>
  std::vector<Joint*>& GameObject::GetComponents()
  {
    return JointComponents;
  }
  template<>
  std::vector<Input*>& GameObject::GetComponents()
  {
    return InputComponents;
  }
  template<>
  std::vector<Logic*>& GameObject::GetComponents()
  {
    return LogicComponents;
  }
  template<>
  std::vector<AudioSource*>& GameObject::GetComponents()
  {
    return AudioSourceComponents;
  }

  // SPAGHETTI_TEMPLATE

  /*template<>
  Transform* GameObject::GetComponent()
  {
    Transform* out = nullptr;
    std::vector<Transform*>& cmps = GetComponents<Transform>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }*/

  template<>
  Transform* GameObject::GetComponent()
  {
    Transform* out = nullptr;
    std::vector<Transform*>& cmps = GetComponents<Transform>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Camera* GameObject::GetComponent()
  {
    Camera* out = nullptr;
    std::vector<Camera*>& cmps = GetComponents<Camera>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  MeshView* GameObject::GetComponent()
  {
    MeshView* out = nullptr;
    std::vector<MeshView*>& cmps = GetComponents<MeshView>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Rigidbody* GameObject::GetComponent()
  {
    Rigidbody* out = nullptr;
    std::vector<Rigidbody*>& cmps = GetComponents<Rigidbody>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Joint* GameObject::GetComponent()
  {
    Joint* out = nullptr;
    std::vector<Joint*>& cmps = GetComponents<Joint>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Input* GameObject::GetComponent()
  {
    Input* out = nullptr;
    std::vector<Input*>& cmps = GetComponents<Input>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Logic* GameObject::GetComponent()
  {
    Logic* out = nullptr;
    std::vector<Logic*>& cmps = GetComponents<Logic>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  AudioSource* GameObject::GetComponent()
  {
    AudioSource* out = nullptr;
    std::vector<AudioSource*>& cmps = GetComponents<AudioSource>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }

  // template<>
  // * GameObject::AddComponent()
  // {
  //   * cmp = Memory::Create<>();
  //   if( cmp == nullptr )
  //   {
  //     SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to add new component of type " );
  //     return nullptr;
  //   }
  //   std::vector<*>& cmps = GetComponents<>();
  //   cmps.push_back( cmp );
  //   cmp->gameObject = this;
  //   cmp->scene = scene;
  //   // cmp->Initialize();
  //   return cmp;
  // }

  // SPAGHETTI_TEMPLATE

  void GameObject::RemoveComponent( Transform* cmp )
  {
    std::vector<Transform*>& cmps = GetComponents<Transform>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( Camera* cmp )
  {
    std::vector<Camera*>& cmps = GetComponents<Camera>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( MeshView* cmp )
  {
    std::vector<MeshView*>& cmps = GetComponents<MeshView>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( Rigidbody* cmp )
  {
    std::vector<Rigidbody*>& cmps = GetComponents<Rigidbody>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( Joint* cmp )
  {
    std::vector<Joint*>& cmps = GetComponents<Joint>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( Input* cmp )
  {
    std::vector<Input*>& cmps = GetComponents<Input>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( Logic* cmp )
  {
    std::vector<Logic*>& cmps = GetComponents<Logic>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  void GameObject::RemoveComponent( AudioSource* cmp )
  {
    std::vector<AudioSource*>& cmps = GetComponents<AudioSource>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }

  // SPAGHETTI_TEMPLATE

  template<>
  const std::vector<Transform*>& GameObject::GetComponentsConst() const
  {
    return TransformComponents;
  }
  template<>
  const std::vector<Camera*>& GameObject::GetComponentsConst() const
  {
    return CameraComponents;
  }
  template<>
  const std::vector<MeshView*>& GameObject::GetComponentsConst() const
  {
    return MeshViewComponents;
  }
  template<>
  const std::vector<Rigidbody*>& GameObject::GetComponentsConst() const
  {
    return RigidbodyComponents;
  }
  template<>
  const std::vector<Joint*>& GameObject::GetComponentsConst() const
  {
    return JointComponents;
  }
  template<>
  const std::vector<Input*>& GameObject::GetComponentsConst() const
  {
    return InputComponents;
  }
  template<>
  const std::vector<Logic*>& GameObject::GetComponentsConst() const
  {
    return LogicComponents;
  }
  template<>
  const std::vector<AudioSource*>& GameObject::GetComponentsConst() const
  {
    return AudioSourceComponents;
  }

  // SPAGHETTI_TEMPLATE

  template<>
  Transform* GameObject::GetComponentConst() const
  {
    Transform* out = nullptr;
    const std::vector<Transform*>& cmps = GetComponentsConst<Transform>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Camera* GameObject::GetComponentConst() const
  {
    Camera* out = nullptr;
    const std::vector<Camera*>& cmps = GetComponentsConst<Camera>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  MeshView* GameObject::GetComponentConst() const
  {
    MeshView* out = nullptr;
    const std::vector<MeshView*>& cmps = GetComponentsConst<MeshView>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Rigidbody* GameObject::GetComponentConst() const
  {
    Rigidbody* out = nullptr;
    const std::vector<Rigidbody*>& cmps = GetComponentsConst<Rigidbody>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Joint* GameObject::GetComponentConst() const
  {
    Joint* out = nullptr;
    const std::vector<Joint*>& cmps = GetComponentsConst<Joint>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Input* GameObject::GetComponentConst() const
  {
    Input* out = nullptr;
    const std::vector<Input*>& cmps = GetComponentsConst<Input>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  Logic* GameObject::GetComponentConst() const
  {
    Logic* out = nullptr;
    const std::vector<Logic*>& cmps = GetComponentsConst<Logic>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  template<>
  AudioSource* GameObject::GetComponentConst() const
  {
    AudioSource* out = nullptr;
    const std::vector<AudioSource*>& cmps = GetComponentsConst<AudioSource>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }

  void GameObject::SetPosition( glm::vec3 pos )
  {
    const std::vector<Transform*>& tfs = GetComponents<Transform>();
    vec3 origin;
    if( tfs.size()>0 )
      origin = tfs.front()->GetPosition();
    for(auto& tf : tfs )
    {
      vec3 offset = tf->GetPosition() - origin;
      tf->SetPosition( pos + offset );
    }

    const std::vector<Rigidbody*>& rbds = GetComponents<Rigidbody>();
    vec3 rbOrigin;
    if( rbds.size()>0 )
      rbOrigin = rbds.front()->boundTransform->GetPosition();
    for(auto& rb : rbds )
    {
      vec3 offset = rb->boundTransform->GetPosition() - rbOrigin;
      // SDL_Log("offset: %f %f %f", offset.x, offset.y, offset.z );
      rb->SetPosition( pos + offset );
    }
  }

  void GameObject::SetState( const json& state )
  {
    for( json::const_iterator it = state.begin(); it != state.end(); ++it )
    {
      if( it.key() == "position")
      {
        std::vector<float> fv = it.value().get<std::vector<float>>();
        SetPosition( glm::vec3(fv[0],fv[1],fv[2]) );
      }
    }
  }

  void to_json( json& j, const GameObject& obj)
  {
    const Transform* t = obj.GetComponentConst<Transform>();
    glm::vec3 position = t->GetPosition();
    j = {
      {"filename", obj.filename},
      {"state",{"position", std::array<float,3>({position.x,position.y,position.z})}}
    };
  }

  void from_json(const json& j, GameObject& obj)
  {
    // obj.id.value = j.at("id").get<ID_t>();
    //obj.name = j.at("name").get<std::string>();
    // 
    // Memory::Create( obj.TransformComponent );
    // (*obj.TransformComponent) = j.at("Transform").get<Transform>();
    // 
    // Memory::Create( obj.CameraComponent );
    // (*obj.CameraComponent) = j.at("Camera").get<Camera>();
    // 
    // Memory::Create( obj.MeshViewComponent );
    // (*obj.MeshViewComponent) = j.at("MeshView").get<MeshView>();
    // 
    // Memory::Create( obj.RigidbodyComponent );
    // (*obj.RigidbodyComponent) = j.at("Rigidbody").get<Rigidbody>();
    // 
    // Memory::Create( obj.JointComponent );
    // (*obj.JointComponent) = j.at("Joint").get<Joint>();
    // 
    // Memory::Create( obj.InputComponent );
    // (*obj.InputComponent) = j.at("Input").get<Input>();
    // 
    // Memory::Create( obj.LogicComponent );
    // (*obj.LogicComponent) = j.at("Logic").get<Logic>();
    // 
    // Memory::Create( obj.AudioSourceComponent );
    // (*obj.AudioSourceComponent) = j.at("AudioSource").get<AudioSource>();
    // 
  }


}
