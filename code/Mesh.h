#pragma once

#include <string>
#include <unordered_map>

#include "GL.h"
#include "json.hpp"
#include "Pool.h"

namespace Spaghetti
{
  struct Mesh : public PoolType
  {
    bool valid = false;
    std::string name;
    uint32_t indexCount;
    uint32_t vertexCount;
    uint32_t vertexFloatCount;  // determined by attributes
    uint32_t vertsPerFace;
    GLuint vbo;
    GLuint vboIndices;
    GLuint vertexArrayID;
    std::unordered_map<std::string, char*> attributeOffset;
    GLuint usage = GL_DYNAMIC_DRAW; //GL_STATIC_DRAW;
    typedef uint16_t IndexType;
    // TODO create DynamicMesh pool type and move the vertex arrays there
    std::vector<IndexType> indexArray;
    std::vector<float> vertexArray;

    bool Initialize( const nlohmann::json& params );
    void OnDestroy();

    // dynamic mesh
    float* GetVertexArrayAttribute(std::string attribute);
    void UpdateMesh();
    // untested because glBufferSubData() seemed fast enough
    // float* MapBuffer();
    // void UnmapBuffer();
  };

  namespace MeshGenerator
  {
    void Init();
    bool CanGenerateMesh( const std::string& type );
    bool Generate( const std::string& meshType, nlohmann::json& out, const nlohmann::json& params );
  }
}
