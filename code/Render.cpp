#include "Camera.h"

#include <sstream>
#include <string>

#include "SDL2/SDL_image.h"

#include "Core.h"
#include "Material.h"
#include "Resource.h"
#include "Timer.h"

namespace Spaghetti
{
  using namespace std;

  uint32_t Core::InitializeVideo()
  {
    // SDL_Init( SDL_INIT_VIDEO );

    // SDL_GL_SetAttribute( SDL_GL_RED_SIZE, 8 );
    // SDL_GL_SetAttribute( SDL_GL_GREEN_SIZE, 8 );
    // SDL_GL_SetAttribute( SDL_GL_BLUE_SIZE, 8 );
    // SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 8 );
    // SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
    // SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
    // SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES );

    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 3 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE );

    // SDL_Log( "Creating window with settings: %s", settings.dump( 4 ).c_str() );
    window = SDL_CreateWindow( "meatball", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, settings["window"]["width"], settings["window"]["height"], SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE );
    if( !window )
    {
      SDL_LogCritical( SDL_LOG_CATEGORY_ERROR, "Window creation failed." );
      return 1;
    }

    context = SDL_GL_CreateContext( window );
    SDL_GL_MakeCurrent( window, context );
    Debug::CheckGLErrors( __FILE__, __LINE__ );

    SDL_Log( "GL_VERSION %s", (char*)glGetString( GL_VERSION ) );
    SDL_Log( "GL_VENDOR %s", (char*)glGetString( GL_VENDOR ) );
    SDL_Log( "GL_RENDERER %s", (char*)glGetString( GL_RENDERER ) );
    SDL_Log( "GL_SHADING_LANGUAGE_VERSION %s", (char*)glGetString( GL_SHADING_LANGUAGE_VERSION ) );

    int glA, glB, glC, glD, glE, glF;
    // glGetIntegerv( GL_MAX_VERTEX_UNIFORM_COMPONENTS, &glA );
    // SDL_Log( "GL_MAX_VERTEX_UNIFORM_COMPONENTS: %d", glA );
    // glGetIntegerv( GL_MAX_VARYING_FLOATS, &glB );
    // SDL_Log( "GL_MAX_VARYING_FLOATS: %d", glB );
    // glGetIntegerv( GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &glC );
    // SDL_Log( "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS: %d", glC );
    // glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, &glD );
    // SDL_Log( "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS: %d", glD );
    // glGetIntegerv( GL_MAX_DRAW_BUFFERS, &glE );
    // SDL_Log( "GL_MAX_DRAW_BUFFERS: %d", glE );
    //
    SDL_GL_GetAttribute( SDL_GL_RED_SIZE, &glA );
    SDL_Log( "SDL_GL_RED_SIZE: %d", glA );
    SDL_GL_GetAttribute( SDL_GL_GREEN_SIZE, &glB );
    SDL_Log( "SDL_GL_GREEN_SIZE: %d", glB );
    SDL_GL_GetAttribute( SDL_GL_BLUE_SIZE, &glC );
    SDL_Log( "SDL_GL_BLUE_SIZE: %d", glC );
    SDL_GL_GetAttribute( SDL_GL_ALPHA_SIZE, &glD );
    SDL_Log( "SDL_GL_ALPHA_SIZE: %d", glD );
    SDL_GL_GetAttribute( SDL_GL_DEPTH_SIZE, &glE );
    SDL_Log( "SDL_GL_DEPTH_SIZE: %d", glE );
    // SDL_GL_GetAttribute( SDL_GL_DOUBLEBUFFER, &glF );
    // SDL_Log( "SDL_GL_DOUBLEBUFFER: %d", glF );
    // CheckGLErrors( __FILE__, __LINE__ );

    SDL_GL_SetSwapInterval( 0 );
    // SDL_SetWindowFullscreen( window, 1 );
    // glViewport( 0, 0, settings["window"]["width"], settings["window"]["height"] );
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );


    // glEnable( GL_CULL_FACE );
    glCullFace( GL_BACK );
    glFrontFace( GL_CCW );
    glEnable( GL_DEPTH_TEST );
    glDepthFunc( GL_LESS );
    // glDisable( GL_LINE_SMOOTH );
    // glLineWidth( 1.0f );
    // glPointSize( 4.0f );
    // glEnable( GL_PROGRAM_POINT_SIZE );

    IMG_Init( IMG_INIT_PNG );
    Debug::CheckGLErrors( __FILE__, __LINE__ );
    return 0;
  }

  void FlipVertical( uint8_t* img, int w, int h, int bytesPerPixel )
  {
    uint8_t* temp = new uint8_t[w * h * bytesPerPixel];
    memset( temp, 0, w * h * bytesPerPixel );
    for( int r = 0; r < h; r++ )  // go row-by-row, copy in inverse order
      memcpy( temp + ( r * w * bytesPerPixel ), img + ( ( h - r - 1 ) * w * bytesPerPixel ), ( w * bytesPerPixel ) );
    memcpy( img, temp, w * h * bytesPerPixel );
    delete[] temp;
  }

  void Core::Screenshot()
  {
    int width = settings["window"]["width"];
    int height = settings["window"]["height"];
    uint8_t* buffer = new uint8_t[width * height * 3];
    glPixelStorei( GL_PACK_ALIGNMENT, 1 );
    glReadPixels( 0, 0, width, height, GL_RGB, GL_UNSIGNED_BYTE, buffer );
    SDL_Surface* screenshot = SDL_CreateRGBSurface( SDL_SWSURFACE, width, height, 24,
#if SDL_BYTEORDER == SDL_LIL_ENDIAN
                                                    0x000000FF, 0x0000FF00, 0x00FF0000, 0
#else
                                                    0x00FF0000, 0x0000FF00, 0x000000FF, 0
#endif
    );
    FlipVertical( buffer, width, height, 3 );
    memcpy( screenshot->pixels, buffer, ( width * height * 3 ) );
    Resource::WritePNG( screenshot, Time::Now() + ".png" );
    SDL_FreeSurface( screenshot );
    delete[] buffer;
  }


}  // namespace Spaghetti
