#pragma once
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "Box2D/Box2D.h"
#include "json.hpp"

#include "Common.h"

namespace Spaghetti
{
  using json = nlohmann::json;

  struct Transform;
  struct GameObject;
  struct Material;
  /*class DebugDraw;*/

  typedef std::function<bool( const json& )> ComponentInitializeFunction;
  struct ComponentInitBinding
  {
    ComponentInitializeFunction cif;
    json params;
  };
/*
  struct ContactListener : public b2ContactListener
  {
    void BeginContact(b2Contact* contact) override;
  };

  struct DestructionListener : public b2DestructionListener
  {
  	/// Called when any joint is about to be destroyed due
  	/// to the destruction of one of its attached bodies.
  	void SayGoodbye(b2Joint* joint);

  	/// Called when any fixture is about to be destroyed due
  	/// to the destruction of its parent body.
  	void SayGoodbye(b2Fixture* fixture);
  };*/

  struct Scene
  {
    static Scene* Current();

    GameObject* GenerateCreature(const json& param);

    void Initialize( const json& obj );
    void UpdateScene();
    void RenderScene( float dT );
    void DebugDumpLayers();
    GameObject* CreateGameObject( const nlohmann::json& obj );
    void AddTransform( int depth, ID_t id, Transform* tf );
    void RemoveTransform( Transform* tf );
    void UpdateTransforms();

    void Save();
    void Load( const std::string& name );

    // depth, ID
    std::map<int, std::unordered_map<ID_t, Transform*>> transformLayer;

    // rigid body simulation
    b2World* world;
    b2ContactListener* contactListener;
    b2DestructionListener* destructionListener;
    bool bRenderDebug=false;

    std::vector<GameObject*> objects;
    // std::unordered_map<std::unique_ptr<GameObject>> objects;
    Material* defaultMaterial = nullptr;

    GameObject* CreateGameObjectSingle( const json& obj, std::unordered_map<ID_t, ID_t>& oldToNewID );

  };
}
