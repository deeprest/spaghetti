#include "Input.h"

#include <unordered_map>

#include "SDL2/SDL_events.h"
#include "SDL2/SDL_mouse.h"
#include "Timer.h"
// #include "MemoryPool.h"
#include "Scene.h"
#include "Resource.h"
#include "json.hpp"

#include "Camera.h"
#include "Transform.h"

namespace Spaghetti
{
  uint32_t Input::KeyHeldEvent;
  // std::shared_ptr<Input> Input::activeInputComponent;
  Input* Input::activeInputComponent = nullptr;

  // std::unordered_map<int,bool> keyDown;
  // std::unordered_map<int,bool> keyPressed;
  // std::vector<int> newlyPressed;
  // std::unordered_map<string,float> axis;

  bool Input::KeyHeld( int keycode )
  {
    if( activeInputComponent != nullptr )
    {
      return activeInputComponent->keyDown[keycode];
    }
    return false;
  }

  bool Input::KeyPressed( int keycode )
  {
    if( activeInputComponent != nullptr )
    {
      return activeInputComponent->keyPressed[keycode];
    }
    return false;
  }

  float Input::GetAxis( const std::string& axisName )
  {
    if( activeInputComponent != nullptr )
    {
      return activeInputComponent->axis[axisName];
    }
    return 0;
  }

  void Input::Activate()
  {
    activeInputComponent = this; //std::make_shared<Input>(*this);
  }

  bool Input::Initialize( const nlohmann::json& params )
  {
    Activate();
    // CameraMouseCoef = 0.1f;

    // TODO: read inputs bindings from params
    axis["mouse.x"] = 0;
    axis["mouse.y"] = 0;

    return true;
  }

  void Input::PreFrame()
  {
    for(auto& id:newlyPressed)
    {
      keyPressed[id] = false;
    }
    newlyPressed.clear();

    for(auto& ax : axis)
    {
      ax.second = 0.0f; //Time::deltaTime * 1.0f;
    }
  }

  void Input::HandleEvent( SDL_Event event )
  {
    Camera* camera = Camera::Current();

    switch( event.type )
    {
      case SDL_KEYDOWN:
        keyDown[event.key.keysym.sym] = true;

        if( !event.key.repeat )
        {
          switch( event.key.keysym.scancode )
          {
            default:
              break;
          }
          SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "KEYDOWN %d %d %d", event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
        }
        break;

      case SDL_KEYUP:
        keyDown[event.key.keysym.sym] = false;
        keyPressed[event.key.keysym.sym] = true;
        newlyPressed.push_back( event.key.keysym.sym );

        switch( event.key.keysym.scancode )
        {
          case SDL_SCANCODE_Q:
            if( camera != nullptr )
              camera->DebugPrint();
            break;

          case SDL_SCANCODE_V:
            if( camera != nullptr )
              camera->ZoomTo( camera->zoomMax, camera->zoomRate );
            break;
          case SDL_SCANCODE_X:
            if( camera != nullptr )
              camera->ZoomTo( camera->zoomMin, camera->zoomRate );
            break;

          default:
            break;
        }
        SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "KEYUP %d %d %d", event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
        break;

      case SDL_MOUSEMOTION:
        {
          // Y is negated so when the mouse moves up, it produces a positive value
          float relx = static_cast<float>( event.motion.xrel );
          float rely = static_cast<float>( -event.motion.yrel );
          axis["mouse.x"] += relx;
          axis["mouse.y"] += rely;
        }
        break;

      default:
        if( event.type == KeyHeldEvent )
        {
          SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "HELD %d %d %d", event.key.keysym.scancode, event.key.timestamp, event.key.repeat );

          if( camera != nullptr )
          {
            // const float tempMoveSpeed = 1.0f;

            switch( event.key.keysym.scancode )
            {
              // case SDL_SCANCODE_E:
              //   camera->AddLocalAcceleration( tempMoveSpeed, 0, 0 );
              //   break;
              // case SDL_SCANCODE_D:
              //   camera->AddLocalAcceleration( -tempMoveSpeed, 0, 0 );
              //   break;
              // case SDL_SCANCODE_F:
              //   camera->AddLocalAcceleration( 0, 0, tempMoveSpeed );
              //   break;
              // case SDL_SCANCODE_S:
              //   camera->AddLocalAcceleration( 0, 0, -tempMoveSpeed );
              //   break;
              default:
                // SDL_Log("KEYDOWN %d %d %d", event.key.keysym.scancode,
                // event.key.timestamp, event.key.repeat );
                break;
            }
          }
        }
        else
        {
          SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "%d %d %d %d", event.type, event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
        }
        break;
    }

    // SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "%d %d %d %d", event.type, event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
  }

}
