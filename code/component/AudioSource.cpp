#include "AudioSource.h"
#include "Mathematics.h"
#include "Resource.h"
#include "SDL2/SDL_events.h"
#include "SDL2/SDL_timer.h"

namespace Spaghetti
{

  bool AudioSource::Initialize( const nlohmann::json& params )
  {
    if( params.empty() )
    {
      channel = 1;
      // sample = Mix_LoadWAV( "sounds/test.wav" );
      std::vector<uint8_t> buffer;
      Resource::ReadBuffer( "sounds/test.wav", buffer );
      sample = Mix_LoadWAV_RW( SDL_RWFromMem( buffer.data(), buffer.size() ), 1 );
    }
    else
    {
      if( params.count( "resource" ) > 0 )
      {
        std::vector<uint8_t> buffer;
        Resource::ReadBuffer( params["resource"].get<std::string>().c_str(), buffer );
        sample = Mix_LoadWAV_RW( SDL_RWFromMem( buffer.data(), buffer.size() ), 1 );
      }
      if( params.count( "channel" ) > 0 )
        channel = params["channel"];
    }

    if( !sample )
    {
      SDL_Log( "Mix_LoadWAV: %s\n", Mix_GetError() );
      return false;
    }

    return true;
  }

  void AudioSource::OnDestroy()
  {
    Mix_FreeChunk( sample );
  }

  unsigned int Done( unsigned int interval, void* param )
  {
    AudioSource* source = (AudioSource*)param;
    source->IsPlaying = false;
    return 0;
  }


  void AudioSource::Play( float volume, bool force )
  {
    if( Mix_Playing( channel ) /*IsPlaying*/ && !force )
      return;


    Mix_VolumeChunk( sample, Clamp<float>( volume, 0, 1 ) * MIX_MAX_VOLUME );

    int chan = Mix_PlayChannel( channel, sample, 0 );
    if( chan == -1 )
    {
      SDL_Log( "Mix_PlayChannel: %s\n", Mix_GetError() );
      // may be critical error, or maybe just no channels were free.
      // you could allocated another channel in that case...
    }
    else
    {
      IsPlaying = true;
      //SDL_TimerID my_timer_id = SDL_AddTimer( 100, Done , this );
    }
  }
}
