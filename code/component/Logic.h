#pragma once

#include <functional>
#include "json.hpp"
#include "Component.h"

namespace Spaghetti
{

  struct Logic : public Component
  {
    std::function<void(Logic&)> logicFunction;

    float elapsedTime = 0;
    // json creatureParams;
    float CameraMouseCoef = 0.1f;

    bool Initialize( const nlohmann::json& params );
    void Update( float DeltaTime );

    void CameraLogic();
    GameObject* lookTarget = nullptr;
    float forwardSpeed = 1000;
    float turnSpeed = 400;
    float turnBrake = 0;
    float distanceThreshold = 0.5;

    void CreatureLogic();
    float pushAmount = 40;
    float torque = 3;
    float flipTime = 0;
    float flipInterval = 0.2;
    float flipMotorSpeed = 1;

  };

}
