#pragma once

#include <string>
#include "glm/glm.hpp"
#include "json.hpp"
#include "Component.h"

namespace Spaghetti
{
  using string = std::string;
  using json = nlohmann::json;

  struct Camera : public Component
  {
    static Camera* Current();
    static glm::mat4 View;

    bool Initialize( const json& params );
    void Activate();
    void PreRender();
    void Update( float DeltaTime );

    void ZoomTo( float target, float rate );
    void AddLocalAcceleration( float right, float up, float forward );
    void RotatePYR( float pitch, float yaw, float roll );
    void DebugPrint();

    // static vec3 GetWorldPoint( const vec3& screenCoord );
    // static vec3 GetScreenPoint( const vec3& worldCoord );
    // vec3 GetDimensions(){ return vec3( zoom*xy_ratio, zoom, 0.f ); }
    // void SetAspectRatio( float ar ){ xy_ratio = ar; }
    // move/look
    //		GameObject* GetTarget();
    //		void MoveTo(const vec3& here);
    //		void SeekTarget(GameObject* t);
    //		void LockTarget(GameObject* t);
    //		void Unlock();


    float RotationSpeed = 10.0f;
    // serialized
    float frustumNear;
    float frustumFar;
    float zoomMin;
    float zoomMax;
    float zoomRate;
    float zoom;
    float zoomTarget;
  };
}
