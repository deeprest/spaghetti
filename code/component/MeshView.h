#pragma once

#include <vector>
#include "GL.h"
#include "Mesh.h"
#include "Material.h"
#include "Component.h"

namespace Spaghetti
{
  struct MeshView : public Component
  {
    private:
      bool active = true;

    public:
      Transform* renderTransform = nullptr;
      Mesh* renderMesh = nullptr;
      Material* material = nullptr;

      GLuint RenderMode = GL_TRIANGLES;
      bool DepthOff = false;

      bool Initialize( const nlohmann::json& obj );
      void OnDestroy();

      bool IsVisible() const;
      void Activate();
      void Deactivate();

      void UpdateView( GLuint OverrideRenderMode = 0 ) const;
  };

}
