#include "Camera.h"

#include "GL.h"
#include "SDL2/SDL_log.h"
// #include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"  // translate, rotate, scale, perspective
// #include "glm/gtc/type_ptr.hpp"          // value_ptr
#include "glm/gtx/euler_angles.hpp"
// #include "glm/gtx/orthonormalize.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtx/string_cast.hpp"
#include "json.hpp"

#include "Component.h"
#include "Core.h"
#include "Debug.h"
#include "Material.h"
#include "Mathematics.h"
#include "Serialize.h"
#include "Timer.h"
#include "Transform.h"


namespace Spaghetti
{
  using namespace std;
  using namespace glm;
  using json = nlohmann::json;

  static Camera* mCurrent;
  static constexpr float KeepCurrentRate = -1.0f;

  // constants
  const float maxDamp = 10.0f;
  const float maxDeltaTime = 0.333f;
  const float damp = 5.0f;  // Exponentiation base for velocity damping. A good range for this is [1, TargetFramerate]
  const float dampBase = pow( 1.f - damp / maxDamp, maxDamp );
  const float RotationSpeed = 1.0f;

  // state
  vec3 PYR;
  vec3 LocalAcceleration;
  vec3 Velocity;
  bool isActive = false;
  bool isZooming = false;

  // shader
  mat4 Projection;
  mat4 Camera::View;
  GLuint LocationView;
  GLuint LocationProjection;

  // transient
  vec3 PYRAccum;  // input rot accum


  Camera* Camera::Current() { return mCurrent; }

  bool Camera::Initialize( const json& obj )
  {
    /*AlignViewToPositiveXAxis = rotate( mat4( 1.0f ), TAU / 4.0f, vec3( 0.0f, 1.0f, 0.0f ) );*/

    if( obj.count( "frustum" ) )
    {
      frustumNear = Read<float>( obj["frustum"], "near", 0.1f );
      frustumFar = Read<float>( obj["frustum"], "far", 100.0f );
    }
    if( obj.count( "zoom" ) )
    {
      zoom = Read<float>( obj["zoom"], "value", 1.0f );
      zoomMin = Read<float>( obj["zoom"], "min", 0.01f );
      zoomMax = Read<float>( obj["zoom"], "max", 3.0f );
      zoomRate = Read<float>( obj["zoom"], "rate", 1.0f );
    }
    zoomTarget = zoom;
    return true;
  }

  void Camera::Activate()
  {
    isActive = true;
    mCurrent = this;
    float xyRatio = (float)Core::settings["window"]["width"] / (float)Core::settings["window"]["height"];
    Projection = perspectiveRH( zoom, xyRatio, frustumNear, frustumFar );
  }


  void Camera::Update( float DeltaTime )
  {
    Transform* transform = gameObject->GetComponent<Transform>();

    if( isZooming )
    {
      isZooming = MoveTowards( zoom, zoomTarget, zoomRate * DeltaTime );
    }

    PYR += PYRAccum * DeltaTime;  // * RotationSpeed;
    PYRAccum = vec3();
    PYR.y = NormalizeAngle( PYR.y );
    PYR.x = Clamp( PYR.x, 0.f, TAU / 2.0f );

    mat4 RotationDelta;
    RotationDelta = mat4( 1.0f );
    // GL is -Z forward
    RotationDelta = rotate( RotationDelta, PYR.z, vec3( 0, 1, 0 ) );
    RotationDelta = rotate( RotationDelta, PYR.x, vec3( -1, 0, 0 ) );
    RotationDelta = rotate( RotationDelta, PYR.y, vec3( 0, 0, 1 ) );
    transform->SetRotation( RotationDelta );

    vec3 WorldAcc = LocalAcceleration * mat3( RotationDelta );
    LocalAcceleration = vec3();
    Velocity += WorldAcc * DeltaTime;
    // framerate independent velocity damping:
    Velocity *= pow( dampBase, DeltaTime );
    transform->SetPosition( transform->GetPosition() + Velocity );
  }

  // mat4 Camera::GetView()
  // {
  //   Transform* transform = gameObject->GetComponent<Transform>();
  //   return translate( transform->GetRotationMatrix(), -transform->GetLocalPosition() );
  // }

  void Camera::PreRender()
  {
    if( isActive )
    {
      if( isZooming )
      {
        float xyRatio = (float)Core::settings["window"]["width"] / (float)Core::settings["window"]["height"];
        Projection = perspectiveRH( zoom, xyRatio, frustumNear, frustumFar );
      }

      GLuint programID = Material::GetCurrent()->GetProgramID();
      LocationProjection = glGetUniformLocation( programID, "Projection" );
      glUniformMatrix4fv( LocationProjection, 1, GL_FALSE, value_ptr( Projection ) );

      Transform* transform = gameObject->GetComponent<Transform>();
      View = translate( transform->GetRotation(), -transform->GetLocalPosition() );
      LocationView = glGetUniformLocation( programID, "View" );
      glUniformMatrix4fv( LocationView, 1, GL_FALSE, value_ptr( View ) );
    }
  }

  void Camera::DebugPrint()
  {
    gameObject->GetComponent<Transform>()->DebugPrint();
    SDL_Log( "GLM: %s", to_string( View ).c_str() );
    SDL_Log( "PYR: %f %f %f", PYR.x, PYR.y, PYR.z );
    // SDL_Log( "zoom: %f", zoom );
  }

  void Camera::AddLocalAcceleration( float right, float up, float forward )
  {
    LocalAcceleration += vec3( right, up, -forward );
  }

  void Camera::RotatePYR( float pitch, float yaw, float roll )
  {
    PYRAccum += vec3( pitch, yaw, roll );
  }

  void Camera::ZoomTo( float target, float rate = KeepCurrentRate )
  {
    zoomTarget = Clamp<float>( target, zoomMin, zoomMax );
    SDL_Log( "zoomTarget: %f", zoomTarget );
    if( rate != KeepCurrentRate )
    {
      zoomRate = rate;
    }
    if( rate < 0.001f )
    {
      zoom = zoomTarget;
    }
    isZooming = true;
  }
}  // namespace Spaghetti
