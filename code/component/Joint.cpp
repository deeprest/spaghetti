#include "Joint.h"
#include "Rigidbody.h"
#include "Scene.h"
#include "Serialize.h"
#include "Transform.h"

namespace Spaghetti
{
  using namespace glm;

  bool Joint::Initialize( const json& param )
  {
    int rbA = param["rigidbodyA"]["id"];
    rigidbodyA = Component::GetComponentByID<Rigidbody>( rbA );
    if( !rigidbodyA )
    {
      SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "Cannot find joint rigidbodyA ID %d", rbA );
      return false;
    }

    int rbB = param["rigidbodyB"]["id"];
    rigidbodyB = Component::GetComponentByID<Rigidbody>( rbB );
    if( !rigidbodyB )
    {
      SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "Cannot find joint rigidbodyB ID %d", rbB );
      return false;
    }
    // copy init params for later
    this->param = param;
    // Delayed initialization.
    // Wait until the rigidbody components are intialized before the joint can be created.
    rigidbodyA->AddInitializeCallback( [&](){ InitJoint(); });
    rigidbodyB->AddInitializeCallback( [&](){ InitJoint(); });
    // InitJoint();
    return true;
  }

  void Joint::InitJoint()
  {
    if( joint==nullptr )
    {
      if( rigidbodyA->initialized && rigidbodyB->initialized )
      {
        string typestring = Read<string>( param, "type", "revolute" );

        if( typestring == "revolute" )
        {
          b2RevoluteJointDef def;
          def.enableLimit = true;
          def.lowerAngle = Read<float>( param, "angleMin", -0.6 );
          def.upperAngle = Read<float>( param, "angleMax", 0.6 );

          def.enableMotor = false;
          def.maxMotorTorque = Read<float>( param, "maxMotorTorque", 1000 );

          b2Transform xf = rigidbodyA->body->GetTransform();
          // b2Vec2 pos = xf.p;
          // b2Rot rot = xf.q;
          // glm::vec3 worldAnchor = rigidbodyA->boundTransform->GetPosition(); //GetWorldPosition( anchor );
          def.Initialize( rigidbodyA->body, rigidbodyB->body, xf.p );
          def.userData = this; // static_cast<void*>( this );
          joint = scene->world->CreateJoint( &def );
        }
      }
    }
  }

  void Joint::OnDestroy()
  {
    SDL_Log("joint destruction");
    // if( joint )
    // {
    //   if( rigidbodyA && rigidbodyA->body )
    //   {
    //     if( rigidbodyB && rigidbodyB->body )
    //     {
    //       scene->world->DestroyJoint( joint );
    //     }
    //   }
    // }
    joint = nullptr;
    rigidbodyA = nullptr;
    rigidbodyB = nullptr;
  }
}
