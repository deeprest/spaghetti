#pragma once
#include "SDL2/SDL_mixer.h"
#include "Audio.h"
#include "Component.h"
#include "json.hpp"
#include <string>


namespace Spaghetti
{
  using string = std::string;

  struct AudioSource : public Component
  {
    bool Initialize( const nlohmann::json& params );
    void OnDestroy();
    void Play( float volume=1, bool force = false );

    // void Play( const AudioClip& clip );
    bool IsPlaying = false;
    string CurrentSound;

    Mix_Chunk* sample;
    int channel;


  };
}
