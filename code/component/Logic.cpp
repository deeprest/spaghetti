#include "Logic.h"

#include <string>
#include <unordered_map>
#include <vector>

#include "ComponentHeaders.h"
#include "Mathematics.h"
#include "MemoryPool.h"
#include "Resource.h"
#include "SDL2/SDL_keycode.h"
#include "Scene.h"
#include "Timer.h"
#include "glm/glm.hpp"
#include "json.hpp"

namespace Spaghetti
{
  using namespace glm;

  bool Logic::Initialize( const nlohmann::json& params )
  {
    if( params.count( "function" ) )
    {
      string name = params["function"];

      if( name == "camera" )
        logicFunction = &Logic::CameraLogic;
      if( name == "creature" )
        logicFunction = &Logic::CreatureLogic;
    }

    torque = 3;
    return true;
  }

  void Logic::Update( float DeltaTime )
  {
    elapsedTime += DeltaTime;

    if( logicFunction )
    {
      logicFunction( *this );
    }
  }


  void Logic::CameraLogic()
  {
    Camera* camera = gameObject->GetComponent<Camera>();
    Transform* transform = gameObject->GetComponent<Transform>();

    /*if( Input::KeyPressed( SDLK_c ) )
    {
      scene->bRenderDebug = !scene->bRenderDebug;
    }*/

    if( lookTarget != nullptr )
    {
      vec3 targetPosition = lookTarget->GetComponent<Transform>()->GetPosition();
      targetPosition.z = transform->GetPosition().z;
      transform->SetPosition( targetPosition );

      float mx = Input::GetAxis( "mouse.x" );
      float my = Input::GetAxis( "mouse.y" );
      CameraMouseCoef = 1.0f;
      camera->RotatePYR( /*my * CameraMouseCoef*/ 0, mx * CameraMouseCoef, 0 );

      b2Vec2 moveTargetPosition( 0, 8 );
      std::vector<Rigidbody*>& bodies = lookTarget->GetComponents<Rigidbody>();
      b2Vec2 dir = moveTargetPosition - bodies[0]->body->GetPosition();
      float distance = dir.Normalize();

      // turn head towards target position
      if( Input::KeyHeld( SDLK_m ) )
      {
        if( distance > distanceThreshold )
        {
          float headRotSpeed = bodies[0]->body->GetAngularVelocity();
          float headDot = b2Dot( bodies[0]->body->GetTransform().q.GetXAxis(), dir );
          bodies[0]->body->ApplyTorque( -headRotSpeed * turnBrake + -headDot * 10, true );
        }
      }

      // move bodies towrads target position using dot
      if( Input::KeyHeld( SDLK_n ) )
      {
        for( auto& rb : bodies )
        {
          float dot = b2Dot( rb->body->GetTransform().q.GetYAxis(), dir );
          float fdot = ( dot + 1.0 ) * 0.5;
          float mass = rb->body->GetMass();
          rb->body->ApplyForceToCenter( b2Vec2( dir.x * mass * 4 * fdot, dir.y * mass * 4 * fdot ), true );
        }
      }

      // TOTAL MASS
      float totalMass = 0;
      for( auto& rb : bodies )
      {
        totalMass += rb->body->GetMass();
      }

      glm::vec3 direction = glm::vec3( glm::vec4( 0, 1, 0, 0 ) * transform->WorldMatrix );
      b2Vec2 forward( direction.x, direction.y );

      // turn towards facing direction
      float headRotSpeed = bodies[0]->body->GetAngularVelocity();
      float turnDot = b2Dot( bodies[0]->body->GetTransform().q.GetXAxis(), forward );
      bodies[0]->body->ApplyTorque( ( -headRotSpeed * turnBrake + -turnDot * turnSpeed ) * totalMass, true );

      if( Input::KeyHeld( SDLK_w ) )
      {
        for( auto& rb : bodies )
        {
          float mass = rb->body->GetMass();
          float dot = b2Dot( rb->body->GetTransform().q.GetYAxis(), forward );
          rb->body->ApplyForceToCenter( b2Vec2( direction.x * dot * mass * forwardSpeed * Time::deltaTime, direction.y * dot * mass * forwardSpeed * Time::deltaTime ), true );
        }
        // tail flip
        flipTime += Time::deltaTime;
        if( flipTime > flipInterval )
        {
          flipTime -= flipInterval;
          torque = -torque;
          bool first = true;
          // std::vector<Rigidbody*>& bodies = gameObject->GetComponents<Rigidbody>();
          for( auto& rb : bodies )
          {
            if( first )
            {
              first = false;
              continue;
            }
            float mass = rb->body->GetMass();
            rb->body->ApplyAngularImpulse( torque * ( 1.0f + mass * mass ), true );
          }
        }
      }
      if( Input::KeyHeld( SDLK_s ) )
      {
        for( auto& rb : bodies )
        {
          float mass = rb->body->GetMass();
          float dot = b2Dot( rb->body->GetTransform().q.GetYAxis(), forward );
          rb->body->ApplyForceToCenter( b2Vec2( -direction.x * dot * mass * forwardSpeed * Time::deltaTime, -direction.y * dot * mass * forwardSpeed * Time::deltaTime ), true );
        }
      }
    }
    else
    {
      float tempMoveSpeed = 1.0f;

      float mx = Input::GetAxis( "mouse.x" );
      float my = Input::GetAxis( "mouse.y" );
      CameraMouseCoef = 1.0f;
      camera->RotatePYR( my * CameraMouseCoef, mx * CameraMouseCoef, 0 );

      // roll camera
      /*if( Input::KeyHeld( SDLK_c ) )
        camera->RotatePYR( 0, 0,-tempMoveSpeed);
      if( Input::KeyHeld( SDLK_z ) )
        camera->RotatePYR( 0,0,tempMoveSpeed );*/

      if( Input::KeyHeld( SDLK_w ) )
        camera->AddLocalAcceleration( 0, 0, tempMoveSpeed );

      if( Input::KeyHeld( SDLK_s ) )
        camera->AddLocalAcceleration( 0, 0, -tempMoveSpeed );

      if( Input::KeyHeld( SDLK_d ) )
        camera->AddLocalAcceleration( tempMoveSpeed, 0, 0 );

      if( Input::KeyHeld( SDLK_a ) )
        camera->AddLocalAcceleration( -tempMoveSpeed, 0, 0 );

      if( Input::KeyHeld( SDLK_e ) )
        camera->AddLocalAcceleration( 0, tempMoveSpeed, 0 );

      if( Input::KeyHeld( SDLK_q ) )
        camera->AddLocalAcceleration( 0, -tempMoveSpeed, 0 );
    }

    if( Input::KeyPressed( SDLK_RETURN ) )
    {
      GameObject* go = scene->GenerateCreature( Resource::ReadJSON( "creature-params.json" ) );
      if( go )
      {
        lookTarget = go;
        lookTarget->OnDestroy = [&]()
        {
          lookTarget = nullptr;
        };
      }
    }

    if( lookTarget && lookTarget->GetComponent<Rigidbody>() == nullptr )
      lookTarget = nullptr;
  }


  void Logic::CreatureLogic()
  {
    if( Input::KeyPressed( SDLK_BACKSPACE ) )
    {
      GameObject::Destroy( gameObject );
    }

    if( Input::KeyPressed( SDLK_k ) )
    {
      float totalMass = 0;
      std::vector<Rigidbody*>& bodies = gameObject->GetComponents<Rigidbody>();
      for( auto& rb : bodies )
      {
        float mass = rb->body->GetMass();
        totalMass += mass;
        SDL_Log( "Mass: %f", mass );
      }
      SDL_Log( "TotalMass: %f", totalMass );

      torque = totalMass;
    }

    if( Input::KeyPressed( SDLK_SPACE ) )
    {
      std::vector<Rigidbody*>& bodies = gameObject->GetComponents<Rigidbody>();
      for( auto& rb : bodies )
      {
        b2Vec2 dir = b2Vec2( -3, 5 ) - rb->body->GetPosition();
        float push = pushAmount * rb->body->GetMass();
        rb->body->ApplyForceToCenter( b2Vec2( dir.x * push, dir.y * push ), true );
      }
    }
  }
}  // namespace Spaghetti
