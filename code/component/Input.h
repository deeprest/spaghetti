#pragma once
#include <unordered_map>
#include <memory>
#include <string>
#include "SDL2/SDL.h"
#include "json.hpp"
#include "Component.h"

namespace Spaghetti
{
  struct Input : public Component
  {
    static uint32_t KeyHeldEvent;
    // static std::shared_ptr<Input> activeInputComponent;
    static Input* activeInputComponent;

    bool Initialize( const nlohmann::json& params );
    void Activate();
    void PreFrame();
    void HandleEvent( SDL_Event event);

    static bool KeyHeld( int keycode );
    static bool KeyPressed( int keycode );
    static float GetAxis( const std::string& axisName );


    std::unordered_map<int,bool> keyDown;
    std::unordered_map<int,bool> keyPressed;
    std::vector<int> newlyPressed;
    std::unordered_map<string,float> axis;
    
  };

}
