#pragma once

#include <map>
#include <unordered_map>
#include <vector>

#include "GL.h"
#include "glm/mat4x4.hpp"
#include "glm/vec3.hpp"
#include "json.hpp"

#include "Component.h"

namespace Spaghetti
{
  struct Transform : public Component
  {
    private:
      // transient
      glm::mat4 Model;
      glm::mat3 NormalMatrix;
      // serialized
      Transform* parent;

    public:
      static GLuint LocationNormalMatrix;
      static GLuint LocationModelMatrix;

      // serialized
      glm::mat4 LocalMatrix;
      // transient
      glm::mat4 WorldMatrix;
      int depth;
      bool updatedThisFrame = false;

      void UpdateLocalMatrix();
      void UpdateWorldMatrix();

      bool Initialize( const nlohmann::json& obj );
      void OnDestroy();

      Transform* GetParent();
      // TODO: params to preserve the local or world transform
      void SetParent( Transform* newParent );
      // world space if not specified
      glm::vec3 GetPosition() const;
      void SetPosition( const glm::vec3& pos );
      glm::vec3 GetLocalPosition();
      void SetLocalPosition( const glm::vec3& pos );
      glm::vec3 GetWorldPosition( const glm::vec3& localPosition );

      glm::mat4 GetRotation();
      void SetRotation( const glm::mat4& rot );
      void SetRotation( const glm::vec3& axis, float angle );
      glm::mat4 GetLocalRotation();
      void SetLocalRotation( const glm::mat4& rot );
      // void SetRotation( float roll, float pitch, float yaw );
      void SetLookDirection( const glm::vec3& dir );

      void SetUniformScale( float scale );

      void Rotate( const glm::vec3& axis, float angle );
      void RotateLocal( const glm::vec3& axis, float angle );

      void DebugPrint();
      void ApplyShaderValues();

      inline int GetDepth() { return depth; }
  };
}
