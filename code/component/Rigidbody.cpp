#include "Rigidbody.h"
#include "glm/glm.hpp"
#include "Scene.h"
#include "Serialize.h"
#include "Transform.h"
#include "MeshView.h"


namespace Spaghetti
{
  using namespace glm;
  using namespace std::placeholders;

  void to_json( json& obj, const Rigidbody& rb )
  {
  }
  void from_json( const json& obj, Rigidbody& rb )
  {
  }

  void Rigidbody::AddInitializeCallback( RigidbodyInitializeCallback cb )
  {
    if( initialized )
      cb();
    else
      callback.push_back( cb );
  }

  bool Rigidbody::  Initialize( const json& param )
  {
    b2Body* attachToBody = nullptr;
    if( param.count( "attach" ) )
    {
      int attachToID = param["attach"]["id"];
      Rigidbody* attachTo = Component::GetComponentByID<Rigidbody>( attachToID );
      if( attachTo )
      {
        attachToBody = attachTo->body;
      }
      else
      {
        SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "Cannot find rigidbody attach ID" );
        return false;
      }
    }
    else
    {
      boundTransform = nullptr;
      if( param.count( "transform" ) )
      {
        int tid = param["transform"]["id"];
        boundTransform = Component::GetComponentByID<Transform>( tid );
        if( !boundTransform )
        {
          SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "Cannot find rigidbody transform ID" );
          return false;
        }
      }
      if( !boundTransform )
      {
        boundTransform = gameObject->GetComponent<Transform>();
        if( !boundTransform )
        {
          SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "Cannot bind rigidbody to transform." );
          return false;
        }
      }

      Transform& transform = *boundTransform;

      b2BodyDef def;
      def.userData = static_cast<void*>( this );
      string typestring = Read<string>( param, "type", "static" );
      if( typestring == "static" )
        def.type = b2_staticBody;
      if( typestring == "dynamic" )
        def.type = b2_dynamicBody;
      if( typestring == "kinematic" )
        def.type = b2_kinematicBody;

      def.position = b2Vec2( transform.GetPosition().x, transform.GetPosition().y );
      //def.angle = Read<float>( param, "angle", 0 );
      def.active = Read<bool>( param, "active", true );
      def.allowSleep = true;
      def.linearDamping = 0.5;
      def.angularDamping = 0.5;

      body = scene->world->CreateBody( &def );
      attachToBody = body;
    }
    if( attachToBody )
    {
      if( param.count( "shape" ) )
      {

        const json& shape = param.at( "shape" );
        for( auto& obj : shape )
        {
          b2FixtureDef fixdef;
          fixdef.density = 0.5f;

          fixdef.restitution = 0.9f;

          string type = Read<string>( obj, "type", "circle" );
          if( type == "circle" )
          {
            b2CircleShape circle;
            circle.m_radius = Read<float>( obj, "radius", 0.5f );
            if( obj.count( "position" ) )
            {
              std::vector<float> pos = obj["position"].get<std::vector<float>>();
              circle.m_p = b2Vec2( pos[0], pos[1] );
            }
            fixdef.shape = &circle;
          }
          else if( type == "polygon" )
          {
            b2PolygonShape poly;
            std::vector<b2Vec2> vert = {{-0.5f, 0}, {-0.5f, -1}, {0.5f, -1}, {0.5f, 0}};
            poly.Set( vert.data(), 4 );
            fixdef.shape = &poly;
            // attachToBody->CreateFixture( &poly, 1 );
          }
          attachToBody->CreateFixture( &fixdef );
        }
      }
    }
    initialized = true;
    for( auto cb : callback )
    {
      cb();
    }
    callback.clear();
    return true;
  }


  void Rigidbody::SetPosition( glm::vec3 pos )
  {
    body->SetTransform( b2Vec2( pos.x, pos.y ), 0 );
  }

  vec3 Rigidbody::GetPosition()
  {
    b2Transform xf = body->GetTransform();
    return vec3( xf.p.x, xf.p.y, 0 );
  }

  void Rigidbody::OnDestroy()
  {
    SDL_Log("rigidbody destruction");
    if( body )
    {
      b2JointEdge* je = body->GetJointList();
      while( je )
      {
        if( je->joint )
        {
          SDL_Log("rigibody destroy joint");
          Joint* jnt = static_cast<Joint*>( je->joint->GetUserData() );
          Component::Destroy( jnt );
        }
        je = je->next;
      }

      // body->SetActive( false );
      body->GetWorld()->DestroyBody( body );
      body = nullptr;
    }
    initialized = false;
    contactCallback = nullptr;

    // find meshview that shares the bound transform
    std::vector<MeshView*> mvRemove;
    std::vector<MeshView*>& mvs = gameObject->GetComponents<MeshView>();
    for( auto& mv : mvs )
      if( mv->renderTransform == boundTransform )
        mvRemove.push_back( mv );
    for( auto& mv : mvRemove )
        Component::Destroy( mv );
    std::vector<Transform*>& tfs = gameObject->GetComponents<Transform>();
    for( auto& tf : tfs )
      if( tf == boundTransform )
        Component::Destroy( tf );
  }

}
