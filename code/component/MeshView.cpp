#include "MeshView.h"

#include "Pool.h"
#include "MemoryPool.h"
// #include "Component.h"
#include "Mesh.h"
#include "Debug.h"
#include "Resource.h"
#include "Serialize.h"
#include "Transform.h"


namespace Spaghetti
{
  using json = nlohmann::json;

  bool MeshView::Initialize( const nlohmann::json& obj )
  {
    if( obj.count( "transform" ) )
    {
      json p = obj.at( "transform" );
      if( p.count( "id" ) )
      {
        int pid = Read<int>( p, "id", -1 );
        renderTransform = Component::GetComponentByID<Transform>( pid );
        if( !renderTransform )
        {
          SDL_Log( "cannot find meshview transform" );
          return false;
        }
      }
    }
    if( renderTransform == nullptr )
    {
      renderTransform = gameObject->GetComponent<Transform>();
    }

    if( obj.count( "mesh" ) )
    {
      json p = obj.at( "mesh" );
      if( p.count( "name" ) )
      {
        // look up shared mesh by name
        string name = Read<std::string>( p, "name", "unknown" );
        Pool<Mesh>& meshes = Memory::GetPool<Mesh>();
        for( auto& m : meshes.pool )
        {
          if( m.name.compare( name ) == 0 )
          {
            renderMesh = &m;
            break;
          }
        }
      }
      else
      {
        Spaghetti::Pool<Mesh>& meshes = Memory::GetPool<Mesh>();
        Mesh* m = meshes.New();
        if( m!=nullptr && m->Initialize( p ) )
        {
          renderMesh = m;
        }
        else
        {
          SDL_LogError( SDL_LOG_CATEGORY_APPLICATION, "failed to generate mesh" );
          return false;
        }
      }
    }

    if( obj.count( "material" ) )
    {
      // materialName = Read<std::string>(obj,"name","noname");
      string materialName = obj["material"];
      Material* check = Material::GetMaterial( materialName );
      if( check )
      {
        material = check;
      }
      else
      {
        string materialPath = string( "material/" ) + materialName + ".json";
        // TODO preallocate materials in a memory pool
        material = new Material();
        material->Initialize( Resource::ReadJSON( materialPath.c_str() ) );

        material->Validate();
      }
    }

    active = true;

    return true;
  }

  void MeshView::OnDestroy()
  {
    renderTransform = nullptr;

    // TODO deincrement reference count for mesh, do garbage sweep later
    //Pool<Mesh>& meshes = GetPool<Mesh>();

    renderMesh = nullptr;
    material = nullptr;
  }

  void MeshView::Activate()
  {
    active = true;
    // if( DepthOff )
    //   glDisable( GL_DEPTH_TEST );
    // glBlendFunc( GL_ONE_MINUS_SRC_ALPHA );
  }

  void MeshView::Deactivate()
  {
    active = false;
    // if( DepthOff )
    //   glEnable( GL_DEPTH_TEST );
  }

  void MeshView::UpdateView( GLuint OverrideRenderMode ) const
  {
    if( !active )
      return;
    if( !renderMesh )
      return;
    Mesh& mesh = *renderMesh;
    if( !mesh.valid )
      return;
    if( !material )
      return;
    if( !renderTransform )
      return;

    renderTransform->ApplyShaderValues();

    // GL 3.0
    glBindVertexArray( mesh.vertexArrayID );
    glBindBuffer( GL_ARRAY_BUFFER, mesh.vbo );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.vboIndices );

    for( const auto& attribute : material->attribute3f )
    {
      glEnableVertexAttribArray( attribute.second );
      glVertexAttribPointer( attribute.second, 3, GL_FLOAT, GL_FALSE, 0, mesh.attributeOffset[attribute.first] );
    }
    for( const auto& attribute : material->attribute2f )
    {
      glEnableVertexAttribArray( attribute.second );
      glVertexAttribPointer( attribute.second, 2, GL_FLOAT, GL_FALSE, 0, mesh.attributeOffset[attribute.first] );
    }

    if( !OverrideRenderMode )
      OverrideRenderMode = RenderMode;

    // glDrawArrays( GL_TRIANGLES, 0, indexCount );
    // #if defined( __IPHONEOS__ )
    //     glDrawElements( OverrideRenderMode, mesh.vertexCount, GL_UNSIGNED_SHORT, NULL );
    // #else
    glDrawRangeElements( OverrideRenderMode, 0, mesh.vertexCount, mesh.indexCount, GL_UNSIGNED_SHORT, NULL );
    // #endif
    for( const auto& attribute : material->attribute3f )
    {
      glDisableVertexAttribArray( attribute.second );
    }
    for( const auto& attribute : material->attribute2f )
    {
      glDisableVertexAttribArray( attribute.second );
    }

    // glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
    // glBindBuffer( GL_ARRAY_BUFFER, 0 );
    // glBindVertexArray( 0 );

    if( Debug::CheckGLErrors( __FILE__, __LINE__ ) )
    {
      SDL_Log( "Mesh: %s", mesh.name.c_str() );
      SDL_Log( "Material: %s", material->name.c_str() );
    }
  }
}
