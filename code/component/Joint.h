#pragma once

#include "json.hpp"
#include "Component.h"
#include "Box2D/Dynamics/Joints/b2Joint.h"

namespace Spaghetti
{
  using json = nlohmann::json;
  struct Rigidbody;

  struct Joint : public Component
  {
    bool Initialize( const json& param );
    void OnDestroy();

    b2Joint* joint;
    Rigidbody* rigidbodyA;
    Rigidbody* rigidbodyB;
    json param;

    private:
    void InitJoint();
  };
}
