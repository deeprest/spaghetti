#include "Transform.h"

#include <cfloat>
#include <cmath>
#include <vector>

#include "SDL2/SDL_log.h"

// #include "glm/glm.hpp"
#include "glm/vec4.hpp"
#include "glm/gtc/matrix_transform.hpp"  // translate, rotate, scale, perspective
#include "glm/gtc/matrix_access.hpp"
#include "glm/gtc/type_ptr.hpp"  // value_ptr
#include "glm/gtx/string_cast.hpp"

#include "Common.h"
#include "Scene.h"
#include "Material.h"
#include "Camera.h"

namespace Spaghetti
{
  using namespace glm;

  GLuint Transform::LocationModelMatrix;  // local to world transform matrix
  GLuint Transform::LocationNormalMatrix;

  void Transform::DebugPrint()
  {
    std::stringstream ss;
    ss << "Transform id:" << id;
    SDL_Log( "%s", ss.str().c_str() );
    // SDL_Log( "Transform id: %llu ", id );
    SDL_Log( "LOCAL MATRIX: %s", to_string( LocalMatrix ).c_str() );
    SDL_Log( "WORLD MATRIX: %s", to_string( WorldMatrix ).c_str() );
  }

  bool Transform::Initialize( const nlohmann::json& obj )
  {
    if( obj.count("localMatrix") )
    {
      std::vector<float> m = obj["localMatrix"].get<std::vector<float>>();
      LocalMatrix = mat4( m[0],m[1],m[2],m[3],m[4],m[5],m[6],m[7],m[8],m[9],m[10],m[11],m[12],m[13],m[14],m[15] );
    }
    else
    {
      LocalMatrix = mat4(1.0f);
    }
    depth = 0;
    scene->AddTransform( depth, id, this );
    if( obj.count("parent")>0 )
    {
      json p = obj.at("parent");
      if( p.count("id") )
      {
        json pid = p.at("id");
        parent = Component::GetComponentByID<Transform>( pid );
      }
    }
    else
    {
      parent = nullptr;
    }
    UpdateWorldMatrix();
    // std::vector<float> m = obj.at("localMatrix").get<std::vector<float>>();
    // LocalMatrix = mat4( m[0],m[1],m[2],m[3], m[4],m[5],m[6],m[7], m[8],m[9],m[10],m[11], m[12],m[13],m[14],m[15] );
    return true;
  }

  void Transform::OnDestroy()
  {
    scene->RemoveTransform( this );
  }

  void Transform::ApplyShaderValues()
  {
    GLuint programID = Material::GetCurrent()->GetProgramID();

    LocationModelMatrix = glGetUniformLocation( programID, "ModelView" );
    Model = Camera::View * WorldMatrix;
    glUniformMatrix4fv( LocationModelMatrix, 1, GL_FALSE, value_ptr( Model ) );

    LocationNormalMatrix = glGetUniformLocation( programID, "NormalMatrix" );
    NormalMatrix = transpose( inverse( mat3( WorldMatrix ) ) );
    glUniformMatrix3fv( LocationNormalMatrix, 1, GL_FALSE, value_ptr( NormalMatrix ) );
  }

  void Transform::UpdateWorldMatrix()
  {
    mat4 ParentWorldMatrix = mat4(1.0f);
    if( parent!=nullptr )
    {
      ParentWorldMatrix = parent->WorldMatrix;
    }
    WorldMatrix = ParentWorldMatrix * LocalMatrix;
  }

  void Transform::UpdateLocalMatrix()
  {
    mat4 ParentRotation = mat4(1.0f);
    if( parent!=nullptr )
    {
      ParentRotation = parent->GetRotation();
    }
    LocalMatrix = inverse( ParentRotation ) * WorldMatrix;
  }

  vec3 Transform::GetLocalPosition()
  {
    return vec3( LocalMatrix[3] );
  }

  vec3 Transform::GetPosition() const
  {
    return vec3( WorldMatrix[3] );
  }

  void Transform::SetLocalPosition( const vec3& pos )
  {
    LocalMatrix[3] = vec4( pos, 1.0f );
    UpdateWorldMatrix();
  }

  void Transform::SetPosition( const vec3& pos )
  {
    WorldMatrix[3] = vec4( pos, 1.0f );
    UpdateLocalMatrix();
  }

  glm::vec3 Transform::GetWorldPosition( const glm::vec3& localPosition )
  {
    return vec3( vec4(localPosition,0) * WorldMatrix );
  }

  mat4 Transform::GetRotation()
  {
    mat4 out = WorldMatrix;
    out[3] = vec4( 0, 0, 0, 1 );
    out[0][3] = 0.0f;
    out[1][3] = 0.0f;
    out[2][3] = 0.0f;
    return out;
  }

  void Transform::SetRotation( const mat4& rot )
  {
    vec4 translation = WorldMatrix[3];
    WorldMatrix = rot;
    WorldMatrix[3] = translation;
  }

  void Transform::SetRotation( const vec3& axis, float angle )
  {
    SetRotation( glm::rotate( glm::mat4(1.0f), angle, axis ) );
  }

  mat4 Transform::GetLocalRotation()
  {
    mat4 out = LocalMatrix;
    out[3] = vec4( 0, 0, 0, 1 );
    out[0][3] = 0.0f;
    out[1][3] = 0.0f;
    out[2][3] = 0.0f;
    return out;
  }

  void Transform::SetLocalRotation( const mat4& rot )
  {
    vec4 translation = LocalMatrix[3];
    LocalMatrix = rot;
    LocalMatrix[3] = translation;
  }

  void Transform::Rotate( const vec3& axis, float angle )
  {
    WorldMatrix = glm::rotate( WorldMatrix, angle, axis );
    UpdateLocalMatrix();
  }

  void Transform::RotateLocal( const vec3& axis, float angle )
  {
    LocalMatrix = glm::rotate( LocalMatrix, angle, axis );
    UpdateWorldMatrix();
  }

  void Transform::SetLookDirection( const vec3& dir )
  {
    LocalMatrix = glm::lookAtRH( vec3( LocalMatrix[3] ), vec3( LocalMatrix[3] ) + dir, vec3( 0, 0, 1 ) );
  }

  // void Transform::GatherAllChildren( std::vector<Transform*>& all );
  // {
  //   for(auto& c : children)
  //     all.push_back( c->GatherAllChilren( all ));
  // }

  void Transform::SetParent( Transform* newParent )
  {
    if( parent == newParent )
      return;

    parent = newParent;
    int oldDepth = depth;
    // std::vector<Transform*> sibling = parent->children;
    if( newParent == nullptr )
    {
      depth = 0;
      // if( sibling.size()>1 )
      // {
      //   sibling[childIndex] = sibling.back();
      // }
      // sibling.pop_back();
      // childIndex=-1;
    }
    else
    {
      depth = newParent->depth + 1;
      // sibling.push_back();
      // childIndex = children.size()-1;
    }

    if( oldDepth != depth )
    {
      scene->transformLayer[ oldDepth ].erase( id );
      scene->transformLayer[ depth ][ id ] = this;
    }
    // update matrices from new parent
    UpdateLocalMatrix();
    UpdateWorldMatrix();
  }

  Transform* Transform::GetParent()
  {
    return parent;
  }

}
