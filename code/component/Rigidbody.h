#pragma once
#include "Box2D/Box2D.h"
#include "Component.h"
#include "SDL2/SDL.h"
#include "glm/glm.hpp"
#include "json.hpp"
#include <functional>
#include <vector>

// b2Settings.h
#undef b2_timeToSleep
#define b2_timeToSleep 0.1f

namespace Spaghetti
{
  using json = nlohmann::json;

  typedef std::function<void()> RigidbodyInitializeCallback;
  // typedef std::function<void()> RigidbodyDestroyCallback;
  typedef std::function<void(Rigidbody*,Rigidbody*,b2Contact*)> RigidbodyContactCallback;

  struct Rigidbody : public Component
  {
    bool Initialize( const json& param );
    void SetPosition( glm::vec3 pos );
    glm::vec3 GetPosition();
    void OnDestroy();


    bool initialized = false;
    std::vector<RigidbodyInitializeCallback> callback;
    void AddInitializeCallback( RigidbodyInitializeCallback cb );

    Transform* boundTransform = nullptr;
    b2Body* body = nullptr;
    b2Fixture* fixture = nullptr;
    /*json param;*/

    RigidbodyContactCallback contactCallback = nullptr;

    // RigidbodyDestroyCallback destroyCallback = nullptr;
    //std::vector<RigidbodyDestroyCallback> callback;
    // void AddDestroyCallback( RigidbodyDestroyCallback cb );

  };
}
