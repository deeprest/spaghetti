#pragma once

#include "SDL2/SDL_platform.h"


#ifdef __LINUX__
#include "SDL2/SDL_opengl.h"
#include "SDL2/SDL_opengl_glext.h"

#elif __MACOSX__
#include "SDL2/SDL_opengl.h"
#include "SDL2/SDL_opengl_glext.h"

#elif __WINDOWS__
// mingw
#else
//#error "Unsupported platform"
#endif


// #include "SDL2/SDL_image.h"
