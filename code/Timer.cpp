#include "Timer.h"
#include "Mathematics.h"  // for Clamp()
#include "SDL2/SDL.h"
#include <vector>
#include <ctime>
#include <iomanip>
#include <sstream>
// #include "SDL2/SDL_timer.h"

namespace Spaghetti
{

  int32_t Time::startTick = 0;
  int32_t Time::thisTick = 0;
  int32_t Time::lastTick = 0;
  int32_t Time::deltaTick = 0;
  float Time::deltaTime = 0.0f;
  int32_t Time::deltaTickAccumulator = 0;
  int32_t Time::elapsedSeconds = 0;

  uint32_t totalFrames = 0;
  uint32_t FramesThisSecond = 0;
  uint32_t maxFPS = 0;
  uint32_t minFPS = 1000;
  float averageFPS = 0.0f;


  void Time::Initialize()
  {
    thisTick = SDL_GetTicks();
    startTick = thisTick;
  }

  void Time::Update()
  {
    lastTick = thisTick;
    thisTick = SDL_GetTicks();
    // This keeps the delta-time at a maximum. Otherwise, a really large delay could break the continuity of the
    //  physics simulation, and collision bodies could end up outside the world  when the program resumes updating.
    // A large delay will happen when the user drags the window in windowed mode, for instance.
    deltaTick = Clamp<int32_t>( thisTick - lastTick, 0, 300 );
    deltaTickAccumulator += deltaTick;
    deltaTime = ( (float)deltaTick ) / 1000.0f;
    ++FramesThisSecond;

    if( deltaTickAccumulator >= 1000 )
    {
      deltaTickAccumulator = deltaTickAccumulator - 1000;
      elapsedSeconds++;
      totalFrames += FramesThisSecond;
      averageFPS = (float)totalFrames / ( (float)elapsedSeconds );
      maxFPS = SDL_max( maxFPS, FramesThisSecond );
      minFPS = SDL_min( minFPS, FramesThisSecond );
      //SDL_Log( "FPS[%d,%2f,%d]", minFPS, averageFPS, maxFPS );
      FramesThisSecond = 0;
    }

    Timer::UpdateTimers();
  }

  float Time::GetElapsedSeconds()
  {
    return ( (float)( thisTick - startTick ) ) / 1000.0f;
  }

  std::string Time::Now()
  {
    std::time_t result = std::time(nullptr);
    std::ostringstream ss;
    ss << std::put_time( std::localtime(&result), "%Y%m%d.%H%M%S");
    return ss.str();
    // time_t now = std::time( nullptr );
    // return std::string( asctime( localtime( &now ) ) );
  }


  std::vector<Timer> Timer::AllTimers;

  void Timer::UpdateTimers()
  {
    for( auto& t : AllTimers )
      if( t.active )
        t.Update();
  }

  void Timer::Initialize( size_t count )
  {
    AllTimers.reserve( count );
  }

  Timer& Timer::GetTimer()
  {
    if( AllTimers.size() + 1 > AllTimers.capacity() )
    {
      SDL_Log( "*** AllTimers resize. All references invalidated ***" );
      AllTimers.reserve( AllTimers.size() + 100 );
      SDL_Log( "New AllTimers capacity: %zu", AllTimers.capacity() );
    }
    AllTimers.emplace_back();
    return AllTimers[ AllTimers.size()-1 ];
  }

  Timer::Timer() : active( false ) {}

  void Timer::Start( int32_t timerDuration, TimerCallback timerCallback )
  {
    active = true;
    start = Time::thisTick;
    duration = timerDuration;
    callback = timerCallback;
  }

  void Timer::Stop( bool invokeCallback )
  {
    active = false;
    if( callback!=nullptr && invokeCallback )
      callback();
  }

  void Timer::Update()
  {
    if( Time::thisTick - start > duration )
      Stop();
  }
}
