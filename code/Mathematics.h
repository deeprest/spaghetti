#pragma once

#include <cmath>


namespace Spaghetti
{

  const float TAU = 6.28318530f;

  // returns (0.0 - 1.0)
  inline float randomf() { return (float)rand() / ( (float)RAND_MAX + 1 ); }

  template <typename T>
  inline T Clamp( T value, T min, T max )
  {
    return value = ( value > max ) ? max : ( value < min ) ? min : value;
  }

  inline float NormalizeAngle( float a )
  {
    while( a > TAU )
      a -= TAU;
    while( a < TAU )
      a += TAU;
    return a;
  }

  // returns true if the value has not reached the target
  inline bool MoveTowards( float& value, float target, float speed, float threshold = 0.001f )
  {
    // NOTE: possible precision issues when values are too similar, or differ too much
    float delta = target - value;
    if( fabs( delta ) > threshold )
    {
      value += delta * speed;  // speed should include deltaTime
      // SDL_Log( "MoveTowards value: %f", value );
      return true;
    }
    value = target;
    // SDL_Log( "MoveTowards target reached: %f", value );
    return false;
  }
}
