#pragma once

#include "Timer.h"

namespace Spaghetti
{

  struct SmoothSample
  {
    uint32_t timestamp;
    double value;
  };

  // template type is input/output. Uses doubles for smoothing and converts back
  template <typename T, size_t sampleCount>
  class Smoothie
  {
  public:
    Smoothie()
    {
      for( size_t i = 0; i < sampleCount; i++ )
      {
        sample[i].timestamp = 0;
        sample[i].value = 0.0;
      }
    }
    inline void AddSample( T newSample, uint32_t currentTick )
    {
      sample[smoothIndex].timestamp = currentTick;
      sample[smoothIndex].value = static_cast<double>( newSample );
      smoothIndex = ( smoothIndex + 1 == sampleCount ) ? 0 : smoothIndex + 1;  // circular indexing
    }
    inline T CalculateSmoothedValue( uint32_t currentTick )
    {
      double sum = 0.0f;
      for( size_t i = 0; i < sampleCount; ++i )
      {
        float gradient = 1.0f - Clamp<double>( static_cast<double>( currentTick - sample[i].timestamp ) / 1000.0 * smoothDuration, 0.0, 1.0 );
        sum += sample[i].value * gradient;
        //SDL_Log( "%zu %.2f %.2f", i, gradient, sample[i].value );
      }
      smoothedValue = static_cast<T>( sum / static_cast<double>( sampleCount ) );
      return smoothedValue;
    }
    //T GetValue(){ return smoothedValue; }
  private:
    SmoothSample sample[sampleCount];
    size_t smoothIndex = 0;
    double smoothDuration = 0.5f;  // in seconds
    T smoothedValue;
  };
}

// uint32_t currentTick = SDL_GetTicks();
// smooth[smoothIndex].timestamp = currentTick;  //event.motion.timestamp;
// smooth[smoothIndex].xd = (float)event.motion.xrel * CameraMouseCoef;
// // smooth[smoothIndex].xy
// smoothIndex = ( smoothIndex + 1 == smoothCount ) ? 0 : smoothIndex + 1;  // wrap around

// float smoothedX = 0.0f;
// for( int i = 0; i < smoothCount; ++i )
// {
//   float gradient = 1.0f - Clamp<float>( static_cast<float>( currentTick - smooth[i].timestamp ) / 1000.0f * smoothDuration, 0.0f, 1.0f );
//   smoothedX += smooth[i].xd * gradient;
//   SDL_Log( "%d %f %f %f", i, gradient, smooth[i].xd, smoothedX );
// }
// smoothedX /= (float)smoothCount;
// smoothLastTimestamp = event.motion.timestamp;

// float mouseXDelta = static_cast<float>( event.motion.xrel ) * CameraMouseCoef;
// float smoothedX = mx.Sample( mouseXDelta, Time::thisTick );
// SDL_Log( "SmoothedX: %f", smoothedX );
