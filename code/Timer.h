#pragma once

#include "Common.h"
#include "MemoryPool.h"
#include <functional>
#include <string>

namespace Spaghetti
{

  struct Time
  {
    static void Initialize();
    static void Update();
    static float GetElapsedSeconds();
    static std::string Now();

    static int32_t startTick;
    static int32_t thisTick;
    static int32_t lastTick;
    static int32_t deltaTick;
    static float deltaTime;
    static int32_t deltaTickAccumulator;
    static int32_t elapsedSeconds;
  };

  typedef std::function<void()> TimerCallback;

  struct Timer
  {
    static std::vector<Timer> AllTimers;
    static void UpdateTimers();

    static void Initialize( size_t count );
    static Timer& GetTimer();

    bool active;
    int32_t start;
    int32_t duration;
    TimerCallback callback;

    void Start( int32_t timerDuration, TimerCallback timerCallback );
    void Stop( bool invokeCallback = true );
    void Update();

    Timer();

  };
}
