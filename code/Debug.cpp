#include "Debug.h"

#include <cerrno>
#include <cfenv>
#include <cmath>
#include <cstring>
#include <ctime>
#include <iostream>
#include <string>
#include <unordered_map>

#include "Pool.h"
#include "MemoryPool.h"
#include "Mathematics.h"
#include "Resource.h"
#include "Timer.h"

#include "Mesh.h"
#include "Transform.h"
#include "MeshView.h"
#include "Material.h"

namespace Spaghetti
{
  using namespace std;
  using namespace glm;

  unordered_map<GLenum, string> enumMap;

  namespace Debug
  {
    void Initialize()
    {
      // CheckAlignment()
      // std::cout << "Alignment of"  "\n"
      //     "- char             : " << alignof(char)    << "\n"
      //     "- pointer          : " << alignof(int*)    << "\n"
      //     "- Transform        : " << alignof(Transform)     << "\n"
      //     "- GameObject      : " << alignof(GameObject)   << "\n"
      //     "- SDL_Event: " << alignof(SDL_Event) << "\n";
      enumMap[GL_NO_ERROR] = "No Error";
      enumMap[GL_INVALID_ENUM] = "Invalid Enum";
      enumMap[GL_INVALID_VALUE] = "Invalid Value";
      enumMap[GL_INVALID_OPERATION] = "Invalid Operation";
      enumMap[GL_INVALID_FRAMEBUFFER_OPERATION] = "Invalid Framebuffer Operation";
      enumMap[GL_OUT_OF_MEMORY] = "Out Of Memory";
#if !defined( __IPHONEOS__ )
      enumMap[GL_STACK_UNDERFLOW] = "Stack Underflow";
      enumMap[GL_STACK_OVERFLOW] = "Stack Overflow";
#endif
    }

    void CheckMath()
    {
      // Taken from http://en.cppreference.com/w/cpp/numeric/math/math_errhandling
      std::cout << "MATH_ERRNO is "
                << ( math_errhandling & MATH_ERRNO ? "set" : "not set" ) << '\n'
                << "MATH_ERREXCEPT is "
                << ( math_errhandling & MATH_ERREXCEPT ? "set" : "not set" ) << '\n';
      std::feclearexcept( FE_ALL_EXCEPT );
      errno = 0;
      std::cout << "log(0) = " << std::log( 0 ) << '\n';
      if( errno == ERANGE )
        std::cout << "errno = ERANGE (" << std::strerror( errno ) << ")\n";
      if( std::fetestexcept( FE_DIVBYZERO ) )
        std::cout << "FE_DIVBYZERO (pole error) reported\n";

      // Taken from http://en.cppreference.com/w/cpp/numeric/fenv/FE_exceptions
      volatile double zero = 0.0;  // volatile not needed where FENV_ACCESS is supported
      volatile double one = 1.0;  // volatile not needed where FENV_ACCESS is supported

      std::feclearexcept( FE_ALL_EXCEPT );
      std::cout << "1.0/0.0 = " << 1.0 / zero << '\n';
      if( std::fetestexcept( FE_DIVBYZERO ) )
      {
        std::cout << "division by zero reported\n";
      }
      else
      {
        std::cout << "divsion by zero not reported\n";
      }

      std::feclearexcept( FE_ALL_EXCEPT );
      std::cout << "1.0/10 = " << one / 10 << '\n';
      if( std::fetestexcept( FE_INEXACT ) )
      {
        std::cout << "inexact result reported\n";
      }
      else
      {
        std::cout << "inexact result not reported\n";
      }

      std::feclearexcept( FE_ALL_EXCEPT );
      std::cout << "sqrt(-1) = " << std::sqrt( -1 ) << '\n';
      if( std::fetestexcept( FE_INVALID ) )
      {
        std::cout << "invalid result reported\n";
      }
      else
      {
        std::cout << "invalid result not reported\n";
      }
    }

    int CheckGLErrors( const char* file, int line )
    {
      int errCount = 0;
      for( GLenum currError = glGetError(); currError != GL_NO_ERROR; currError = glGetError(), ++errCount )
      {
        string errstr = enumMap[currError];
        if( errstr.size() == 0 )
          errstr = std::to_string( (int)currError );
        SDL_LogError( SDL_LOG_CATEGORY_ERROR, "GL %s %s:%d", errstr.c_str(), file, line );
      }
      return errCount;
    }
  }

  Transform* xf;
  MeshView* mv;
  std::unordered_map<string, Mesh*> mesh;
  Material* debugMaterial;
  GLint colorLoc;

  void DebugDraw::Initialize( Scene* scene )
  {
    go = scene->CreateGameObject( R"(
    {
      "transform": [
        {
          "id": 1
        }
      ],
      "meshview": [
        {
          "material": "debug",
          "transform": { "id": 1 }
        }
      ]
    })"_json );

    xf = go->GetComponent<Transform>();
    mv = go->GetComponent<MeshView>();
    mv->Deactivate();

    json defaultMeshes = R"([
      {
        "name":"disc",
        "type": "disc",
        "radius": 1
      },
      {
        "name":"circle",
        "type": "circle",
        "radius": 1
      },
      {
        "name":"transform",
        "type": "transform"
      },
      {
        "name":"line",
        "type":"dynamic",
        "count":6
      },
      {
        "name":"polygon",
        "type":"dynamic",
        "count":32
      }
    ])"_json;

    SDL_Log("Debug Meshes...");
    for (json::iterator it = defaultMeshes.begin(); it != defaultMeshes.end(); ++it)
    {
      Spaghetti::Pool<Mesh>& meshes = Memory::GetPool<Mesh>();
      Mesh* m = meshes.New();
      if( m && m->Initialize( *it ) )
        mesh.emplace( make_pair( m->name, m ) );
    }

    debugMaterial = mv->material;
    colorLoc = std::get<0>( debugMaterial->uniform4fv["primaryColor"] );
  }

  int segmentIndex = 0;

  void DebugDraw::PreDraw()
  {
    debugMaterial->Activate();
    mv->Activate();
    segmentIndex = 0;
  }

  void DebugDraw::PostDraw()
  {
    mv->Deactivate();
  }

  void DebugDraw::DrawPolygon( const b2Vec2* vertices, int32 vertexCount, const b2Color& color )
  {
    array<float, 4> a4 = {color.r, color.g, color.b, 0.7};
    glUniform4fv( colorLoc, 1, a4.data() );
    mv->renderMesh = mesh["polygon"];
    mv->renderTransform->SetPosition( vec3( 0, 0, 0 ) );
    mv->renderTransform->SetRotation( vec3( -1, 0, 0 ), 0 );

    float* vertexArray = mv->renderMesh->GetVertexArrayAttribute( "position" );
    for( size_t i = 0; i < vertexCount && i < mv->renderMesh->vertexCount; i++ )
    {
      vertexArray[i * 3] = 0;
      vertexArray[i * 3 + 1] = vertices[i].y;
      vertexArray[i * 3 + 2] = vertices[i].x;
    }
    mv->renderMesh->UpdateMesh();
    mv->UpdateView( GL_LINE_LOOP );
  }
  /// Draw a solid closed polygon provided in CCW order.
  void DebugDraw::DrawSolidPolygon( const b2Vec2* vertices, int32 vertexCount, const b2Color& color )
  {
    DrawPolygon( vertices, vertexCount, color );
  }

  void DebugDraw::DrawCircle( const b2Vec2& center, float32 radius, const b2Color& color )
  {
    array<float, 4> a4 = {color.r, color.g, color.b, 0.7};
    glUniform4fv( colorLoc, 1, a4.data() );
    mv->renderMesh = mesh["circle"];
    mv->renderTransform->SetPosition( vec3( 0, center.y, center.x ) );
    mv->renderTransform->SetRotation( vec3( -1, 0, 0 ), 0 );

    float* vertexArray = mv->renderMesh->GetVertexArrayAttribute( "position" );
    size_t sides = mv->renderMesh->vertexCount;
    for( size_t i = 0; i < sides; i++ )
    {
      float inc = (float)i / (float)sides;
      vertexArray[i * 3] = radius * sinf( TAU * inc );
      vertexArray[i * 3 + 1] = radius * cosf( TAU * inc );
      vertexArray[i * 3 + 2] = 0;
    }
    mv->renderMesh->UpdateMesh();
    mv->UpdateView( GL_LINE_LOOP );
  }

  void DebugDraw::DrawSolidCircle( const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color )
  {
    array<float, 4> a4 = {color.r, color.g, color.b, 0.7};
    glUniform4fv( colorLoc, 1, a4.data() );
    mv->renderMesh = mesh["disc"];
    mv->renderTransform->SetPosition( vec3( 0, center.y, center.x ) );
    mv->renderTransform->SetRotation( vec3( -1, 0, 0 ), b2Atan2( axis.y, axis.x ) );

    float* vertexArray = mv->renderMesh->GetVertexArrayAttribute( "position" );
    for( size_t i = 0; i < mv->renderMesh->vertexCount; i++ )
    {
      float inc = (float)i / (float)mv->renderMesh->vertexCount;
      vertexArray[i * 3] = radius * sinf( TAU * inc );
      vertexArray[i * 3 + 1] = radius * cosf( TAU * inc );
      vertexArray[i * 3 + 2] = 0;
    }
    mv->renderMesh->UpdateMesh();
    mv->UpdateView( GL_TRIANGLES );
  }

  void DebugDraw::DrawSegment( const b2Vec2& p1, const b2Vec2& p2, const b2Color& color )
  {
    /*
    DrawCircle(p1,0.05,color);
    DrawCircle(p2,0.05,color);

    array<float,4> a4 = {1,1,1,1}; //{color.r,color.g,color.b,color.a};
    glUniform4fv( colorLoc, 1, a4.data() );
    mv->renderMesh = mesh["line"];
    mv->renderTransform->SetPosition( vec3(0,0,0) );
    mv->renderTransform->SetRotation( vec3(-1,0,0), 0 );

    float* vertexArray = mv->renderMesh->GetVertexArrayAttribute("position");
    vertexArray[0] = 0;
    vertexArray[1] = p1.y;
    vertexArray[2] = p1.x;
    vertexArray[3] = 0;
    vertexArray[4] = p2.y;
    vertexArray[5] = p2.x;
    mv->renderMesh->UpdateMesh();
    mv->UpdateView( GL_LINES );

*/
  }

  void DebugDraw::DrawTransform( const b2Transform& xf )
  {
    array<float, 4> color = {1, 0, 1, 1};
    glUniform4fv( colorLoc, 1, color.data() );
    mv->renderMesh = mesh["transform"];
    mv->renderTransform->SetPosition( vec3( xf.p.x, xf.p.y, 0 ) );
    mv->renderTransform->SetRotation( vec3( 0, 0, -1 ), xf.q.GetAngle() );
    mv->UpdateView( GL_LINES );
  }
}
