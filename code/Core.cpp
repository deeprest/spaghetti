#include "Core.h"

#include <csignal>
#include <memory>
#include <random>

#include "SDL2/SDL_events.h"
#include "SDL2/SDL_stdinc.h"

#include "GL.h"
//#include "SDL2/SDL_events.h"
#include "SDL2/SDL_log.h"
#include "SDL2/SDL_mouse.h"

#include "glm/vec3.hpp"

#include "Audio.h"
#include "Material.h"
#include "Resource.h"
#include "Scene.h"
#include "Timer.h"
#include "Debug.h"

#include "Input.h"
#include "MemoryPool.h"
#include "Mesh.h"  // for MeshGenerator::Init();

namespace Spaghetti
{

  using namespace std;
  using namespace Debug;
  using json = nlohmann::json;

  json Core::settings;
  std::fstream fs;
  SDL_LogOutputFunction defaultLogCallback;
  void* defaultLogUserdata = nullptr;

  volatile std::sig_atomic_t wait = 0;

  void Signal_Generic( int signal )
  {
    SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "*** SIGNAL caught: %d", signal );
  }

  void Signal_Continue( int signal )
  {
    SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "Continue signal caught" );
    wait = 0;
  }

  void Signal_SegFault( int signal )
  {
    stringstream ss;
    ss << "SEG FAULT: " << signal;
    SDL_LogCritical( SDL_LOG_CATEGORY_APPLICATION, "%s", ss.str().c_str() );
    SDL_Quit();
    exit( -1 );
  }

  void LogOutput( void* userdata, int category, SDL_LogPriority priority, const char* message )
  {
    fs << message << endl;
    if( defaultLogCallback )
    {
      defaultLogCallback( defaultLogUserdata, category, priority, message );
    }
  }

  Core::Core( int argc, char** argv )
  {
    // signal( SIGINT, Signal_Generic );
    signal( SIGSEGV, Signal_SegFault );
    signal( SIGINT, Signal_Continue );

    stringstream ss;
    for( int i = 0; i < argc; i++ )
    {
      if( std::strcmp( argv[i], "DEBUGBREAK" ) == 0 )
        wait = 1;
      ss << "args[" << i << "] = " << argv[i] << endl;
    }
    SDL_Log( "%s", ss.str().c_str() );

    SDL_Log( "waiting for continue signal ..." );
    while( wait )
    {
      //SDL_Log("DEBUGBREAK");
      // SDL_TriggerBreakpoint();
      // std::raise( SIGINT );
    }

    std::string fileLog = "game.log";
    try
    {
      fs.open( fileLog, ios::out );  // | ios::ate ); }
    }
    catch( const exception& e )
    {
      cout << "*** ERROR: log file cannot be created: " << e.what() << endl;
    }

    if( SDL_Init( SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_AUDIO ) != 0 )
    {
      fs << "*** SDL initialization failed" << endl;
      cout << "*** SDL initialization failed" << endl;
    }
    atexit( SDL_Quit );

    SDL_LogGetOutputFunction( &defaultLogCallback, &defaultLogUserdata );
    SDL_LogSetOutputFunction( LogOutput, nullptr );
    SDL_LogSetAllPriority( SDL_LOG_PRIORITY_DEBUG );  //SDL_LOG_PRIORITY_INFO //SDL_LOG_PRIORITY_DEBUG
    SDL_Log( "Launched: %s", Time::Now().c_str() );


    Debug::Initialize();
    Resource::Initialize();
    json configSettings = Resource::ReadJSON( "config.json" );
    if( configSettings.empty() )
    {
      SDL_LogWarn( SDL_LOG_CATEGORY_SYSTEM, "No config.json file found; using default settings." );
      settings = R"({
          "window":{
            "width":1280,
            "height":720,
            "x":0,
            "y":0
          }
        }
      )"_json;
    }
    else
    {
      settings = configSettings;
    }

    Pool<Mesh>& meshes = Memory::GetPool<Mesh>();


    InitializeVideo();
    Memory::InitializePool( 1000 );
    Time::Initialize();
    Timer::Initialize( 5 );
    InitializeInput();
#ifdef SCRIPTING_ENABLED
    InitializeScript();
#endif
    Audio::Initialize();

    MeshGenerator::Init();
  }


  Core::~Core()
  {
    SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Core dtor" );
    fs.close();
  }

  void Core::Execute()
  {
    SDL_Log( "Executing..." );

    InitializeScene();

    executing = true;
    while( executing )
    {
      Time::Update();

      UpdateInput();

      currentScene->UpdateScene();

      glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
      currentScene->RenderScene( Time::deltaTime );
      SDL_GL_SwapWindow( window );
      // This is so the CPU gets a break when v-sync is disabled, otherwise, cpu usage is around 100%!!!
      // This should only be enabled in windowed mode, or for saving the battery on laptops
      SDL_Delay( 1 );

      // TODO: error checks for audio, render

      Memory::CollectGarbage();
    }

    SDL_SetRelativeMouseMode( SDL_FALSE );
    // SDL_SetWindowFullscreen( window, 0 );
    // SDL_Quit();
    SDL_Log( "Terminated: %s", Time::Now().c_str() );

    Audio::Shutdown();
  }

  void Core::InitializeScene()
  {

    currentScene = std::make_shared<Scene>();
    json scene;
    // scene = Resource::ReadJSON( "scene.json");
    // scene = Resource::ReadCBOR( "scene.cbor");

    // generate a scene
    std::random_device randomdevice;
    std::default_random_engine randomengine( randomdevice() );
    std::uniform_real_distribution<float> distPosition( -100, 100 );
    std::uniform_real_distribution<float> distRadius( 1, 8 );
    scene["object"] = json::array();
    for( size_t i = 0; i < 80; i++ )
    {
      json obj = R"( {"transform":[{"id":1,"localMatrix":[1,0,0,0,0,1,0,0,0,0,1,0,0,0,0,1]}],"meshview":[{"transform":{"id":1},"mesh":{"type":"flatquad","radius":1},"material":"war"}]} )"_json;
      float radius = distRadius( randomengine );
      obj["meshview"][0]["mesh"]["radius"] = radius;
      if( radius > 6 )
      {
        obj["meshview"][0]["material"] = "blue";
      }
      else if( radius > 2 )
      {
        obj["meshview"][0]["material"] = "blue";
      }
      else
      {
        obj["meshview"][0]["material"] = "war";
      }
      //obj["audiosource"] = R"({"resource":"sounds/squee.ogg", "channel":2})"_json;

      obj["state"]["position"][0] = distPosition( randomengine );
      obj["state"]["position"][1] = distPosition( randomengine );
      obj["state"]["position"][2] = 0;
      scene["object"].push_back( obj );
    }

    // Resource::WriteJSON( "scene.json", scene );
    // Resource::WriteCBOR( "scene.cbor", scene );
    currentScene->Initialize( scene );
    scenes["one"] = currentScene;
  }

  void Core::InitializeInput()
  {
    if( SDL_SetRelativeMouseMode( SDL_TRUE ) == -1 )
    {
      SDL_Log( "Relative mouse mode not supported." );
    }
    // SDL_StopTextInput();
    // if( SDL_IsTextInputActive() )
    Input::KeyHeldEvent = SDL_RegisterEvents( 1 );
    SDL_FlushEvents( SDL_FIRSTEVENT, SDL_LASTEVENT );
  }


  SDL_Event event;
  void Core::UpdateInput()
  {
    if( Input::activeInputComponent )
      Input::activeInputComponent->PreFrame();

    while( SDL_PollEvent( &event ) )
    {
      bool handled = false;
      switch( event.type )
      {
        case SDL_QUIT:
          executing = false;
          handled = true;
          break;

        case SDL_WINDOWEVENT:
          SDL_LogDebug( SDL_LOG_CATEGORY_INPUT, "SDL_WINDOWEVENT %d", event.window.event );
          switch( event.window.event )
          {
            case SDL_WINDOWEVENT_SIZE_CHANGED:
              settings["window"]["width"] = event.window.data1;
              settings["window"]["height"] = event.window.data2;
              int w, h;
              SDL_GL_GetDrawableSize( window, &w, &h );
              SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "%d %d", w, h );
              glViewport( settings["window"]["x"], settings["window"]["y"], w, h );
              handled = true;
              break;

            case SDL_WINDOWEVENT_MOVED:
              settings["window"]["x"] = event.window.data1;
              settings["window"]["y"] = event.window.data2;
              handled = true;
              break;
          }
          break;

        case SDL_KEYDOWN:
          //if( textInputMode )
          if( !event.key.repeat )
          {
            switch( event.key.keysym.scancode )
            {
              default:
                break;
            }
            HeldKey[event.key.keysym.scancode] = event;
            SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "KEYDOWN %d %d %d", event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
          }
          break;

        case SDL_KEYUP:
          HeldKey.erase( event.key.keysym.scancode );
          switch( event.key.keysym.scancode )
          {
            case SDL_SCANCODE_ESCAPE:
              executing = false;
              handled = true;
              break;
            case SDL_SCANCODE_P:
              Memory::Dump();
              handled = true;
              break;
            case SDL_SCANCODE_F1:
              SDL_SetRelativeMouseMode( (SDL_bool)!SDL_GetRelativeMouseMode() );
              handled = true;
              break;
            case SDL_SCANCODE_F2:
              Screenshot();
              break;
            case SDL_SCANCODE_F3:
              Audio::SimpleAudioTest();
              break;
            case SDL_SCANCODE_O:
              currentScene->Save();
              handled = true;
              break;
            case SDL_SCANCODE_I:
              currentScene->Load( "scenebin" );
              handled = true;
              break;
            default:
              break;
          }
          SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "KEYUP %d %d %d", event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
          break;

        default:
          // SDL_LogDebug( SDL_LOG_CATEGORY_INPUT, "%d %d %d %d", event.type, event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
          break;
      }

      if( !handled )
      {
        if( Input::activeInputComponent )
          Input::activeInputComponent->HandleEvent( event );
      }

      // SDL_LogVerbose( SDL_LOG_CATEGORY_INPUT, "%d %d %d %d", event.type, event.key.keysym.scancode, event.key.timestamp, event.key.repeat );
    }

    for( auto& element : HeldKey )
    {
      element.second.type = Input::KeyHeldEvent;
      // event is passed by pointer but copied into SDL event queue
      SDL_PushEvent( &element.second );
    }
  }
}
