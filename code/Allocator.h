#pragma once
#include <cstddef>
#include <memory>
#include "SDL2/SDL_log.h"
namespace Spaghetti
{
  extern size_t total;

  template <typename T>
  struct Allocator
  {
    typedef T value_type;
    Allocator() = default;
    template <class U>
    Allocator( const Allocator<U>& ) {}
    T* allocate( std::size_t n )
    {
      n *= sizeof( T );
      total += n;
      SDL_Log( "ALLOC %zu bytes", n );
      return static_cast<T*>(::operator new( n ) );
    }
    void deallocate( T* p, std::size_t n )
    {
      total -= n * sizeof *p;
      SDL_Log( "DEALLOC %zu bytes", n * sizeof *p );
      ::operator delete( p );
    }
  };
  template <class T, class U>
  bool operator==( const Allocator<T>&, const Allocator<U>& ) { return true; }
  template <class T, class U>
  bool operator!=( const Allocator<T>&, const Allocator<U>& ) { return false; }
}
