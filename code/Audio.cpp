#include "Audio.h"
#include "SDL2/SDL_mixer.h"
#include "Resource.h"

namespace Spaghetti
{

  namespace Audio
  {
    Mix_Music* music;
    std::vector<uint8_t> buffer;

    void Initialize()
    {
      // system byte order, stereo audio, using 1024 byte chunks
      if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 1, 1024 ) == -1 )
      {
        SDL_Log( "Mix_OpenAudio: %s", Mix_GetError() );
        exit( 2 );
      }

      SDL_version compile_version;
      const SDL_version* link_version = Mix_Linked_Version();
      SDL_MIXER_VERSION( &compile_version );
      SDL_Log( "compiled with SDL_mixer version: %d.%d.%d", compile_version.major, compile_version.minor, compile_version.patch );
      SDL_Log( "linked with SDL_mixer version: %d.%d.%d", link_version->major, link_version->minor, link_version->patch );

      int flags = MIX_INIT_OGG;
      int init = Mix_Init( flags );
      if( (init & flags) != flags )
      {
        SDL_Log( "Mix_Init: Failed to init required ogg support!" );
        SDL_Log( "Mix_Init: %s", Mix_GetError() );
      }

      Mix_AllocateChannels( 8 );


      Resource::ReadBuffer( "music/test.ogg", buffer );
      music = Mix_LoadMUS_RW( SDL_RWFromConstMem( buffer.data(), buffer.size() ), SDL_TRUE );
      // music = Mix_LoadMUS( "music/test.ogg" );
      if( !music )
      {
        SDL_Log( "Mix_LoadMUS: %s", Mix_GetError() );
      }
      Mix_VolumeMusic( MIX_MAX_VOLUME / 5 );
      if( Mix_FadeInMusic( music, -1, 2000 ) == -1 )
      {
        SDL_Log( "Mix_FadeInMusic: %s", Mix_GetError() );
      }
      // Mix_PlayMusic( music, -1 );
    }

    void Shutdown()
    {
      Mix_FreeMusic( music );

      Mix_Quit();
      Mix_CloseAudio();
    }

    void SimpleAudioTest()
    {
      /*
      Mix_Chunk* sample = Mix_LoadWAV( "sounds/test.wav" );
      if( !sample )
        return;
      Mix_VolumeChunk( sample, MIX_MAX_VOLUME );
      Mix_PlayChannel( 1, sample, 0 );
      //Mik_FreeChunk( sample );
      */
    }
  }

/*
  std::vector<AudioClip> AudioClip::all;

  void AudioClip::Initialize()
  {
    all.reserve( 10 );
  }

  AudioClip* AudioClip::New( const std::string& name )
  {
    all.emplace_back();
    AudioClip* ref = &all[all.size() - 1];
    ref->name = name;
    return ref;
  }
  */
}
