#include "Mesh.h"

#include <algorithm>
#include <functional>
#include <sstream>
#include <unordered_map>
#include <vector>

#include "GL.h"
#include "SDL2/SDL_log.h"
#include "glm/glm.hpp"
#include "json.hpp"

#include "Common.h"
#include "Mathematics.h"
#include "Resource.h"
#include "Serialize.h"

namespace Spaghetti
{
  using json = nlohmann::json;
  using namespace std;

  bool Mesh::Initialize( const json& params )
  {
    valid = false;
    string type;
    json obj;

    if( params.empty() || params.is_null() )
    {
      SDL_LogWarn( LOG_CATEGORY_ASSET, "Mesh initializer is empty." );
      return false;
    }
    else
      // if( params.is_string() )
      // {
      //   // filename
      //   obj = Resource::ReadJSON( string("mesh/")+params.get<string>() );
      //   if( obj.empty() )
      //   {
      //     SDL_LogError(LOG_CATEGORY_ASSET, "Failed to load mesh %s", params.dump().c_str() );
      //     return false;
      //   }
      // }
      // else
      if( params.is_object() )
      {
        if( params.count( "type" ) > 0 )
        {
          type = params["type"].get<std::string>();
          if( !type.empty() && MeshGenerator::CanGenerateMesh( type ) )
          {
            std::transform( type.begin(), type.end(), type.begin(), ::tolower );
            if( !MeshGenerator::Generate( type, obj, params ) )
            {
              return false;
            }
          }
        }
        // else if( params.count( "metadata" ) > 0 )
        // {
        //   type = params["metadata"]["type"];
        // }
        else if( params.count( "resource" ) )
        {
          // filename
          obj = Resource::ReadJSON( string( "mesh/" ) + params["resource"].get<string>() );
          SDL_Log( "MESH %s", obj.dump().c_str() );
          if( obj.empty() )
          {
            SDL_LogError( LOG_CATEGORY_ASSET, "Failed to load mesh %s", params.dump().c_str() );
            return false;
          }
        }
        else
        {
          obj = params;
        }
      }

    if( !obj.count( "data" ) || !obj["data"].count( "attributes" ) || !obj["data"]["attributes"].count( "position" ) )
    {
      SDL_LogError( LOG_CATEGORY_ASSET, "Mesh has invalid format %s", obj.dump().c_str() );
      return false;
    }

    if( obj["data"].count( "index" ) > 0 && obj["data"]["index"].count( "type" ) > 0 )
    {
      if( obj["data"]["index"]["type"] == "Uint16Array" )
      {
        indexArray = obj["data"]["index"].at( "array" ).get<std::vector<IndexType>>();
      }
      else
      {
        SDL_LogError( LOG_CATEGORY_ASSET, "index type is not uint16" );
        return false;
      }
    }
    else
    {
      uint32_t count = 0;
      if( obj.count( "metadata" ) > 0 )
      {
        if( obj["metadata"].count( "position" ) > 0 )
        {
          SDL_LogWarn( LOG_CATEGORY_ASSET, "generating indices" );
          count = (uint32_t)( obj["metadata"]["position"] );
        }
      }
      else
      {
        count = obj["data"]["attributes"]["position"]["array"].size() / 3;
      }
      indexArray.reserve( count );
      for( IndexType i = 0; i < count; ++i )
      {
        indexArray.push_back( i );
      }
    }
    indexCount = indexArray.size();
    vertexCount = 0;
    vertexFloatCount = 3;
    vertsPerFace = 3;

    size_t count = 0;
    const json& attributes = obj["data"]["attributes"];
    for( auto& att : attributes )
    {
      count += att.at( "array" ).size();
      if( vertexCount == 0 )
        vertexCount = att.at( "array" ).size() / vertsPerFace;
    }

    vertexArray.reserve( count );
    char* offset = NULL;
    for( auto it = attributes.begin(); it != attributes.end(); ++it )
    {
      string attName = it.key();
      attributeOffset[attName] = offset;
      offset += vertexCount * vertexFloatCount * 4;  // sizeof(float)
      std::vector<float> v = it.value().at( "array" ).get<std::vector<float>>();
      std::copy( v.begin(), v.end(), std::back_inserter( vertexArray ) );
    }
    vertexArray.shrink_to_fit();

    size_t bufferSize = sizeof( float ) * vertexArray.size();

    glGenBuffers( 1, &vbo );
    glGenBuffers( 1, &vboIndices );

    glGenVertexArrays( 1, &vertexArrayID );

    glBindBuffer( GL_ARRAY_BUFFER, vbo );
    if( usage == GL_STATIC_DRAW )
    {
      glBufferData( GL_ARRAY_BUFFER, bufferSize, vertexArray.data(), usage );
    }
    if( usage == GL_DYNAMIC_DRAW )
    {
      glBufferData( GL_ARRAY_BUFFER, sizeof( float ) * vertexArray.size(), 0, usage );
      glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof( float ) * vertexArray.size(), vertexArray.data() );
    }

    glBindVertexArray( vertexArrayID );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, vboIndices );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( IndexType ) * indexArray.size(), indexArray.data(), usage );

    // glBindBuffer( GL_ARRAY_BUFFER, 0 );
    // glBindVertexArray( 0 );

    GLint errorCount = 0;
    for( GLenum currError = glGetError(); currError != GL_NO_ERROR; currError = glGetError(), ++errorCount )
      SDL_LogError( LOG_CATEGORY_ASSET, "Mesh is invalid %s %d", name.c_str(), currError );
    if( errorCount > 0 )
      return false;
    GLint bufsiz = 0;
    glGetBufferParameteriv( GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &bufsiz );
    if( bufsiz != bufferSize )
    {
      SDL_LogError( LOG_CATEGORY_ASSET, "Mesh buffer has wrong size %s", name.c_str() );
      return false;
    }

    name = Read<string>( params, "name", "" );

    valid = true;
    return true;
  }

  void Mesh::OnDestroy()
  {
    valid = false;
    name = "";
    indexArray.clear();
    vertexArray.clear();
    attributeOffset.clear();
    glDeleteBuffers( 1, &vbo );
    vbo = 0;
    glDeleteBuffers( 1, &vboIndices );
    vboIndices = 0;
    glDeleteVertexArrays( 1, &vertexArrayID );
    vertexArrayID = 0;
  }

  float* Mesh::GetVertexArrayAttribute( std::string attribute )
  {
    char* offset = attributeOffset[attribute];
    size_t index = (size_t)offset / sizeof( float );
    return &vertexArray.data()[index];
  }

  void Mesh::UpdateMesh()
  {
    glBindBuffer( GL_ARRAY_BUFFER, vbo );
    glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof( float ) * vertexArray.size(), vertexArray.data() );
    GLint errorCount = 0;
    for( GLenum currError = glGetError(); currError != GL_NO_ERROR; currError = glGetError(), ++errorCount )
    {
      SDL_LogError( LOG_CATEGORY_ASSET, "Error on mesh update %s %d", name.c_str(), currError );
      valid = false;
    }
  }
  /*
    float* Mesh::MapBuffer()
    {
      glBindBuffer( GL_ARRAY_BUFFER, vbo );
      float* buffer = (float*)glMapBuffer( GL_ARRAY_BUFFER, GL_WRITE_ONLY );
      if( !buffer )
        SDL_Log("failed to map buffer");
      GLvoid* buf = nullptr;
      glGetBufferPointerv( GL_ARRAY_BUFFER, GL_BUFFER_MAP_POINTER, &buf );
      return buffer;
    }

    void Mesh::UnmapBuffer()
    {
      glBindBuffer( GL_ARRAY_BUFFER, vbo );
      if( !glUnmapBuffer(GL_ARRAY_BUFFER) )
        SDL_Log("failed to unmap buffer");
    }
  */

  /*
  void ExperimentalUpdateWaveStep()
  {
    // Finite difference method step
    v = current
    p = previous
    n = next
    n[x][y] = 2*v[x][y] - p[x][y] + 0.1 * (v[x+1][y] - 2*v[x][y] + v[x-1][y] + v[x][y+1] - 2 * v[x][y] + v[x][y-1] );
    n[x][y] *= damp;
    // set edges to zero here.
    // swap: temp = p; p = v; v = n; n = temp;
  }
  */

  namespace MeshGenerator
  {
    using json = nlohmann::json;
    using namespace std;

    typedef function<json( const json& params )> MeshGeneratorFunction;
    unordered_map<string, MeshGeneratorFunction> MeshGenMap;
    json templateMesh;

    bool CanGenerateMesh( const string& type )
    {
      if( MeshGenMap.find( type ) != MeshGenMap.end() )
        return true;
      return false;
    }

    bool Generate( const std::string& meshType, json& out, const json& params )
    {
      if( MeshGenMap.find( meshType ) != MeshGenMap.end() )
      {
        out = MeshGenMap[meshType]( params );
        out["name"] = meshType;
        return true;
      }
      SDL_LogError( LOG_CATEGORY_ASSET, "Failed to generate mesh type %s", meshType.c_str() );
      return false;
    }

    json GenerateQuad( const json& params )
    {
      json obj = templateMesh;
      float length = 1;
      if( params.count( "length" ) )
        length = params["length"];
      std::vector<float> vts = { -length, -length, 0, length, -length, 0, length, length, 0, -length, length, 0 };
      std::vector<float> nrm = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };
      std::vector<uint16_t> idx = { 0, 1, 3, 1, 2, 3 };
      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      // obj["data"]["attributes"]["normal"]["array"] = nrm;
      return obj;
    }

    json GenerateFlatQuad( const json& params )
    {
      json obj = templateMesh;
      float length = 1;
      if( params.count( "length" ) )
        length = params["length"];
      std::vector<uint16_t> idx = { 0, 1, 3, 1, 2, 3 };
      std::vector<float> vts = { -length, -length, 0, length, -length, 0, length, length, 0, -length, length, 0 };
      std::vector<float> nrm = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };
      std::vector<float> uvs = { 0, 0, 1, 0, 1, 1, 0, 1 };
      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      /*obj["data"]["attributes"]["normal"]["array"] = nrm;*/
      obj["data"]["attributes"]["texcoord"]["array"] = uvs;
      return obj;
    }

    json GenerateQuadSegment( const json& params )
    {
      json obj = templateMesh;
      float length = 1;
      if( params.count( "length" ) )
        length = params["length"];
      float cap1 = 0.15f;
      if( params.count( "cap1" ) )
        cap1 = params["cap1"];
      float cap2 = 0.15f;
      if( params.count( "cap2" ) )
        cap2 = params["cap2"];

      glm::vec2 a( params["dir"][0], params["dir"][1] );
      a = glm::normalize( a );
      glm::vec2 b( a.y, -a.x );

      float vts[] = {
          -cap1 * b.x, -cap1 * b.y, 0,
          length * a.x - cap2 * b.x, length * a.y - cap2 * b.y, 0,
          length * a.x + cap2 * b.x, length * a.y + cap2 * b.y, 0,
          cap1 * b.x, cap1 * b.y, 0 };
      float nrm[] = { 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1 };
      uint16_t idx[] = { 0, 1, 3, 1, 2, 3 };
      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      // obj["data"]["attributes"]["normal"]["array"] = nrm;
      return obj;
    }

    json GenerateTransform( const json& params )
    {
      json obj = templateMesh;
      float size = 0.5f;
      float vts[] = { 0, 0, -size, 0, 0, size, 0, -size, 0, 0, size, 0 };
      float nrm[] = { -1, 0, 0, -1, 0, 0, -1, 0, 0, -1, 0, 0 };
      uint16_t idx[] = { 0, 1, 2, 3 };
      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      // obj["data"]["attributes"]["normal"]["array"] = nrm;
      return obj;
    }

    json GenerateCircle( const json& params )
    {
      json obj = templateMesh;
      int sides = 16;
      if( params.count( "sides" ) )
        sides = params["sides"];
      float radius = 1;
      if( params.count( "radius" ) )
        radius = params["radius"];
      float perside = TAU / (float)sides;
      std::vector<float> vts;
      vts.reserve( ( sides + 1 ) * 3 );
      std::vector<float> nrm;
      nrm.reserve( ( sides + 1 ) * 3 );
      std::vector<uint16_t> idx;
      idx.reserve( sides );
      float theta = 0;
      for( size_t i = 0; i < sides; i++ )
      {
        idx.push_back( i );
        vts.push_back( cos( theta ) * radius );
        vts.push_back( sin( theta ) * radius );
        vts.push_back( 0.0f );
        nrm.push_back( 0.0f );
        nrm.push_back( 0.0f );
        nrm.push_back( 1.0f );
        theta += perside;
      }
      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      return obj;
    }

    json GenerateDisc( const json& params )
    {
      json obj = templateMesh;
      int sides = 16;
      if( params.count( "sides" ) )
        sides = params["sides"];
      float radius = 1;
      if( params.count( "radius" ) )
        radius = params["radius"];
      float perside = TAU / (float)sides;
      std::vector<float> vts;
      vts.reserve( ( sides + 1 ) * 3 );
      std::vector<float> nrm;
      nrm.reserve( ( sides + 1 ) * 3 );
      std::vector<uint16_t> idx;
      idx.reserve( sides * 3 );
      float theta = 0;
      for( size_t i = 0; i < sides; i++ )
      {
        idx.push_back( i );
        idx.push_back( ( i + 1 ) % sides );
        idx.push_back( sides );
        vts.push_back( sin( theta ) * radius );
        vts.push_back( cos( theta ) * radius );
        vts.push_back( 0.0f );
        nrm.push_back( 0.0f );
        nrm.push_back( 1.0f );
        nrm.push_back( 0.0f );
        theta += perside;
      }
      vts.push_back( 0.0f );
      vts.push_back( 0.0f );
      vts.push_back( 0.0f );
      nrm.push_back( 0.0f );
      nrm.push_back( 0.0f );
      nrm.push_back( 1.0f );

      obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      obj["data"]["attributes"]["normal"]["array"] = nrm;
      return obj;
    }

    json GenerateDynamic( const json& params )
    {
      json obj = templateMesh;
      int count = Read<int>( params, "count", 32 );
      std::vector<float> vts( count * 3 );
      // std::vector<float> idx( count );
      // obj["data"]["index"]["array"] = idx;
      obj["data"]["attributes"]["position"]["array"] = vts;
      // obj["data"]["attributes"]["normal"]["array"] = nrm;
      return obj;
    }

    void Init()
    {
      // templateMesh = R"({
      //   "type":"BufferGeometry",
      //   "data":{
      //     "index":{ "type":"Uint16Array", "array":[], "itemSize":1 },
      //     "attributes":{
      //       "normal":{ "type":"Float32Array", "array":[], "itemSize":3 },
      //       "position":{ "type":"Float32Array", "array":[], "itemSize":3 }
      //     }
      //   },
      //   "uuid":""
      // })"_json;
      templateMesh = R"({
        "type":"BufferGeometry",
        "data":{
          "index":{ "type":"Uint16Array", "array":[], "itemSize":1 },
          "attributes":{
            "position":{ "type":"Float32Array", "array":[], "itemSize":3 }
          }
        },
        "uuid":""
      })"_json;
      MeshGenMap["circle"] = GenerateCircle;
      MeshGenMap["disc"] = GenerateDisc;
      MeshGenMap["quad"] = GenerateQuad;
      MeshGenMap["flatquad"] = GenerateFlatQuad;
      MeshGenMap["segment"] = GenerateQuadSegment;
      MeshGenMap["transform"] = GenerateTransform;
      MeshGenMap["dynamic"] = GenerateDynamic;
    }
  }  // namespace MeshGenerator
}  // namespace Spaghetti
