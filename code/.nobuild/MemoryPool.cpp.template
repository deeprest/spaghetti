#include "MemoryPool.h"

#include <new>
#include <memory>
#include <unordered_map>
#include <typeinfo>
#include <typeindex>
#include <iostream>
#include <string>
#include <vector>

#include "SDL2/SDL_log.h"
#include "Box2D/Box2D.h"
#include "physfs.h"

#include "../Allocator.h"
#include "../Pool.h"
#include "../Resource.h"
#include "../Scene.h"
#include "Component.h"
{{#Components}}
#include "{{type}}.h"
{{/Components}}
{{#PoolTypes}}
#include "{{type}}.h"
{{/PoolTypes}}

#if 0
void* operator new(std::size_t sz)
{
  // SDL_PollEvent() calls SDL_PumpEvents(). On MacOS, this process receives many calls to
  // new() for input events. This originates in macho/kernel, a kevent is sent using XPC
  // and a std::vector of ulongs is allocated with default new() for mouse and key events.
  // SDL_Log("new called, size = %zu",sz);
  return std::malloc(sz);
}
void operator delete(void* ptr) noexcept
{
  // SDL_Log("delete called");
  std::free(ptr);
}
#endif

// Memory allocators. Modify these to use your own allocator.
void* b2Alloc(int32 size)
{
  // SDL_Log("Box2d alloc: %d", size );
  //total += size;
	return malloc(size);
}

void b2Free(void* mem)
{
  // SDL_Log("Box2d free");
	free(mem);
}

// You can modify this to use your logging facility.
void b2Log(const char* string, ...)
{
	va_list args;
	va_start(args, string);
  SDL_Log( string, args );
	//vprintf(string, args);
	va_end(args);
}

namespace Spaghetti
{
	//Allocator.h
  size_t total;

  using namespace std;
  using json = nlohmann::json;
  using std::placeholders::_1;

  // Using a void* because tha map values are vector, and they hold different types.
  // On lookup, use the key-type to cast the value to a vector of the correct type.
  unordered_map<type_index, void*> typemap;
  unordered_map<type_index, string> typeToName;

	// Components
	{{#Components}}
  Pool<{{type}}> {{type}}Pool;
  {{/Components}}
	// PoolTypes
	{{#PoolTypes}}
  Pool<{{type}}> {{type}}Pool;
  {{/PoolTypes}}

  std::vector<GameObject*> garbage;
  unordered_map<ID_t,PoolType*> allPoolTypes;
	ID_t NextGlobalID = 1;

#if 0
  template <typename T>
  void Pool<T>::Initialize( size_t poolsize )
  {
    pool.reserve( poolsize );
    bucket.reserve( poolsize );
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Mem total: %zu MB", Spaghetti::total / ( 1024 * 1024 ) );
  }

  template <typename T>
  T* Pool<T>::New()
  {
    const char* typnam = typeid( T ).name();
    if( pool.size() + 1 > pool.capacity() )
    {
      // 'freecount' is the count of allocated-but-unused objects.
      if( freecount > 0 )
      {
        // find an unused bucket
        for( auto& bu : bucket )
        {
          if( bu.isFree )
          {
            bu.isFree = false;
            bu.active = true;
            freecount--;
            SDL_LogVerbose( LOG_CATEGORY_MEMORY, "CREATE (RECYCLE) %s bucket:%zu count:%zu capacity:%zu",
                            typnam, bu.index, pool.size(), pool.capacity() );
            return &pool[bu.index];
          }
        }
      }
      // No free buckets found
      // A new capacity would invalidate all existing pointers
      // If reallocating here, then preserve object references with handles (offsets from base) instead of pointers.
      // transform.reserve( transform.capacity() * 2 );
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "*** No free buckets in Pool<%s> ***", typnam );
      return nullptr;
    }
    // There are free slots in the pool
    pool.emplace_back();
    bucket.emplace_back();
    size_t index = pool.size() - 1;
    bucket[index].index = index;
    bucket[index].active = true;
    // SDL_Log( "{{type}} count:%zu capacity:%zu", pool.Pool.size(), pool.Pool.capacity() );
    T& pt = pool.data()[index];
    pt.id = NextGlobalID++;
    pt.index = index;
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> New() count:%zu freecount:%zu capacity:%zu",
                  typnam, pool.size(), freecount, pool.capacity() );
    return &pt;
  }

  template<typename T>
  void Pool<T>::MarkForDestruction( T* cmp )
  {
    bucket[cmp->index].active = false;
  }

  template <typename T>
  void Pool<T>::Destroy( T* cmp )
  {
    bucket[cmp->index].isFree = true;
    bucket[cmp->index].active = false;
    freecount++;

    const char* typnam = typeid( T ).name();
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<%s> Destroy() index:%zu count:%zu freecount:%zu capacity:%zu",
                  typnam, cmp->index, pool.size(), freecount, pool.capacity() );
  }

  template <typename T>
  void Pool<T>::Dump()
  {
    SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<{{type}}> Dump() count:%zu freecount:%zu capacity:%zu",
                  pool.size(), freecount, pool.capacity() );
  }

#endif

  namespace Memory
  {
		/*template<typename T>
		Pool<T>& GetPool()
		{
			// On lookup, use the key-type to cast the value to a vector of the correct type.
			return *((Pool<T>*)(typemap[type_index(typeid(T))]));
		}*/

		{{#Components}}
		template<> Pool<{{type}}>& GetPool(){ return {{type}}Pool; }
		{{/Components}}

		{{#PoolTypes}}
		template<> Pool<{{type}}>& GetPool(){ return {{type}}Pool; }
		{{/PoolTypes}}


    void InitializePool( size_t poolsize )
    {
      SDL_LogVerbose( LOG_CATEGORY_MEMORY, "Initialize Memory Pools");
      {{#Components}}
      {{type}}Pool.Initialize( poolsize );
      typemap[type_index(typeid({{type}}))] = &{{type}}Pool;
      typeToName[type_index(typeid({{type}}))] = "{{lowerName}}";
      {{/Components}}

			{{#PoolTypes}}
			{{type}}Pool.Initialize( poolsize );
      typemap[type_index(typeid({{type}}))] = &{{type}}Pool;
      typeToName[type_index(typeid({{type}}))] = "{{lowerName}}";
			{{/PoolTypes}}

/*			// meshes
			char** listBegin = PHYSFS_enumerateFiles( "mesh/" );
			for( char** file = listBegin; *file != NULL; file++ )
			{
				SDL_Log( " MESH [%s]", *file );
				json jmesh = Resource::ReadJSON(string( "mesh/" ) + ( *file ) );
				Pool<Mesh>& meshes = GetPool<Mesh>();
				Mesh& mesh = *meshes.New();
				mesh.Initialize( jmesh );
			}
			PHYSFS_freeList( listBegin );
*/

      SDL_Log("Mem total: %zu MB", Spaghetti::total / (1024*1024) );
    }


    template<typename T>
    T* Create()
    {
      Pool<T>& pool = GetPool<T>();
      T* pt = pool.New();
			if( pt )
			{
				allPoolTypes[ pt->id ] = pt;
				return pt;
			}
			return nullptr;
    }

    template<typename T>
    void MarkForDestruction( T* cmp )
    {
      Pool<T>& pool = GetPool<T>();
			pool.MarkForDestruction( cmp );
    }

    template<typename T>
    void Destroy( T* cmp )
    {
      Pool<T>& pool = GetPool<T>();
			pool.Destroy( cmp );

      cmp->gameObject = nullptr;
    }

    void Dump()
    {
      {{#Components}}
      SDL_LogDebug( LOG_CATEGORY_MEMORY, "Pool<{{type}}> count:%zu freecount:%zu capacity:%zu",
        {{type}}Pool.pool.size(), {{type}}Pool.freecount, {{type}}Pool.pool.capacity() );
      {{/Components}}
    }

    void CollectGarbage()
    {
      for( auto& go : garbage )
      {
        GameObject::DestroyImmediate( go );
      }
      garbage.clear();
    }
  }

  {{#Components}}
  template<>
  {{type}}* Component::GetComponentByID( ID_t id )
  {
    return ({{type}}*)allPoolTypes[id];
  }
  {{/Components}}

  void GameObject::DestroyImmediate( GameObject* go )
  {
    {{#Components}}
    std::vector<{{type}}*>& cmps{{type}} = go->GetComponents<{{type}}>();
    for(auto& cmp:cmps{{type}})
      cmp->OnDestroy();
    {{/Components}}
    {{#Components}}
    for(auto& cmp:cmps{{type}})
      Memory::Destroy( cmp );
    {{/Components}}
  }

  void GameObject::Destroy( GameObject* go )
  {
    if( go->OnDestroy )
    {
      go->OnDestroy();
    }
    garbage.push_back( go );
    {{#Components}}
    std::vector<{{type}}*>& cmps{{type}} = go->GetComponents<{{type}}>();
    for(auto& cmp:cmps{{type}})
      Memory::MarkForDestruction( cmp );
    {{/Components}}
  }

  typedef std::function<void(nlohmann::json&)> JsonFilterFunction;

  // run a filter function on every object
  void FilterJson( json& obj, JsonFilterFunction jff )
  {
    if( obj.is_array() )
    {
      for( auto it = obj.begin(); it != obj.end(); ++it)
      {
        FilterJson( *it, jff );
      }
    }
    else
    if( obj.is_object() )
    {
      jff( obj );
      for( auto it = obj.begin(); it!=obj.end(); ++it )
      {
        // cout << it.key() << " : " << it.value() << "\n";
        if( (*it).is_object() || (*it).is_array() )
        {
          FilterJson( *it, jff );
        }
      }
    }
  }


  {{#Components}}
  std::vector<ComponentInitBinding> inits{{type}};
  {{/Components}}
// bool CreateComponent(Scene* scene, const json& jsonComponent, const std::string& lowercase, GameObject* go, unordered_map<ID_t,ID_t>& oldToNewID, std::vector<ComponentInitBinding>& inits )
  bool CreateComponent(Scene* scene, const json& jsonComponent, const std::string& lowercase, GameObject* go, unordered_map<ID_t,ID_t>& oldToNewID )
  {
    {{#Components}}
    if( lowercase == "{{lowerName}}" )
    {
      // Must create all components to get the real ID
      {{type}}* cmp = Memory::Create<{{type}}>();
      if( cmp == nullptr )
      {
        SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to create component %s", lowercase.c_str() );
        return false;
      }
      cmp->gameObject = go;
      go->GetComponents<{{type}}>().push_back( cmp );
      cmp->scene = scene;


      // Add old and new IDs to map
      if( jsonComponent.count( "id" ) > 0 )
      {
        ID_t oldID = jsonComponent["id"].get<ID_t>();
        oldToNewID[ oldID ] = cmp->id;
      }
      // initialize the component later with a map of correct global IDs
      inits{{type}}.push_back( {std::bind( &{{type}}::Initialize, cmp, _1 ), jsonComponent } );
      // inits.push_back( {std::bind( &{{type}}::Initialize, cmp, _1 ), jsonComponent } );
      return true;
    }
    {{/Components}}
    return true;
  }


  GameObject* Scene::CreateGameObject( const nlohmann::json& obj )
  {
    bool valid = true;
    GameObject* go = new GameObject();
    unordered_map<ID_t,ID_t> oldToNewID;
    // std::vector<ComponentInitBinding> inits;
    {{#Components}}
    inits{{type}}.clear();
    {{/Components}}
    string type;
    string lowercase;
    for( auto it = obj.begin(); valid && it!=obj.end(); ++it )
    {
      type = it.key();
      lowercase = type;
      std::transform( lowercase.begin(), lowercase.end(), lowercase.begin(), ::tolower );
      if( (*it).is_object() )
      {
        // if( !CreateComponent( this, *it, lowercase, go, oldToNewID, inits ) )
        if( !CreateComponent( this, *it, lowercase, go, oldToNewID ))
        {
          valid = false;
          break;
        }
      }
      else
      if( (*it).is_array() )
      {
        for( auto element = (*it).begin(); valid && element!=(*it).end(); ++element )
        {
          // if( !CreateComponent( this, *element, lowercase, go, oldToNewID, inits ) )
          if( !CreateComponent( this, *element, lowercase, go, oldToNewID )) //, inits ) )
          {
            valid = false;
            break;
          }
        }
      }
    }
    if( valid )
    {
      //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID (old to new):");
      /*for(auto o2n : oldToNewID )
      {
        SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "%llu:%llu", o2n.first, o2n.second );
      }*/
      // Initialize components in order defined in build config.
      // Components can rely on this order when initializing.
      // Correct the IDs for each component before initializing,
      // so that components can call Component::GetComponentByID()
      {{#Components}}
      for( auto cib : inits{{type}})
      // for( auto cib : inits )
      {
        json correctedIDs = cib.params;
        FilterJson( correctedIDs, [&](json& j)
        {
          if( j.count("id"))
          {
            int old = j["id"].get<int>();
            j["id"] = oldToNewID[ old ];
            //SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "ID old:%d new:%d", old, j["id"].get<int>() );
          }
        });

        // cif = Component Initialize Function
        if( !cib.cif( correctedIDs ) )
        {
          valid = false;
          SDL_LogDebug( SDL_LOG_CATEGORY_APPLICATION, "Component init failed ({{type}})" );
          break;
        }
      }
      {{/Components}}
    }
    if( valid )
    {
      objects.push_back( go );
      return go;
    }
    GameObject::DestroyImmediate( go );
    delete go;
    SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Game Object did not initialize!");
    return nullptr;
  }


}
