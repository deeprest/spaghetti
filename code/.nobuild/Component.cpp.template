#include "Component.h"

#include <vector>
#include <typeinfo>
#include <typeindex>
#include <cstddef> //for offsetof

#include "json.hpp"
#include "MemoryPool.h"
#include "ComponentHeaders.h"

namespace Spaghetti
{
  using namespace std;
  using namespace glm;
  using json = nlohmann::json;

  // template<typename T>
  // void Component::Destroy( T* cmp )
  // {
  //   cmp->gameObject->RemoveComponent( cmp );
  //   cmp->OnDestroy();
  //   Memory::Destroy( cmp );
  // }

  // template<typename T>
  // void GameObject::RemoveComponent( T* cmp )
  // {
  //   std::vector<T*>& cmps = GetComponents<T>();
  //   for (auto it = cmps.begin(); it != cmps.end(); )
  //   {
  //     if( *it == cmp )
  //     {
  //       cmps.erase( it );
  //       break;
  //     }
  //   }
  // }

  {{#Components}}
  template<>
  void Component::Destroy( {{type}}* cmp )
  {
    cmp->OnDestroy();
    cmp->gameObject->RemoveComponent( cmp );
    Memory::Destroy( cmp );
  }
  {{/Components}}

  {{#Components}}
  template<>
  std::vector<{{type}}*>& GameObject::GetComponents()
  {
    return {{type}}Components;
  }
  {{/Components}}

  {{#Components}}
  template<>
  {{type}}* GameObject::GetComponent()
  {
    {{type}}* out = nullptr;
    std::vector<{{type}}*>& cmps = GetComponents<{{type}}>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  {{/Components}}

  // template<>
  // {{type}}* GameObject::AddComponent()
  // {
  //   {{type}}* cmp = Memory::Create<{{type}}>();
  //   if( cmp == nullptr )
  //   {
  //     SDL_LogWarn( SDL_LOG_CATEGORY_APPLICATION, "Failed to add new component of type {{type}}" );
  //     return nullptr;
  //   }
  //   std::vector<{{type}}*>& cmps = GetComponents<{{type}}>();
  //   cmps.push_back( cmp );
  //   cmp->gameObject = this;
  //   cmp->scene = scene;
  //   // cmp->Initialize();
  //   return cmp;
  // }

  {{#Components}}
  void GameObject::RemoveComponent( {{type}}* cmp )
  {
    std::vector<{{type}}*>& cmps = GetComponents<{{type}}>();
    for (auto it = cmps.begin(); it != cmps.end(); ++it )
    {
      if( *it == cmp )
      {
        it = cmps.erase( it );
        break;
      }
    }
  }
  {{/Components}}

  {{#Components}}
  template<>
  const std::vector<{{type}}*>& GameObject::GetComponentsConst() const
  {
    return {{type}}Components;
  }
  {{/Components}}

  {{#Components}}
  template<>
  {{type}}* GameObject::GetComponentConst() const
  {
    {{type}}* out = nullptr;
    const std::vector<{{type}}*>& cmps = GetComponentsConst<{{type}}>();
    if( !cmps.empty() )
      out = cmps.front();
    return out;
  }
  {{/Components}}

  void GameObject::SetPosition( glm::vec3 pos )
  {
    const std::vector<Transform*>& tfs = GetComponents<Transform>();
    vec3 origin;
    if( tfs.size()>0 )
      origin = tfs.front()->GetPosition();
    for(auto& tf : tfs )
    {
      vec3 offset = tf->GetPosition() - origin;
      tf->SetPosition( pos + offset );
    }

    const std::vector<Rigidbody*>& rbds = GetComponents<Rigidbody>();
    vec3 rbOrigin;
    if( rbds.size()>0 )
      rbOrigin = rbds.front()->boundTransform->GetPosition();
    for(auto& rb : rbds )
    {
      vec3 offset = rb->boundTransform->GetPosition() - rbOrigin;
      // SDL_Log("offset: %f %f %f", offset.x, offset.y, offset.z );
      rb->SetPosition( pos + offset );
    }
  }

  void GameObject::SetState( const json& state )
  {
    for( json::const_iterator it = state.begin(); it != state.end(); ++it )
    {
      if( it.key() == "position")
      {
        std::vector<float> fv = it.value().get<std::vector<float>>();
        SetPosition( glm::vec3(fv[0],fv[1],fv[2]) );
      }
    }
  }

  void to_json( json& j, const GameObject& obj)
  {
    const Transform* t = obj.GetComponentConst<Transform>();
    glm::vec3 position = t->GetPosition();
    j = {
      {"filename", obj.filename},
      {"state",{"position", std::array<float,3>({position.x,position.y,position.z})}}
    };
  }

  void from_json(const json& j, GameObject& obj)
  {
    // obj.id.value = j.at("id").get<ID_t>();
    //obj.name = j.at("name").get<std::string>();
    // {{#Components}}
    // Memory::Create( obj.{{type}}Component );
    // (*obj.{{type}}Component) = j.at("{{type}}").get<{{type}}>();
    // {{/Components}}
  }


}
