// Copyright © 2018 Matt Spencer

#pragma once

// Standard
#include <cstdarg>
#include <cstddef>  // for nullptr
#include <cstdio>
#include <ctime>
#include <fstream>
#include <iostream>
#include <locale>
#include <sstream>
#include <string>
#include <unordered_map>
#include <memory>
#include <vector>

// Libraries
#include "SDL2/SDL.h"
#include "SDL2/SDL_log.h"
#include "SDL2/SDL_opengl.h"
#include "GL.h"

#ifdef SCRIPTING_ENABLED
#include "angelscript.h"
#include "angelscript/contextmgr.h"
#endif
#include "json.hpp"

// Engine
#include "Common.h"
#include "Debug.h"
#include "Scene.h"


namespace Spaghetti
{
  using json = nlohmann::json;

  class Core
  {
  public:
    static json settings;

    Core( int argc, char** argv );
    ~Core();

    void Execute();

  private:

    void InitializeAudio();

    // Render Task
    uint32_t InitializeVideo();
    void Screenshot();

    // Input Task
    void InitializeInput();
    void UpdateInput();

    // Scene Task
    void InitializeScene();

#ifdef SCRIPTING_ENABLED
    // Script Task
    void InitializeScript();
    void ExecuteContext();
    // script helpers
    void ScriptBuild( const char* scriptName );
    void RegisterAllBindings( asIScriptEngine* engine );
#endif

    bool executing;
    SDL_Window* window;
    SDL_GLContext context;
    std::unordered_map<uint32_t, SDL_Event> HeldKey;

    // per-task
    uint32_t updateAccumulator = 0;
    uint32_t targetUpdateTimestep = 15;

#ifdef SCRIPTING_ENABLED
    // Script
    asIScriptEngine* engine;
    asIScriptModule* mod;  // single module for now
    asIScriptFunction* func;
    asIScriptContext* ctx;
    CContextMgr* contextManager;
#endif

    std::unordered_map<std::string, std::shared_ptr<Scene>> scenes;
    std::shared_ptr<Scene> currentScene;

  };
}
