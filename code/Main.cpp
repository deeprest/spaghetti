#include "Core.h"

// TODO: accept a filename to a settings.json
// #include "json.hpp"

// TODO: accept key=value pairs to override specific settings (after options settings file is loaded)

int main( int argc, char** argv )
{
  Spaghetti::Core core( argc, argv );
  core.Execute();
  return 0;
}
