# Spaghettii
a silly mess of game code

## Principles
* Only use free libraries.
* Avoid non-free tools when possible.
* Avoid unnecessary abstractions at the cost of potential refactor.
* Avoid high complexity.
* Avoid pre-optimization; cross that bridge when you get to it, and make sure it's necessary.

## Preferences
* JSON for config files, object structure, mesh data, metadata. Can use CBOR binary format for release.
* Code: avoid polymorphism and instead use 'flat' hierarchies (single parent for related classes) and tight coupling.
* Code: prefers externally preprocessed, templated source rather than using C++ templates and the C++ preprocessor.
* VCS: Git, and [Git Large File Storage](https://git-lfs.github.com/) is a plugin for git that handles binary files.
* Give priority to Linux and MacOS

## Toolset
1. This repo, the engine source (C++11)
2. (Required) [Sauce](https://github.com/deeprest/sauce/) build tool. Written for nodejs and uses [gulp](https://www.npmjs.com/package/gulp). It generates script bindings, prepares build for packaging, and much more.
3. *todo* Code for game systems (C++ and AngelScript)

* Libraries: SDL2, OpenGL, AngelScript
* Tools: NodeJS(gulp,libclang), Clang++


## How to Build This Thing
I use a custom build tool [Sauce](https://github.com/deeprest/sauce/). Supported development platforms are currently Linux (Debian) and MacOS. Clang++ is the compiler frontend of choice.

On MacOS, the Command Line Tools [here](https://developer.apple.com/download/more/) will include clang.
