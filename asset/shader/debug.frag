#version 330 core
uniform vec4 primaryColor;
out vec4 outValue;
void main()
{
  outValue = primaryColor;
}
