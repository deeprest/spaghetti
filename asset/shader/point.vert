#version 330 core
const float minPointSize = 32;
const float maxPointSize = 256;
const float maxPointDistance = 20;
const float wiggleRange = 0.005;
const float wiggleTimescale = 8;
// uniform vec3 offset;
uniform mat4 ModelView;
uniform mat3 NormalMatrix;
uniform mat4 Projection;
uniform vec4 primaryColor;
in vec3 position;
in vec3 normal;
centroid out vec4 litColor;
noperspective out vec4 screenPos;
void main()
{
  //vec3 pos = vec3( position.x+sin((time+gl_VertexID)*wiggleTimescale)*wiggleRange, position.y, position.z+cos((time+gl_VertexID)*wiggleTimescale)*wiggleRange );
  gl_Position = ( Projection * ModelView ) * vec4( position, 1 );
  //gl_PointSize = minPointSize + ( (1.0 - gl_Position.z/gl_Position.w) * (maxPointSize-minPointSize) );
  screenPos = vec4( ((gl_Position.x/gl_Position.w*2.0)*0.5+0.5), ((gl_Position.y/gl_Position.w*2.0)*0.5+0.5), 0, 1);
  vec3 lightDir = vec3( 1, 0, 0 );
  vec3 worldNormal = NormalMatrix * normal;
  litColor = vec4(0.2) + vec4( 0.8 ) * max( dot( normalize(-worldNormal), normalize(lightDir) ), 0);
  litColor = vec4( (1-primaryColor.a)*litColor.rgb + primaryColor.a*primaryColor.rgb, litColor.a);
}
