#version 330 core

uniform mat4 ModelView;
uniform mat3 NormalMatrix;
uniform mat4 Projection;
//uniform vec4 primaryColor;
in vec3 position;
in vec3 normal;
void main()
{
  gl_Position = ( Projection * ModelView ) * vec4( position, 1 );

  vec3 wtf = normal; // must use vertex attribute to avoid error, apparently.
}
