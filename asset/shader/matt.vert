#version 330 core

uniform mat4 ModelView;
uniform mat3 NormalMatrix;
uniform mat4 Projection;
uniform vec4 primaryColor;
in vec3 position;
in vec3 normal;
centroid out vec4 litColor;
noperspective out vec4 screenPos;
void main()
{
  gl_Position = ( Projection * ModelView ) * vec4( position, 1 );

  screenPos = vec4( ((gl_Position.x/gl_Position.w*2.0)*0.5+0.5), ((gl_Position.y/gl_Position.w*2.0)*0.5+0.5), 0, 1);
  vec3 lightDir = vec3( 1, 0, 0 );
  vec3 worldNormal = NormalMatrix * normal;
  litColor = vec4(0.2) + vec4( 0.8 ) * max( dot( normalize(-worldNormal), normalize(lightDir) ), 0);
  litColor = vec4( (1-primaryColor.a)*litColor.rgb + primaryColor.a*primaryColor.rgb, litColor.a);
}
