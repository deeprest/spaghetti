#version 330 core

uniform mat4 ModelView;
uniform mat3 NormalMatrix;
uniform mat4 Projection;
uniform vec4 primaryColor;
in vec3 position;
centroid out vec4 litColor;

void main()
{
  gl_Position = ( Projection * ModelView ) * vec4( position, 1 );

  vec3 lightDir = vec3( 1, 0, 0 );
  vec3 worldNormal = NormalMatrix * vec3( 1,0,0 );
  litColor = vec4(0.2) + vec4( 0.8 ) * max( dot( normalize(-worldNormal), normalize(lightDir) ), 0);
  litColor = vec4( (1-primaryColor.a)*litColor.rgb + primaryColor.a*primaryColor.rgb, litColor.a);
}
