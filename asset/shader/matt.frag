#version 330 core
precision lowp float;
centroid in vec4 litColor;
noperspective in vec4 screenPos;
const float gridSize = 512;
const float gridIntensity = 0.2;
const float noiseMin = 0.3;
const float noiseMax = 0.4;
out vec4 outValue;
void main()
{
  float bulb = 1 - clamp( pow(length(screenPos.xy*2-1)*0.4,2), 0, 1 );
  float grid = mix( 1, round( sin(screenPos.y*gridSize)*0.5+0.5 ), 0.4 );
  // edge fade and grid
  outValue = litColor * vec4(bulb*mix(1,grid*(1-bulb),gridIntensity));
  // noise
  float noise = fract(sin(dot(vec2(screenPos.r,screenPos.g) ,vec2(12.9898,78.233))) * 43758.5453);
  outValue *= vec4( mix(1,noise, noiseMax * (noiseMin+(1-noiseMin)*(1-bulb)) ) );
}
/*
uniform vec2 resolution;
uniform vec2 mouse;  //Robert Schütze (trirop) 07.12.2015
void main()
{
  vec2 resolution = vec2(640,360);
  vec2 mouse = vec2(0.5,0.5);

  vec3 p = vec3( ( gl_FragCoord.xy ) / ( resolution.x ), mouse.x );
  for( int i = 0; i < 50; i++ )
  {
    p.xzy = vec3( 1, 1, 1 ) * ( abs( ( abs( p ) / dot( p, p ) - vec3( 1.0, 1.0, 0.5 * mouse.y ) ) ) );
  }
  outValue.rgb = p;
  outValue.a = 1.0;
}
*/
